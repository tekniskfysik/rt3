<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class addUser extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:addUser';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$email = $this->ask('email?');
		$password = $this->secret('What is the password?');

		$data = array(
			'email' => $email,
			'password' => Hash::make($password),
		);
		$validator = Validator::make($data = $data, User::$rules);

		if ($validator->fails())
		{
			$this->info('Something went wrong, try again.');
			$this->info($validator->errors());
		}
		else{
			$user = User::create($data);
			$this->info('User created');
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
