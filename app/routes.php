<?php

//TRollololol
if(!Config::get('app.debug')) {
	App::error(function(Exception $exception) {
		return View::make('javlagunnars');
	});
}

Route::post('test', function(){
    return Input::all();
    \Goals\Fsrs\FsrExporter::export(Input::get('courses'));

    return;
    return Input::all();
    return \RT\Courses\Course::whereIn('id', Input::get('courses'))->lists('title');
});

Route::get('matrix', function(){
    $sum = new \RT\Courses\CourseSummarizer([1,2,3,5]);
    return \Acme\Services\HighChart::render($sum->plotGoalMatrix());
});

Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'PagesController@dashboard']);
Route::get('tutorial', ['as' => 'tutorial', 'uses' => 'PagesController@tutorial']);



Route::get('chart', function(){
    $chart = new \Acme\Services\HighChart();
    //$data =['data' => ['Uppnådd' => [12, 3, 2,3], 'Krav' => [13, 9, 4,4], 'Häst' => [[1.2,1], [2,2], [3,3], [4,4]]], 'categories' => [null, "Två", "tre", "alfont"]];
    //$chart = $chart->base($data, "", "", "", ['column', 'spline', 'column'])->stacked()->shared();

    $data = [
        'series' => [
            [
                'data' => [7,2,3,7,2],
                'name' => "omgång 1",
                'yAxis' => 1,
                'type' => 'column',

            ],
            [
                'data' => [3,4,5,3,5],
                'name' => "omgång 2",
                'yAxis' => 1,
                'type' => 'column',
            ],
            [
                'data' => [61,42,53,44,41],
                'name' => "score",
                'tooltip' => ["valueSuffix" => '%'],
                'dashStyle' => 'shortdot'
            ]
        ],
        'xaxis' => [
            ['title' => 'xxxx', 'categories' => [4, 3,12, 43]],
        ],
        'yaxis' => [
            ['title' => 'yyy3', 'format' => '{value}%'],
            ['title' => 'yyy1', 'format' => '{value} st'],
        ]
    ];

    //$data = ['series' => [['data' => [1, 2, 3], 'type' => 'column'], ['data' => [1, 4, 5],  'type' => 'column']]];
    $chart = $chart->plot($data)->shared();


    return \Acme\Services\HighChart::render($chart);
});

Route::post('form', ['as' => 'form', 'uses' => 'PagesController@formTester']);


Route::get('excel', function(){
    $excel = Excel::create('hej', function($excel) {
        $excel->setTitle('Our new awesome title');
        $excel->setCreator('Röda Tråden 3')->setCompany('Teknisk Fysik');
    });
    $summary = $excel->sheet('Summary');
    $courses =  RT\Courses\Course::all()->toArray();
    $summary->with($courses);
    $excel->export('xls');
});

Route::get('excel', function(){
    return \Acme\Users\User::all();
});


# Resources
Route::get('auth', 'Tappleby\AuthToken\AuthTokenController@index');
Route::post('auth', 'Tappleby\AuthToken\AuthTokenController@store');
Route::delete('auth', 'Tappleby\AuthToken\AuthTokenController@destroy');

Route::group(array(/*'before' => 'auth.token',*/
    'prefix' => 'api'), function () {

    Route::resource('fsrs', 'FsrGoalController',
        array('only' => array('show', 'store')));
    Route::resource('likes', 'LikesController',
        array('only' => array('destroy', 'store', 'index')));
    Route::get('courses', function () {
        return Response::json(Course::get()
        )->header('Access-Control-Allow-Origin', '*');
    });
    Route::resource('courses', 'CoursesApiController');
    Route::get('search', 'ApiSearchController@index');
});


Route::get('@{username}/blocks', [
    'as' => 'userblocks_path',
    'uses' => 'BlocksController@userIndex']);

Route::get('@{username}/courses', [
    'as' => 'usercourses_path',
    'uses' => 'PrivateCoursesController@userIndex']);

Route::get('@{username}', [
    'as' => 'userprofile_path',
    'uses' => 'UsersController@showUserProfile']);

Route::get('{title}', [
    'as' => 'pages.fromTitle',
    'uses' => 'PagesController@fromTitle']);


App::missing(function ($exception) {
    return Redirect::to('/');
});




//Route::group(["before" => "guest"], function()
//{
// $resources = Resource::where("secure", false)->get();

// foreach ($resources as $resource)
// {
//     Route::any($resource->pattern, [
//         "as"   => $resource->name,
//         "uses" => $resource->target
//     ]);
// }
//});

// Route::group(["before" => "auth"], function()
// {
//     $resources = Resource::where("secure", true)->get();

//     foreach ($resources as $resource)
//     {
//         Route::match(array($resource->type), $resource->pattern, [
//         	"as"   => $resource->name,
//             "uses" => $resource->target
//         ]);
//     }
// });
