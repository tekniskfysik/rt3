<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Administrator Language Lines
	|--------------------------------------------------------------------------
	|
	| sv - Swedish
	|
	*/

	'title' => 'Röda Tråden admin',
	'course' => 'Course',
	'course_title' => 'Course title (swedish)', 
	'course_code' => 'Course code',
	'ects' => 'ECTS',
	'department' => 'Departments',
	'categories' => 'Categories',
	'tracks' => 'Tracks',
	'level' => 'Level',
	'occasions' => 'Occasions',
);