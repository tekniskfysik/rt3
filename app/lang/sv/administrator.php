<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Administrator Language Lines
	|--------------------------------------------------------------------------
	|
	| sv - Swedish
	|
	*/

	'course' => 'Kurs',
	'course_title' => 'Kursnamn', 
	'course_code' => 'Kurskod',
	'ects' => 'Högskolepoäng',
	'department' => 'Institutuion',
	'categories' => 'Kategorier',
	'tracks' => 'Spår',
	'level' => 'Nivå',
	'occasions' => 'Tillfällen',
);