<?php namespace Acme\Registration;

use Acme\Users\User;
use Acme\Groups\Group;

use Acme\Users\Repository\DbUserRepository;
use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;

/**
 * Register a user as a member
 *
 * Class RegisterUserCommandHandler
 * @package RT\Registration
 */
class RegisterUserCommandHandler implements CommandHandler{

    use DispatchableTrait;

    /**
     * @var DbUserRepository
     */
    protected  $repository;

	function __construct(DbUserRepository $repository){
		$this->repository = $repository;
		
	}

    /**
     * Handle the command
     *
     * @param $command
     * @return mixed|static
     */
    public function handle($command){
		$user = User::register(
			$command->username, $command->email, $command->password
		);
		
		$this->repository->save($user);
		$this->dispatchEventsFor($user);
		
		//Set default group
		$group = Group::where('name', 'student')->first();
		$user->groups()->attach($group->id);
		
		return $user; 
	}
	
	
}
