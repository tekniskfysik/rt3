<?php
namespace Acme\Registration\Events;

use Acme\Users\User;
use Acme\Groups\Group;

class UserRegistered {
	public $user;
	
	function __construct(User $user){
		$this->user = $user;
	}
}
