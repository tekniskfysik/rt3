<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 23/08/14
 * Time: 22:47
 */

namespace Acme;


use Laracasts\Presenter\Presenter;

abstract class BasePresenter extends Presenter {

    protected $route;

    abstract function setRoute();

    public function __construct($entity){
        parent::__construct($entity);
        $this->setRoute();
    }

    public function url($action = 'show', $id = null){
        if($id != null){
            return route($this->route . '.' . $action, $id);
        }
        return route($this->route . '.' . $action, $this->entity->id);
    }

    public function editUrl(){
        return $this->url('edit');
    }

    public function showUrl(){
        return $this->url('show');
    }

    public function destroyUrl(){
        return $this->url('destroy');
    }

    public function deleteButton($name, $url, $class = '', $message = '') {
        $form = \Form::open(array('url' => $url, 'style' => 'margin:0px; margin-top:-2px; padding:0px; clear: both; float:right;'));
        $form .= \Form::hidden('_method', 'DELETE');
        $class .= 'btn-link';
        if ($message == '')
            $message = 'Delete this item?';
        $form .= '<button style="margin:0; padding:0" type="submit" class="' . $class . '" data-confirm="' . $message . '">';
        $form .= '<i class="glyphicon glyphicon-remove"></i>';
        $form .= $name;
        $form .= '</button>';
        $form .= \Form::close();
        return $form;
    }


    public function link($url = null, $title = null){
        $url = $url ?: $this->url();
        $title = $title ?: $this->entity->title;

        return "<a href={$url}>{$title}</a>";
    }

    public function addButton(){
        return $this->link($this->url('show'), "<i class=\"glyphicon glyphicon-plus\"></i>");
    }

    public function likeButton(){
        return $this->link($this->url('show'), "<i class=\"glyphicon glyphicon-heart\"></i>");
    }

    public function showButton(){
        return $this->link($this->url('show'), "<i class=\"glyphicon glyphicon-eye-open\"></i>");
    }

    public function editButton(){
        return $this->link($this->url('edit'), "<i class=\"glyphicon glyphicon-edit\"></i>");
    }

    public function removeButton(){
        return $this->deleteButton('',$this->url('destroy'));
    }

    public function actionButtons(){
        return "<div style='float:right'>" .$this->showButton() . " " . $this->editButton() . " " . $this->removeButton() . "</div>";
    }


} 