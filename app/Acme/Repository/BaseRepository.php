<?php namespace Acme\Repository;

/**
 * Base repository implementation for Eloquent specific ORM
 *
 * Class DbBaseRepository
 * @package RT
 */
interface BaseRepository
{
    /**
     * Get all records
     *
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = array('*'));

    public function create(array $attributes);

    public function newInstance(array $attributes = ['*']);

    public function lists($column, $key = null);

    /**
     * Find a model by id
     *
     * @param $id
     * @return \Illuminate\Support\Collection|static
     */
    public function find($id);


    /**
     * Get all records with paginator
     *
     * @param null $perPage
     * @param array $columns
     * @return \Illuminate\Pagination\Paginator
     */
    public function paginate($perPage = null, $columns = array());

    public function destroy($ids);

    public function updateWithIdAndInput($id, array $input);


}
