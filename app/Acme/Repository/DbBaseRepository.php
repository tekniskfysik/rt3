<?php namespace Acme\Repository;
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 08/08/14
 * Time: 11:07
 */



/**
 * Base repository implementation for Eloquent specific ORM
 *
 * Class DbBaseRepository
 * @package RT
 */
abstract class DbBaseRepository implements BaseRepository
{

    /**
     * @var String
     */
    protected $modelClassName;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        return call_user_func_array("{$this->modelClassName}::create", array($attributes));
    }


    /**
     * @param array $columns
     * @return \Illuminate\Database\Eloquent\Collection|mixed|static[]
     */
    public function all($columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::all", array($columns));
    }

    public function lists($column, $key = null)
    {
        return call_user_func_array("{$this->modelClassName}::lists", array($column, $key));
    }


    /**
     * @param int $count
     * @param array $columns
     * @return \Illuminate\Pagination\Paginator|mixed
     */
    public function paginate($count = 25, $columns = array('*')){
        return call_user_func_array("{$this->modelClassName}::paginate", array($count, $columns));
    }

    /**
     * @param $id
     * @param array $columns
     * @return \Illuminate\Support\Collection|mixed|static
     */
    public function find($id, $columns = array('*'))
    {
        return call_user_func_array("{$this->modelClassName}::findOrFail", array($id, $columns));
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function destroy($ids)
    {
        return call_user_func_array("{$this->modelClassName}::destroy", array($ids));
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function newInstance(array $attributes = ['*'])
    {
        return call_user_func_array("{$this->modelClassName}::newInstance", array($attributes));
    }


    public function updateWithIdAndInput($id, array $input)
    {
        $instance = $this->find($id);
        $instance->update($input);
        return $instance;
    }
}
