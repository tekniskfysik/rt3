<?php

namespace Acme;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class AcmeServiceProvider
 * @package Acme
 */
class AcmeServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //App::bind('Acme\Repositories\UserRepositoryInterface', '\Acme\Repositories\UserRepository');
        $this->app->bind('Acme\Users\Repository\UserRepositoryInterface', 'Acme\Users\Repository\DbUserRepository');
        $this->app->bind('Acme\Pages\Repository\PageRepositoryInterface', 'Acme\Pages\Repository\DbPageRepository');
        $this->app->bind('Acme\Groups\Repository\GroupRepositoryInterface', 'Acme\Groups\Repository\DbGroupRepository');
    }

    public function boot()
    {
        Route::get('/', ['as' => 'home', 'uses' => 'PagesController@start']);

        # Registration
        Route::get('register', ['as'=> 'register', 'uses' => 'RegistrationController@create']);
        Route::post('register', 'RegistrationController@store');

        # Authentication
        Route::get('login', ['as' => 'login', 'uses' => 'SessionsController@create']);
        Route::get('logout',  ['as' => 'logout', 'uses' => 'SessionsController@destroy']);
        Route::post('login', 'SessionsController@store');


        Route::resource('users', 'UsersController');
        Route::resource('pages', 'PagesController');
        Route::resource('roles', 'RolesController');
        Route::resource('groups', 'GroupsController');

        Route::get('rss', function(){
            return "Not implemented yet....";
        });


        Route::controller('password', 'RemindersController');


        Route::post('follows', [
            'as' => 'follows_path',
            'uses' => 'FollowersController@store'
        ]);

        Route::delete('follows/{id}', [
            'as' => 'follow_path',
            'uses' => 'FollowersController@destroy'
        ]);


        Route::get('comments', function () {
            JavaScript::put(['comment' => ['user_id' => Auth::id(), 'commentable_type' => 'Course', 'commentable_id' => 1]]);
            return View::make('angular.commentsTest');
        });

        Route::get('likes', function () {
            JavaScript::put(['like' => ['user_id' => Auth::id(), 'likable_type' => 'Course', 'likable_id' => 5]]);
            return View::make('angular._like');
        });
    }
}
