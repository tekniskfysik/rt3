<?php namespace Acme\Users\Repository;

use Acme\Repository\DbBaseRepository;
use Acme\Users\User;

class DbUserRepository extends DbBaseRepository implements UserRepositoryInterface
{

    protected $modelClassName = "Acme\Users\User";

    public function findByUsername($username){
        return User::whereUsername($username)->first();
    }

    /**
     * Follow a user
     *
     * @param $userIdToFollow
     * @param User $user
     */
    public function follow($userIdToFollow, User $user)
    {
        return $user->followedUsers()->attach($userIdToFollow);
    }

    /**
     * Unfollow a user
     *
     * @param $userIdToUnfollow
     * @param User $user
     * @return int
     */
    public function unfollow($userIdToUnfollow, User $user)
    {
        return $user->followedUsers()->detach($userIdToUnfollow);
    }

    public function getById($id){
        return $this->model->find($id);
    }

    public function save(User $user){
        $user->save();
    }

}

