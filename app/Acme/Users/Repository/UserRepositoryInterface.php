<?php namespace Acme\Users\Repository;

use Acme\Repository\BaseRepository;
use Acme\Users\User;

interface UserRepositoryInterface extends BaseRepository
{
    public function findByUsername($username);
    public function follow($userIdToFollow, User $user);
    public function unfollow($userIdToUnfollow, User $user);
    public function save(User $user);
}
