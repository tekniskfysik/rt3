<?php namespace Acme\Users;

/**
 * Class FollowableTrait
 * @package Acme\Users
 */
trait FollowableTrait
{
    /**
     * Get the list of users that the current user follows.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followedUsers()
    {
        return $this->belongsToMany('Acme\Users\User', 'follows', 'follower_id', 'followed_id')
            ->withTimestamps();
    }

    /**
     * Get the list of users who follow the current user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followers()
    {
        return $this->belongsToMany('Acme\Users\User', 'follows', 'followed_id', 'follower_id')
            ->withTimestamps();
    }

    /**
     * Return true if this user is followd by $otherUser
     *
     * @param User $otherUser
     * @return bool
     */
    public function isFollowedBy(User $otherUser)
    {
        $idsWhoUserFollows = $otherUser->followedUsers()->lists('followed_id');
        return in_array($this->id, $idsWhoUserFollows);
    }

} 