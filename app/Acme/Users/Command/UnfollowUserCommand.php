<?php namespace Acme\Users\Command;

/**
 * Define data for unfollowing user command
 *
 * Class UnfollowUserCommand
 * @package RT\Users
 */
class UnfollowUserCommand {

    public $userId;

    public $userIdToUnfollow;

    public function __construct($userId, $userIdToUnfollow)
    {
        $this->userId = $userId;
        $this->userIdToUnfollow = $userIdToUnfollow;
    }

}