<?php namespace Acme\Users\Command;

/**
 * Define data for following user command
 *
 * Class FollowUserCommand
 * @package RT\Users
 */
class FollowUserCommand {
	public $userId;
	public $userIdToFollow;
	
	function __construct($userId, $userIdToFollow){
		$this->userId = $userId;
		$this->userIdToFollow = $userIdToFollow;
	}

}
