<?php namespace Acme\Users\Command;

use Laracasts\Commander\CommandHandler;
use Acme\Users\Repository\UserRepositoryInterface;

/**
 * Handle the command to unfollow a users
 *
 * Class UnfollowUserCommandHandler
 * @package RT\Users
 */
class UnfollowUserCommandHandler implements CommandHandler
{

    protected $userRepo;

    function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }


    /**
     * Handle the command.
     *
     * @param UnfollowUserCommand $command
     * @return void
     */
    public function handle($command)
    {

        $user = $this->userRepo->find($command->userId);

        $this->userRepo->unfollow($command->userIdToUnfollow, $user);
    }

}