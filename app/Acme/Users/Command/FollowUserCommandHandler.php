<?php namespace Acme\Users\Command;

use Laracasts\Commander\CommandHandler;
use Acme\Users\Repository\UserRepositoryInterface;

/**
 * Handle the command to follow a users
 *
 * Class FollowUserCommandHandler
 * @package RT\Users
 */
class FollowUserCommandHandler implements CommandHandler{
	

	
	protected  $userRepo;
	
	function __construct(UserRepositoryInterface $userRepo){
		$this->userRepo = $userRepo;
		
	}

    /**
     * The command
     *
     * @param $command
     * @return \Illuminate\Support\Collection|mixed|static
     */
    public function handle($command){
		
		$user = $this->userRepo->find($command->userId);
		
		$this->userRepo->follow($command->userIdToFollow, $user);
		
		return $user;
		
	}
	
	
}
