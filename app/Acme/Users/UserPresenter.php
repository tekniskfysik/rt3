<?php namespace Acme\Users;


use Acme\BasePresenter;

/**
 * Presenter for user.
 *
 * Class UserPresenter
 * @package Acme\Users
 */
class UserPresenter extends BasePresenter
{

    function setRoute()
    {
        $this->route = 'users';
    }

    /**
     * Get the avatar link from gravatar.com
     *
     * @param int $size
     * @return string
     */
    public function gravatar($size = 30)
    {
        $email = md5($this->email);
        return "//www.gravatar.com/avatar/{$email}?s={$size}";
    }

    /**
     * Returns the number of follower for this user
     *
     * @return string
     */
    public function followerCount()
    {
        $followersCount = $this->entity->followers()->count();
        $plural = str_plural('Follower', $followersCount);

        return "{$followersCount} {$plural}";

    }



    public function link($url = null, $title = null){
        return parent::link(null, $this->entity->username);
    }

    public function title(){
        return $this->entity->username;
    }
}
