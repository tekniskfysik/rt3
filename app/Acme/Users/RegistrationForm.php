<?php namespace Acme\Users;

use Laracasts\Validation\FormValidator;
class RegistrationForm extends FormValidator{

	protected $rules = [
		'username' => 'required|unique:users',
		'email' => 'required|email|unique:users',
		'password' => 'required|confirmed',
		'captcha' => 'required|accepted'
	];

	protected $messages = [
		'email.email' => 'Det måste vara en riktig mailadress.',
		'captcha.accepted' => 'Felaktig captcha.'
	];
	
	public function setRules($newRules) {
		if(array_key_exists('username', $newRules)) 
			$this->rules['username'] = $newRules['username'];
		if(array_key_exists('email', $newRules))
			$this->rules['email'] = $newRules['email'];
		if(array_key_exists('password', $newRules)) 
			$this->rules['password'] = $newRules['password'];
	}
	public function getRules() {
		echo $this->rules;
	}
}
