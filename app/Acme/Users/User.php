<?php namespace Acme\Users;

use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Acme\Registration\Events\UserRegistered;
use Laracasts\Commander\Events\EventGenerator;
use Laracasts\Presenter\PresentableTrait;
use RT\Courses\Course;


/**
 * An Eloquent Model: 'User'
 *
 * @property integer $id
 * @property integer $username
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Group $groups
 * @property-read \Like $likes
 * @property string $name
 * @property string $remember_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\Block[] $blocks
 * @property-read \Illuminate\Database\Eloquent\Collection|\RT\Users\User[] $followedUsers
 * @property-read \Illuminate\Database\Eloquent\Collection|\RT\Users\User[] $
 * followers
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereUsername($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User wherePassword($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereRememberToken($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\RT\Users\User whereUpdatedAt($value) 
 */

/**
 * Class User
 * @package Acme\Users
 */
class User extends \BaseModel implements UserInterface, RemindableInterface
{

	use UserTrait, RemindableTrait, EventGenerator, PresentableTrait, FollowableTrait;

	protected $hidden = array('password', 'remember_token');
	protected $fillable = ['username', 'email', 'password', 'name', 'public'];
	protected $presenter = 'Acme\Users\UserPresenter';

	/**
	 * Block schedule that this user owns
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphMany
	 */
	public function blocks()
	{
		return $this->morphMany('RT\Blocks\Block', 'blockable');
	}


/**
* Private courses that this users has created
*
* @return \Illuminate\Database\Eloquent\Relations\HasMany
*/
	public function privateCourses()
	{
		return $this->hasMany('RT\PrivateCourses\PrivateCourse');
	}

	public function groups()
	{
		return $this->belongsToMany("Acme\Groups\Group")->withTimestamps();
	}
	public function likes()
	{
		return $this->hasMany("Like");
	}

	public function likedCourses()
	{
		return $this->likes()->where('likable_type', '=', 'RT\Courses\Course');
	}

	/**
	* Register a new user
	*
	* @param $username
	* @param $email
	* @param $password
	* @return static
	*/
	public static function register($username, $email, $password)
	{
		$user = new static(compact('username', 'email', 'password'));
		$user->raise(new UserRegistered($user));

		return $user;
	}

	/**
	* Hash the password before saving it to the database
	*
	* @param $password
	*/
	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

	/**
	* Compare this user to another user, return true if they have same id.
	*
	* @param User $user
	* @return bool
	*/
	public function is(User $user = null)
	{
		if (is_null($user))
			return false;
		return $this->id == $user->id;
	}

	public function inGroup($group){
		switch($group){
			case('admin'):
				return $this->isAdmin();
			case('ledning'):
				return $this->isLedning();
			case('editor'):
				return $this->isEditor();
		}
	}

	private function isInGroup($group){
		return in_array($group, \Auth::user()->groups->lists('name'));
	}

	public function isAdmin(){
		return $this->isInGroup('admin');
	}

	public function isLedning(){
		return $this->isInGroup('ledning') || $this->isAdmin();
	}

	public function isEditor(){
		return $this->isInGroup('editor') || $this->isLedning();
	}

	public function isStudent(){
		return $this->isInGroup('student') || $this->isEditor();
	}

}
