<?php
namespace Acme\Statuses;

use Laracasts\Commander\CommandHandler;
use Laracasts\Commander\Events\DispatchableTrait;


class PublishStatusCommandHandler implements CommandHandler{
	
	use DispatchableTrait;
	
	protected $statusRepository;
	
	function __construct(StatusRepository $repository){
		$this->statusRepository = $repository;
	}
	
	
	public function handle($command){
		
		$status = Status::publish($command->body);
		
		$status = $this->statusRepository->save($status, $command->userId);
		
		$this->dispatchEventsFor($status);
		
		return $status;
		
	}
}	