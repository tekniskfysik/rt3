<?php namespace Acme\Statuses;

class PublishStatusCommand {
	public $body;
	public $userId;
	
	function __construct($body, $user_id){
		$this->body = $body;
		$this->userId = $user_id;
	}
	
}
