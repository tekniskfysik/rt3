<?php namespace Acme\Statuses;


use Acme\BasePresenter;

class StatusPresenter extends BasePresenter {
	
	public function timeSincePublished(){
		return $this->created_at->diffForHumans();
	}

    function setRoute()
    {
        $this->route = 'statuses';
    }


}