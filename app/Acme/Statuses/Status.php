<?php namespace Acme\Statuses;

use Laracasts\Commander\Events\EventGenerator;

use Acme\Statuses\Events\StatusWasPublished;

use Laracasts\Presenter\PresentableTrait;

class Status extends \Eloquent {
	
	use EventGenerator, PresentableTrait;

	protected $fillable = ["body"];
	
	protected $presenter = 'Acme\Statuses\StatusPresenter';
	
	public function user()
	{
		return $this->belongsTo("Acme\Users\User");
	}
	
	public static function publish($body){
		$status = new static(compact('body'));
				
		$status->raise(new StatusWasPublished($body));
		
		return $status;
	}

}