<?php namespace Acme\Pages;
use Laracasts\Presenter\PresentableTrait;

/**
 * Page
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Page whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Page whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Page whereImage($value) 
 * @method static \Illuminate\Database\Query\Builder|\Page whereContent($value) 
 * @method static \Illuminate\Database\Query\Builder|\Page whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Page whereUpdatedAt($value) 
 */
class Page extends \BaseModel {

	protected $fillable = ['title', 'content', 'slug'];

    use PresentableTrait;
    protected $presenter = 'Acme\Pages\PagePresenter';

}