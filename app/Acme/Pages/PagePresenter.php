<?php

namespace Acme\Pages;

use Acme\BasePresenter;

class PagePresenter extends BasePresenter
{
    function setRoute()
    {
        $this->route = 'pages';
    }

    public function showButton(){
        return $this->link($this->url('fromTitle', $this->entity->slug), "<i class=\"glyphicon glyphicon-eye-open\"></i>");
    }

} 