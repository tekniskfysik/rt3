<?php namespace Acme\Pages\Repository;

use Acme\Repository\DbBaseRepository;

class DbPageRepository extends DbBaseRepository implements PageRepositoryInterface
{
    protected $modelClassName = "Acme\Pages\Page";

}