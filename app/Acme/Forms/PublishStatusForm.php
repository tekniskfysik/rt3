<?php namespace Acme\Forms;

use Laracasts\Validation\FormValidator;
class PublishStatusForm extends FormValidator{

	/*
	 * Validation form for the registration form
	*/

	protected $rules = [
		'body' => 'required'
	];

}
