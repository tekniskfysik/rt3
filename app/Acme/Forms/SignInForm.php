<?php namespace Acme\Forms;

use Laracasts\Validation\FormValidator;
class SignInForm extends FormValidator{

	/*
	 * Validation form for the registration form
	 */
	
	protected $rules = [
		'email' => 'required',
		'password' => 'required',
		'captcha' => 'required|accepted'
	];
}
