<?php

namespace Acme;

class BaseFilterPresenter implements FilterPresenter{

    protected $grid;
    protected $filter;
    protected $prepared = false;
    protected $filterCount = 0;

    public function __construct($source, array $attributes = []){
        $this->filter = \DataFilter::source($source);
        $this->filter->attributes(array('class'=>'form-inline', 'id' => 'courseOccasionFilterForm'));
        $this->filter->attributes(array('class' => 'form-inline'));

    }

    public function addFilter($column, $title = null, $type = 'text'){
        if($title == null){
            $title = $column;
        }
        $this->filterCount++;
        return $this->filter->add($column, $title, $type);
    }


    public function add($column, $title = "", $sortable = false){
        if(!$this->prepared){
            $this->grid = \DataGrid::source($this->filter);
            $this->grid->attributes(['class' => 'table table-striped table-condensed table-hover']);
            $this->prepared = true;
        }
        $this->grid->add($column,$title, $sortable);
    }

    public function link($url, $title, $pos){
        $this->grid->link($url, $title, $pos);
    }

    public function present(){
        $this->grid->add('actionButtons|html','');
        $this->grid->paginate(15);
        if($this->filterCount){
            return ['grid' => $this->grid, 'filter' => $this->filter];
        }
        return  ['grid' => $this->grid];

    }

    public function filter()
    {
        // TODO: Implement filter() method.
    }

    public function json(array $columns = [])
    {
        // TODO: Implement json() method.
    }
}