<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 29/12/14
 * Time: 14:18
 */

namespace Acme\Services;


class HighChart {

    public $colors = ["#555", '#d5001c', '#90ed7d' , '#f7a35c', '#8085e9'];

    protected $chartArray;

    public function __construct($type = 'column', $title = "Total User Assets"){
        $this->chartArray["credits"] = array("enabled" => false);
        $this->chartArray["navigation"] = array("buttonOptions" => array("align" => "right"));
        //$this->chartArray["tooltip"]["pointFormat"] = '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>';
    }

    public function bar($data, $title = "title", $xAxis = "x", $yAxis = "y", $options = []){
        return $this->column($data, $title, $xAxis, $yAxis, 'bar', $options);
    }

    public function line($data, $title = "title", $xAxis = "x", $yAxis = "y", $options = []){
        return $this->base($data, $title, $xAxis, $yAxis, 'line', $options);
    }

    public function scatter($data, $title = "title", $xAxis = "x", $yAxis = "y", $options = []){
        return $this->base($data, $title, $xAxis, $yAxis, 'scatter', $options);
    }

    public function column($data, $title = "title", $xAxis = "x", $yAxis = "y", $options = []){
        return $this->base($data, $title, $xAxis, $yAxis, 'column', $options);
    }



    // data:  [1,2,3]
    // series:              ---     ['data'=> [1,2,3], 'type' => 'spline', yAxis => 0, xAxis => 0]    ---     [ [], [] ]
    // xaxis: 'Month'       ---     ['title' => 'Month', 'categories' => ['Jan', 'Feb']]        ---     [ [], [] ]
    // yaxis: 'Temperature' ---     ['title' => 'Temperature', 'format' => '{value}']           ---     [ [], [] ]
    public function plot(array $data, $title = "", $options = null){
        if(array_has($data, 'data')){
            if(!array_has($data, 'type')){
                $data['type'] = 'spline';
            }
            $series = [['data' => $data['data'], 'type' => $data['type'], 'name' => $title]];
        }else{
            $series = $data['series'];
        }
        if(array_has($data, 'xaxis')){
            if(!is_array($data['xaxis'])){
                $data['xaxis'] = [['title' => $data['xaxis']]];
            }
        }else{
            $data['xaxis'] = [['title' => '']];
        }
        if(array_has($data, 'yaxis')){
            if(!is_array($data['yaxis'])){
                $data['yaxis'] = [['title' => $data['yaxis']]];
            }
        }
        else{
            $data['yaxis'] = [['title' => '']];
        }


        $chartArray = $this->chartArray;
        $i = 0;
        foreach($series as $key => $serie){
            if(!array_has($serie, 'yAxis')){
                $serie['yAxis'] = 0;
            }
            $serie['color'] = $this->colors[$i % 5];

            if(count($data['yaxis']) > 1){
                $serie['borderColor'] = $this->colors[$serie['yAxis']];
                $serie['borderWidth'] = 3;
            }
            $series[$key] = $serie;
            $i++;
        }

        $chartArray['series'] = $series;
        $chartArray["title"] = array("text" => $title);
        $chartArray["legend"]["symbolWidth"] = 60;

        $index = 0;
        foreach($data['xaxis'] as $axis){
            $chartArray["xAxis"][] =  [
                "title" => ["text" => $axis['title']],
                "categories" => array_has($axis, 'categories') ? $axis['categories'] : "",
                "opposite" => $index > 0 ? true : false
            ];
            $index++;
        }

        $index = 0;
        foreach($data['yaxis'] as $axis){
            $chartArray["yAxis"][] =  [
                "title" => ["text" => $axis['title'], "style" => ["color" => $this->colors[$index]]],
                "labels" => ["format" => array_has($axis, 'format') ? $axis['format'] : "", "style" => ["color" =>  $this->colors[$index]]],
                "opposite" => $index > 0 ? true : false
            ];
            $index++;
        }

        $this->chartArray = $chartArray;
        return $chartArray;

    }

    public function base(array $data, $title = "title", $xAxis = "x", $yAxis = "y", $types = 'column', $options = []){

        $series = $data['data'];
        if(! is_array($types)){
            $types = array_fill(0, count($series), $types);
        }

        $cats = [];
        if(array_has($data, 'categories')){
            $cats = $data['categories'];
        }

        $chartArray = $this->chartArray;
        $chartArray["title"] = array("text" => $title);

        $i = 0;
        foreach($series as $key => $data){
            $chartArray["series"][] = ["name" => $key, "data" => $data,  "type" => $types[$i]];
            $i++;
        }

        $chartArray["xAxis"] = [
            "title" => ["text" => $xAxis],
            "categories" => $cats
        ];
        $chartArray["yAxis"] = ["title" => ["text" => $yAxis]];
        $chartArray += $options;
        $this->chartArray = $chartArray;
        return $this;
    }

    public function shared(){
        $this->chartArray["tooltip"]["shared"] = true;
        return $this;
    }
    public function stacked($type = 'normal' /* percent */){
        $this->chartArray["plotOptions"]["column"]["stacking"] = $type;
        return $this;
    }



    public static function render($chart){
       // print_r($chart->chartArray);
        return \View::make('charts/test')->with("chartArray", $chart);
    }


    public function pie($data, $title = ""){
        $series = $data['data'];
        $cats = [];
        if(array_has($data, 'categories')){
            $cats = $data['categories'];
        }

        $chartArray = $this->chartArray;
        foreach($series as $key => $data){
            $o["name"] = $key;
            foreach($data as $index => $val){
                $o["data"][] = [$cats[$index], $val];
            }
            $o["innerSize"] = "50%";
            $o["type"] = "pie";
            $chartArray["series"][] = $o;
            break;
        }

        $chartArray["title"]  = ["text" => $title, "y" => 100, "verticalAlign" => "middle"];
        //$chartArray["plotOptions"]["pie"] = ["startAngle" => -90, "size" => '100%', "endAngle" => 90,  "center" => ['50%', '50%']];
        $chartArray["plotOptions"]["pie"]["showInLegend"] = true;
        //$chartArray["plotOptions"]["pie"]["dataLabels"] = ["format" => '<b>{point.name}</b>: {point.y}'];
        $chartArray["legend"] = ["verticalAlign" => 'bottom'];
        $chartArray["chart"]["marginBottom"] = 100;

        $this->chartArray = $chartArray;
        return $this;
    }



} 
