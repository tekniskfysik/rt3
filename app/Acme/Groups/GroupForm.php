<?php namespace Acme\Groups;

use Laracasts\Validation\FormValidator;
class GroupForm extends FormValidator{

    protected $rules = [
        'name' => 'required',
    ];

}
