<?php namespace Acme\Groups\Repository;

use Acme\Groups\Group;
use Acme\Repository\DbBaseRepository;

class DbGroupRepository extends DbBaseRepository implements GroupRepositoryInterface
{
    protected $modelClassName = "Acme\Groups\Group";

    public function createWithRelations(array $attributes)
    {
        $instance = parent::create($attributes);
        $this->updateRelations($instance, $attributes);
        return $instance;
    }

    public function updateWithRelations(Group $instance, array $attributes)
    {
        $instance->update($attributes);
        $this->updateRelations($instance, $attributes);
    }

    private function updateRelations(Group $instance, array $attributes)
    {

        if (array_key_exists('users', $attributes)) {
            $instance->users()->sync($attributes['users']);
        }
        else{
            $instance->users()->sync(array());
        }

    }

}

