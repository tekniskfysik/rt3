<?php namespace Acme\Groups\Repository;

use Acme\Groups\Group;
use Acme\Repository\BaseRepository;

interface GroupRepositoryInterface extends BaseRepository
{

    public function createWithRelations(array $attributes);

    public function updateWithRelations(Group $fsr, array $attributes);

}