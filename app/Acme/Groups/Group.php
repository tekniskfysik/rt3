<?php namespace Acme\Groups;
use Laracasts\Presenter\PresentableTrait;

/**
 * Group
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Resource[] $resources
 * @property-read \Illuminate\Database\Eloquent\Collection|\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\Group whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Group whereName($value) 
 * @method static \Illuminate\Database\Query\Builder|\Group whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Group whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Group whereDeletedAt($value) 
 */
class Group
extends \BaseModel
{
    use PresentableTrait;
    protected $presenter = 'Acme\Groups\GroupPresenter';

    protected $table = "groups";

    protected $softDelete = true;

    protected $fillable = ['name', 'user_id'];

    public function users()
    {
        return $this->belongsToMany('Acme\Users\User');
    }
}
