<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 19/09/14
 * Time: 09:38
 */

namespace Acme;


use Illuminate\Support\Collection;

class CrudButtonCreator {

    private $quiet = false;

    function make($route, $level){
        $a = ['All'];

        // route: create or index
        switch ($level){
            case 'admin':
            case 'editor':
                $a[] = 'Create';
        }

        if($route == 'index' || $route == 'create')
            return $a;

        // route: show, edit, delete; bounded to a specific model
        $a[] = 'Show';
        switch ($level){
            case 'admin':
                $a[] = 'Delete';
            case 'editor':
                $a[] = 'Edit';
        }

        return $a;
    }

    public function setQuiet($quiet = true){
        $this->quiet = $quiet;
    }

    function makeButtons($model, $route, $level = 'viewer'){
        $a = $this->make($route, $level);
        $buttons = [];

        if(in_array('All', $a)){
            $buttons['Alla'] = [
                'icon' => 'glyphicon-th-list',
                'route' => $model . '.index'
            ];
        }
        if(in_array('Create', $a)){
            $buttons['Skapa'] = [
                'icon' => 'glyphicon-plus',
                'route' => $model . '.create'
            ];
        }
        if(in_array('Show', $a)){
            $buttons['Visa'] = [
                'icon' => 'glyphicon-eye-open',
                'route' => $model . '.show'
            ];
        }
        if(in_array('Edit', $a)){
            $buttons['Ändra'] = [
                'icon' => 'glyphicon-edit',
                'route' => $model . '.edit'
            ];
        }
        if(in_array('Delete', $a)){
            $buttons['Ändra'] = [
                'icon' => 'glyphicon-remove',
                'route' => $model . '.destroy'
            ];
        }

        return $buttons;
    }

    function render($model, $route, $level = 'viewer', $id = null){
        if($this->quiet)
            return null;
        $links = new Collection();
        foreach($this->makeButtons($model, $route, $level) as $title => $button){
            try{
                $links->push((object)[
                    'icon' => $button['icon'],
                    'link' => ($id==null)? route($button['route']): route($button['route'], $id),


                    'tooltip' => $title,
                    'class' => ((\Route::is($button['route'])) ? 'active ' : '')]);
            } catch(\InvalidArgumentException $e){
            }

        }

        if(count($links) < 2)
            return null;
        return $links;
    }
}