<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 26/12/14
 * Time: 18:08
 */

namespace Acme;

interface FilterPresenter {
    public function present();
    public function filter();
    public function json(array $columns = []);
} 