<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.admin', function()
{
	$user = Auth::user(); 
	if(!$user || !$user->isAdmin()){
        Flash::error('Du har inte behörighet hit.');
        return Redirect::back();
    }
});

Route::filter('auth.ledning', function()
{
    $user = Auth::user();
    if(!$user || !$user->isLedning())
    {
        Flash::error('Du har inte behörighet hit.');
        return Redirect::back();
    }
});

Route::filter('auth.editor', function()
{
    $user = Auth::user();
    if(!$user || !$user->isEditor()){
        Flash::error('Du har inte behörighet hit.');
        return Redirect::back();
    }
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});


Route::filter('course.editor', function($request, $route, $param)
{
    if(!in_array($param, Auth::user()->courses->lists('id'))){
        return Response::make('Unauthorized', 401);
    }
});


Route::filter('block.editor', function($request, $route, $param)
{
	$owner = RT\Blocks\Block::find($param)->blockable_id;
	if (!($owner == Auth::user()->id)) 
	{
		return Response::make('Unauthorized', 401);
	};
});


Route::filter('user.editor', function($request, $route, $param)
{
	if (!($param == Auth::user()->id)) 
	{
		return Response::make('Unauthorized', 401);
	};
});



// filters.php
App::after(function($request, $response)
{
	 // HTML Minification
	 if(App::Environment() != 'local')
	 {
		 if($response instanceof Illuminate\Http\Response)
		 {
			$output = $response->getOriginalContent();
			 // Clean comments
			$output = preg_replace('/<!--([^\[|(<!)].*)/', '', $output);
			$output = preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $output);
			 // Clean Whitespace
			$output = preg_replace('/\s{2,}/', '', $output);
			$output = preg_replace('/(\r?\n)/', '', $output);
			$response->setContent($output);
		 }
	 }
});



 
// Filters
Route::filter('cache', function($route, $request, $response = null)
{
	if(App::Environment() != 'local')
	{
		$key = 'route-'.Str::slug(Request::url());
		if(is_null($response) && Cache::has($key))
		{
		 	return Cache::get($key);
		}
		elseif(!is_null($response) && !Cache::has($key))
		{
		 	Cache::put($key, $response->getContent(), 30);
		}
	}
});


Route::filter('force.ssl', function()
{
    if( ! Request::secure())
    {
        return Redirect::secure(Request::path());
    }

});

