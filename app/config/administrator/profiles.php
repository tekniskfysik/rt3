<?php

/**
 * User model config
 */

return array(

	'title' => 'Profiler',

	'single' => 'profil',

	'model' => 'Profiles',

	/**
	 * The display columns
	 */
	'columns' => array(
		'title',
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title',
		'desc' => array(
			'title' => 'Beskrivning',
			'type' => 'wysiwyg'
			),
	),

	'form_width' => 600,
);