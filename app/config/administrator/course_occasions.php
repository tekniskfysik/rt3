<?php

/**
 * User model config
 */

return array(

	'title' => 'Kurstillfällen',

	'single' => 'kurstillfälle',

	'model' => 'CourseOccasion',


	/**
	 * The display columns
	 */
	'columns' => array(
		'course' => array(
			'title' => 'Kurs',
			'relationship' => 'course',
			'select' => '(:table).title',
		),
		'year',
		'start',
		'weeks',
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'course' => array(
			'title' => 'Kurs',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'year',
		'start',
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'course' => array(
			'title' => 'Course',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'blocks' => array(
			'title' => 'Blocks',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'year',
		'start',
		'weeks',
		'contact_name',
		'contact_email',
	),
);