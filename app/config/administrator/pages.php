<?php

/**
 * User model config
 */

return array(

	'title' => 'Sidor',

	'single' => 'Sida',

	'model' => 'Page',

	/**
	 * The display columns
	 */
	'columns' => array(
		'title',
		'image', 
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title',
		'image' => array(
			'title' => 'Image',
			'type' => 'image',
			'location' => public_path() . '/uploads/products/originals/',
			'naming' => 'random',
			'length' => 20,
			'size_limit' => 2,
		),

		'content' => array(
			'title' => 'Beskrivning',
			'type' => 'wysiwyg'
			),
	),

	'form_width' => 600,
);