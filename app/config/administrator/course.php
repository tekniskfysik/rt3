<?php

/**
 * User model config
 */

return array(

	'title' => 'Kurser',

	'single' => 'kurs',

	'model' => 'Course',

	/**
	 * The display columns
	 */
	'columns' => array(
		'title' => array('title' => trans('administrator.course_title'),),
		'code' => array('title' => trans('administrator.course_code')),
		'ects' => array('title' => trans('administrator.ects')),
		'level' => array(
			'title' => trans('administrator.level'),
			'relationship' => 'level',
			'select' => '(:table).title',
		),
		'occasions' => array(
			'title' => trans('administrator.occasions'),
			'output' => '<a href="' . URL::to('admin/course_occasions') .'">Link</a>'

		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title' => array('title' => trans('administrator.course_title'),),
		'code' => array('title' => trans('administrator.course_code')),
		'ects' => array('title' => trans('administrator.ects')),
		'department' => array(
			'title' => trans('administrator.department'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'categories' => array(
			'title' => trans('administrator.categories'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'tracks' => array(
			'title' => trans('administrator.tracks'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'level' => array(
			'title' => trans('administrator.level'),
			'type' => 'relationship',
			'name_field' => 'title',
		),

	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title' => array('title' => 'Kursnamn'),
		'code' => array('title' => 'Kurskod'),
		'ects' => array('title' => 'Högskolepoäng'),
		'department' => array(
			'title' => 'Institution',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'desc' => array(
			'title' => 'Beskrivning',
			'type' => 'wysiwyg'
			),
		'prerequisites' => array(
			'title' => 'Förkunskaper',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'categories' => array(
			'title' => 'Kategorier',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'tracks' => array(
			'title' => 'Spår',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'title_eng' => array('title' => 'Kursnamn (eng)'),
	),

	'form_width' => 600,
);