<?php

/**
 * User model config
 */

return array(

	'title' => 'Spår',

	'single' => 'spår',

	'model' => 'Track',

	/**
	 * The display columns
	 */
	'columns' => array(
		'title',
		'profile' => array(
			'title' => trans('administrator.profile'),
			'relationship' => 'profile',
			'select' => '(:table).title',
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title',
		'profile' => array(
			'title' => trans('administrator.profile'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title',
		'desc' => array(
			'title' => 'Beskrivning',
			'type' => 'wysiwyg'
			),
		'profile' => array(
			'title' => trans('administrator.profile'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
	),

	'form_width' => 600,
);