<?php

/**
 * User model config
 */

return array(

	'title' => 'Institutioner',

	'single' => 'institution',

	'model' => 'Departments',

	/**
	 * The display columns
	 */
	'columns' => array(
		'title',
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title',
		'desc' => array(
			'title' => 'Beskrivning',
			'type' => 'wysiwyg'
			),
		'title_eng',
		'desc_eng'
	),

	'form_width' => 600,
);