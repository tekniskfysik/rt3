<?php

/**
 * User model config
 */

return array(

	'title' => 'Users',

	'single' => 'user',

	'model' => 'User',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'username',
		'email',
		'created_at',
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'username', 
		'email',
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'username',
		'email',
	),

);