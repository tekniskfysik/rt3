<?php

/**
 * User model config
 */

return array(

	'title' => 'Block - Tillfälle',

	'single' => 'block tillfälle',

	'model' => 'BlockCourseOccasion',

	// 'query_filter' => function($query){
	// 	$query->where('blockable_type', '=', 'Track');
	// },

	/**
	 * The display columns
	 */
	'columns' => array(
		'block' => array(
			'title' => 'Block',
			'relationship' => 'block',
			'select' => '(:table).title',
		),
		'course' => array(
			'title' => 'Kurs',
			'relationship' => 'course_occasion.course',
			'select' => '(:table).title',
		),
		'year' => array(
			'title' => 'År',
			'relationship' => 'course_occasion',
			'select' => '(:table).year',
		),
		'start' => array(
			'title' => 'Start',
			'relationship' => 'course_occasion',
			'select' => '(:table).start',
		),
	),
	/**
	 * The filter set
	 */
	'filters' => array(

	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'block' => array(
			'title' => 'Block',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'course_occasion' => array(
			'title' => 'Tillfälle',
			'type' => 'relationship',
			'name_field' => 'title'
		),

	),

	'form_width' => 600,
);