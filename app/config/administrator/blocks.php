<?php

/**
 * User model config
 */

return array(

	'title' => 'Track - Blocks',

	'single' => 'block',

	'model' => 'Block',

	'query_filter'=> function($query){
	 	$query->where('blockable_type' , '=', 'Track');
	 },

	/**
	 * The display columns
	 */
	'columns' => array(
		'title' => array('title' => trans('administrator.model_title')),
		'note' => array('title' => trans('administrator.note')),
		'blockable' => array(
			'title' => trans('administrator.blockable'),
			'relationship' => 'track',
			'select' => '(:table).title',
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'title' => array('title' => trans('administrator.model_title')),
	),


	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title' => array('title' => trans('administrator.model_title')),
		'track' => array(
			'title' => trans('administrator.track'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'courseoccasions' => array(
			'title' => trans('administrator.track'),
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'blockable_type' => array(
			'visible' => false,
			'value' => 'Track', 
		),
		'note' => array(
			'title' => trans('administrator.note'),
			'type' => 'wysiwyg'
			),
	),

	'form_width' => 600,
);