<?php

/**
 * User model config
 */

return array(

	'title' => 'Kurskategorier',

	'single' => 'Kurskategori',

	'model' => 'CategoryCourse',

	/**
	 * The display columns
	 */
	'columns' => array(
		'course' => array(
			'title' => 'Kurs',
			'relationship' => 'course',
			'select' => '(:table).title',
		),
		'category' => array(
			'title' => 'Kurs',
			'relationship' => 'category',
			'select' => '(:table).title',
		),
		'etcs',
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'course' => array(
			'title' => 'Kurs',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'category' => array(
			'title' => 'Kategori',
			'type' => 'relationship',
			'name_field' => 'title',
		),
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'course' => array(
			'title' => 'Course',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'category' => array(
			'title' => 'Kategori',
			'type' => 'relationship',
			'name_field' => 'title',
		),
		'etcs'
	),
);