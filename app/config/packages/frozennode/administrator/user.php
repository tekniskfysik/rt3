<?php

/**
 * Films model config
 */

return array(

	'title' => 'Users',

	'single' => 'user',

	'model' => 'User',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'title'
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'title'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'title'
	),

);