<?php


return array(

    /*
    |--------------------------------------------------------------------------
    | Field Class
    |--------------------------------------------------------------------------
    |
    |
    */
    'field'=> array(
        'attributes' => array('class'=>'selectpicker show-tick', 'onchange'=>"this.form.submit()", 'data-live-search'=>"true"),
    ),
    'lang' => 'de',

);
