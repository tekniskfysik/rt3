<?php
// return array(
//   'default' => 'sqlite',
//   'connections' => array(
//     'sqlite' => array(
//       'driver'   => 'sqlite',
//       'database' => ':memory:',
//       'prefix'   => ''
//     ),
//   )
// );


return array(
  'default' => 'sqlite',
  'connections' => array(
    'sqlite' => array(
      'driver'   => 'sqlite',
		'database' => __DIR__.'/../../tests/_data/db.sqlite',
		'prefix'   => '',
    ),
  )
);
