<?php


HTML::macro('ullink', function ($array, $route) {
    $html = '<ul>';
    foreach ($array as $element) {
        $html .= "<li><a href=\"$route/$element->id\">$element->title</a></li>";
    }
    return $html . '</ul>';
});



Form::macro("formGroup", function ($field, $label, $class = "") {
    $form = '<div class="form-group">';
    $form .= '  <label for="'
        . $label
        . '" class="col-lg-2 control-label">'
        . $label
        . '</label>';
    $form .= '  <div class="col-lg-10 ' . $class . '">';
    $form .= $field;
    $form .= '  </div>';
    $form .= '</div>';
    return $form;
});


Form::macro("field", function ($options) {
    $markup = "";

    $type = "text";

    if (!empty($options["type"])) {
        $type = $options["type"];
    }

    if (empty($options["name"])) {
        return;
    }

    $name = $options["name"];

    $label = "";

    if (!empty($options["label"])) {
        $label = $options["label"];
    }

    $value = Input::old($name);

    if (!empty($options["value"])) {
        $value = Input::old($name, $options["value"]);
    }

    $placeholder = "";

    if (!empty($options["placeholder"])) {
        $placeholder = $options["placeholder"];
    }

    $class = "";

    if (!empty($options["class"])) {
        $class = " " . $options["class"];
    }

    $parameters = [
        "class" => "form-control" . $class,
        "placeholder" => $placeholder
    ];

    $error = "";

    if (!empty($options["form"])) {
        $error = $options["form"]->getError($name);
    }

    if ($type !== "hidden") {
        $markup .= "<div class='form-group";
        $markup .= ($error ? " has-error" : "");
        $markup .= "'>";
    }

    switch ($type) {
        case "text": {
            $markup .= Form::label($name, $label, [
                "class" => "control-label"
            ]);

            $markup .= Form::text($name, $value, $parameters);

            break;
        }

        case "password": {
            $markup .= Form::label($name, $label, [
                "class" => "control-label"
            ]);

            $markup .= Form::password($name, $parameters);

            break;
        }

        case "checkbox": {
            $markup .= "<div class='checkbox'>";
            $markup .= "<label>";
            $markup .= Form::checkbox($name, 1, (boolean)$value);
            $markup .= " " . $label;
            $markup .= "</label>";
            $markup .= "</div>";
            break;
        }

        case "hidden": {
            $markup .= Form::hidden($name, $value);
            break;
        }
    }

    if ($error) {
        $markup .= "<span class='help-block'>";
        $markup .= $error;
        $markup .= "</span>";
    }

    if ($type !== "hidden") {
        $markup .= "</div>";
    }

    return $markup;
});


Form::macro('multiselect', function ($name, $list = [], array $selected = [], $options = []) {

    $options['name'] = $name;
    $html = array();
    if (is_string($selected)) $selected[$selected] = $selected;
    //dd($list, $selected);
    foreach ($list as $value => $display) {
        $sel = isset($selected[$value]) ? ' selected="selected"' : '';
        $html[] = '<option value="' . $value . '"' . $sel . '>' . e($display) . '</option>';
    }

    // build out a final select statement, which will contain all the values.
    $options = HTML::attributes($options);

    $list = implode('', $html);

    return "<select{$options} multiple=\"multiple\">{$list}</select>";

});





HTML::macro('dataTable', function ($fields = array(), $header = null, $data = array(), $resource) {
    $table = '<table id="example" class="display" cellspacing="0" width="100%">';
    if ($header != null) {
        $table .= '<thead><tr>';
        foreach ($header as $field) {
            $table .= '<th>' . Str::title($field) . '</th>';
        }
        $table .= '</tr></thead>';
        $table .= '<tfoot><tr>';
        foreach ($header as $field) {
            $table .= '<th>' . Str::title($field) . '</th>';
        }
        $table .= '</tr></tfoot>';
    }
    $table .= "<tbody>";
    foreach ($data as $d) {
        $table .= "<tr>";
        foreach ($fields as $key) {
            $table .= '<td>' . $d->$key . '</td>';
        }
        $table .= "</tr>";
    }
    $table .= "</tbody></table>";
    return $table;
});

// Macro
HTML::macro('table', function (
    $fields = array(), $header = null, $data = array(), $resource,
    $showEdit = true, $showDelete = true, $showView = true
) {
    $table = '<input type="search" id="search" value="" class="form-control" placeholder="Search using Fuzzy searching">';
    $table ='';
    $table .= '<table class="table table-striped table-hover table-condensed" id="table">';
    if ($header != null) {
        $table .= '<tr>';
        foreach ($header as $field) {
            $table .= '<th>' . Str::title($field) . '</th>';
        }
        if ($showEdit || $showDelete || $showView)
            $table .= '<th></th>';
        $table .= '</tr>';
    }

    foreach ($data as $d) {
        $table .= '<tr>';

        foreach ($fields as $key) {
            $table .= '<td>' . $d->$key . '</td>';
        }
        if ($showEdit || $showDelete || $showView) {
            $table .= '<td>';
            if ($showEdit)
                $table .= '<a href="' . route($resource . '.edit', $d->id) .
                    '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a>';
            if ($showView)
                $table .= '<a href="' . route($resource . '.show', $d->id) .
                    '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i></a> ';

            if ($showDelete)
                $table .= HTML::deleteButton('', route($resource . '.destroy', $d->id), 'btn-xs');

            $table .= '</td>';
        }
        $table .= '</tr>';
    }
    $table .= '</table>';
    return $table;
});




/*
|--------------------------------------------------------------------------
| Delete form macro
|--------------------------------------------------------------------------
|
| This macro creates a form with only a submit button.
| We'll use it to generate forms that will post to a certain url with the DELETE method,
| following REST principles.
|
| Call it like this: {{Form::delete('resource/'. $resource->id, 'Delete')}}
| or pass more parameters at will
*/
Form::macro('delete', function ($url, $button_label = 'Delete', $form_parameters = array(), $button_options = array()) {

    if (empty($form_parameters)) {
        $form_parameters = array(
            'method' => 'DELETE',
            'class' => 'delete-form',
            'url' => $url
        );
    } else {
        $form_parameters['url'] = $url;
        $form_parameters['method'] = 'DELETE';
    };

    return Form::open($form_parameters)
    . Form::submit($button_label, $button_options)
    . Form::close();
});


HTML::macro('deleteButton', function ($name, $url, $class = '', $message = '') {
    $form = Form::open(array('url' => $url, 'class' => 'pull-right', 'style' => 'margin-top:-1px'));
    $form .= Form::hidden('_method', 'DELETE');
    $class .= ' btn btn-warning confirm';
    if ($message == '')
        $message = 'Delete this item?';

    $form .= '<button type="submit" class="' . $class . '" data-confirm="' . $message . '">';
    $form .= '<i class="glyphicon glyphicon-remove"></i>';
    $form .= $name;
    $form .= '</button>';
    $form .= Form::close();
    return $form;
});

HTML::macro('alert', function ($type, $message, $head = null) {
    $head = $head ? $head : ucwords($type);
    return '<div class="alert alert-' . $type . '"><strong>' . $head . ': </strong>' . $message . '</div>';
});

HTML::macro('alertDismissible', function ($class, $message) {
    $alert = '<div class="alert alert-dismissible alert-link ' . $class . '" role="alert">';
    $alert .= '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
    $alert .= $message;
    return $alert . '</div>';
});

// //e.g. login.blade.php
// @if($errors->any())
//     <div class="col-md-8 col-md-offset-2">
//      @foreach($errors->all() as $error)
//      {{ HTML::alert('danger', $error, 'Whoops') }}
//      @endforeach
//     </div>
// @endif


