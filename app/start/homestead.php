<?php



function l($val)
{
    return Clockwork::info($val);
}


function start($name, $desc)
{
    return Clockwork::startEvent($name, $desc);
}

function stop($name)
{
    return Clockwork::endEvent($name);
}
