<?php

class DepartmentsTableSeeder extends Seeder {

	public function run()
	{
		RT\Departments\Department::create(['id' => 2, 'title' => 'Institutionen för fysik', 'title_eng' => 'Departments of physics']);
		RT\Departments\Department::create(['id' => 3, 'title' => 'Institutionen för matematik och matematisk statistik', 'title_eng' => 'Departments of mathematics and mathematical statistics']);
		RT\Departments\Department::create(['id' => 4, 'title' => 'Institutionen för datavetenskap', 'title_eng' => 'Departments of computing science']);
		RT\Departments\Department::create(['id' => 5, 'title' => 'Institutionen för tillämpad fysik och elektronik', 'title_eng' => 'Departments of applied physics and electronics']);
		RT\Departments\Department::create(['id' => 6, 'title' => 'Enheten för professionskurser', 'title_eng' => '']);
		RT\Departments\Department::create(['id' => 7, 'title' => 'Radiofysik och medicinsk teknik', 'title_eng' => 'Radiation Physics and Medical Bioengineering']);
		RT\Departments\Department::create(['id' => 8, 'title' => 'Institutionen för idé- och samhällsstudier', 'title_eng' => 'Departments of historical, philosophical and religious studies']);
		RT\Departments\Department::create(['id' => 9, 'title' => 'Institutionen för språkstudier', 'title_eng' => 'Departments of language studies']);
		RT\Departments\Department::create(['id' => 10, 'title' => 'Handelshögskolan vid Umeå universitet', 'title_eng' => 'Umeå school of business']);
		RT\Departments\Department::create(['id' => 11, 'title' => 'Institutionen för ekologi, miljö och geovetenskap', 'title_eng' => 'Departments of ecology and environmental science']);
		RT\Departments\Department::create(['id' => 12, 'title' => 'Statistiska institutionen', 'title_eng' => 'Departments of statistics']);
		RT\Departments\Department::create(['id' => 13, 'title' => 'Institutionen för nationalekonomi', 'title_eng' => 'Departments of economics']);
		RT\Departments\Department::create(['id' => 14, 'title' => 'Kemiska institutionen', 'title_eng' => 'Departments of chemistry']);
		RT\Departments\Department::create(['id' => 15, 'title' => 'Institutionen för ekonomisk historia', 'title_eng' => 'Departments of Economic History']);
	}
}



	
