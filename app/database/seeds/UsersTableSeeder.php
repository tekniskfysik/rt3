<?php
use Acme\Users\User;
use Acme\Groups\Group;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$baseUsers = array(
			'admin',
			'ledning',
			'editor',
			'student'
		);
		
		foreach($baseUsers as $baseUser) {
			$group = Group::where('name', $baseUser)->first();
			$user = User::create(array(
				'name'     => $baseUser,
				'username' => $baseUser,
				'email'    => $baseUser . '@test.se',
				'password' => $baseUser,
			));
			$user->groups()->attach($group->id);
		}
		
		$group = Group::where('name', 'student')->first();
		$faker = Faker::create();
		for($i=0; $i<10; $i++) {
			$extraUser = $faker->firstName($gender = null|'male'|'female');
			$user = User::create(array(
				'name'     => $extraUser,
				'username' => $extraUser,
				'email'    => $extraUser . '@test.se',
				'password' => $extraUser,
			));
			$user->groups()->attach($group->id);
		}
	}
}



