<?php

use RT\Exams\Exam;

class ExamsTableSeeder extends Seeder {

	public function run()
	{
		Exam::create(['title' => 'Teknisk Fysik', 'ects' => 300, 'year' => 2014]);
	}

}