<?php
use Goals\Goals\Goal;

class GoalsTableSeeder extends Seeder {

	public function run()
	{
		Goal::create(['id' => 1, 'title' => 'CDIO', 'desc' =>' ', 'parent_id' => 0]);
		Goal::create(['id' => 2, 'title' => '1', 'desc' =>' MATEMATISKA, NATURVETENSKAPLIGA OCH TEKNIKVETENSKAPLIGA KUNSKAPER↵', 'parent_id' => 1]);
		Goal::create(['id' => 3, 'title' => '1.1', 'desc' =>' KUNSKAPER I GRUNDLÄGGANDE MATEMATISKA OCH NATURVETENSKAPLIGA ÄMNEN', 'parent_id' => 2]);
		Goal::create(['id' => 4, 'title' => '1.2', 'desc' =>' KUNSKAPER I TEKNIKVETENSKAPLIGA ÄMNEN', 'parent_id' => 2]);
		Goal::create(['id' => 5, 'title' => '1.3', 'desc' =>' FÖRDJUPADE KUNSKAPER I NÅGOT/NÅGRA TILLÄMPADE ÄMNEN', 'parent_id' => 2]);
		Goal::create(['id' => 6, 'title' => '2', 'desc' =>' INDIVIDUELLA OCH YRKESMÄSSIGA FÄRDIGHETER OCH FÖRHÅLLNINGSSÄTT', 'parent_id' => 1]);
		Goal::create(['id' => 7, 'title' => '2.1', 'desc' =>' INGENJÖRSMÄSSIGT TÄNKANDE OCH PROBLEMLÖSANDE↵⇥Problemidentifiering och-formulering↵⇥Modellering↵⇥Kvantitativa och kvalitativa uppskattningar↵⇥Analys med hänsyn till osäkerheter och risker↵⇥Slutsatser och rekommendationer', 'parent_id' => 6]);
		Goal::create(['id' => 8, 'title' => '2.2', 'desc' =>' EXPERIMENTERANDE OCH KUNSKAPSBILDNING↵⇥Hypotesformulering↵⇥Informationskompetens↵⇥Experimentell metodik↵⇥Hypotesprövning', 'parent_id' => 6]);
		Goal::create(['id' => 9, 'title' => '2.3', 'desc' =>' SYSTEMTÄNKANDE↵⇥Helhetstänkande↵⇥Interaktion och framträdande egenskaper hos system↵⇥Prioritering och fokusering↵⇥Kompromisser och avvägningar i val av lösningar', 'parent_id' => 6]);
		Goal::create(['id' => 10, 'title' => '2.4', 'desc' =>' INDIVIDUELLA FÄRDIGHETER OCH FÖRHÅLLNINGSSÄTT↵⇥Initiativförmåga och risktagande↵⇥Uthållighet och anpassningsförmåga↵⇥Kreativt tänkande↵⇥Kritiskt tänkande↵⇥Självkännedom↵⇥Nyfikenhet och livslångt lärande↵⇥Planering av tid och resurser', 'parent_id' => 6]);
		Goal::create(['id' => 11, 'title' => '2.5', 'desc' =>' PROFESSIONELLA FÄRDIGHETER OCH FÖRHÅLLNINGSSÄTT↵⇥Yrkesetik, integritet, ansvar och pålitlighet↵⇥Professionellt uppträdande↵⇥Aktiv karriärplanering↵⇥Att hålla sig à jour med professionens utveckling', 'parent_id' => 6]);
		Goal::create(['id' => 12, 'title' => '3', 'desc' =>' FÖRMÅGA ATT ARBETA I GRUPP OCH ATT KOMMUNICERA', 'parent_id' => 1]);
		Goal::create(['id' => 13, 'title' => '3.1', 'desc' =>' ATT ARBETA I GRUPP↵⇥ Att skapa effektiva grupper↵⇥Grupparbete↵⇥Grupputveckling↵⇥Ledarskap↵⇥Gruppsammansättning', 'parent_id' => 12]);
		Goal::create(['id' => 14, 'title' => '3.2', 'desc' =>' ATT KOMMUNICERA↵⇥Kommunikationsstrategi↵⇥Budskapets struktur↵⇥Skriftlig framställning↵⇥Multimedia och elektronisk kommunikation↵⇥Grafisk kommunikation↵⇥Muntlig framställning', 'parent_id' => 12]);
		Goal::create(['id' => 15, 'title' => '3.3', 'desc' =>' ATT KOMMUNICERA PÅ FRÄMMANDE SPRÅK↵⇥Engelska↵⇥Språk i länder av regionalt industriellt intresse↵⇥Andra språk', 'parent_id' => 12]);
		Goal::create(['id' => 16, 'title' => '4', 'desc' =>' PLANERING, UTVECKLING, REALISERING OCH DRIFT AV TEKNISKA SYSTEM MED↵HÄNSYN TILL AFFÄRSMÄSSIGA OCH SAMHÄLLELIGA BEHOV OCH KRAV', 'parent_id' => 1]);
		Goal::create(['id' => 17, 'title' => '4.1', 'desc' =>' SAMHÄLLELIGA VILLKOR↵⇥Ingenjörens roll och ansvar↵⇥Teknikens roll i samhället↵⇥Samhällets regelverk↵⇥Historiska perspektiv och kulturella sammanhang↵⇥Aktuella frågor och värderingar↵⇥Utvecklande av ett globalt perspektiv', 'parent_id' => 16]);
		Goal::create(['id' => 18, 'title' => '4.2', 'desc' =>' FÖRETAGS- OCH AFFÄRSMÄSSIGA VILLKOR↵⇥Förståelse för olika affärskulturer↵⇥Planering, strategier och mål för affärsverksamhet↵⇥Teknikbaserat entreprenörskap↵⇥Att arbeta framgångsrikt i en organisation', 'parent_id' => 16]);
		Goal::create(['id' => 19, 'title' => '4.3', 'desc' =>' ATT PLANERA SYSTEM↵⇥Att specificera systemmål och -krav↵⇥Att definiera systemets funktion, koncept och arkitektur↵⇥Att modellera system och att säkerställa måluppfyllelse↵⇥Ledning av utvecklingsprojekt', 'parent_id' => 16]);
		Goal::create(['id' => 20, 'title' => '4.4', 'desc' =>' ATT UTVECKLA SYSTEM↵⇥Konstruktionsprocessen↵⇥Konstruktionsprocessens faser och metodik↵⇥Kunskapsanvändning vid konstruktion↵⇥Disciplinär konstruktion (inom ett teknikområde, t.ex. hydraulikkonstruktion)↵⇥Multidisciplinär konstruktion↵⇥Konstruktion med hänsyn till multipla, motstridiga mål', 'parent_id' => 16]);
		Goal::create(['id' => 21, 'title' => '4.5', 'desc' =>' ATT REALISERA SYSTEM↵⇥Uformning av realiseringsprocessen↵⇥Tillverkning av hårdvara↵⇥Implementering av mjukvara↵⇥Integration av mjuk- och hårdvara↵⇥Test, verifiering, validering och certifiering↵⇥Ledning av realiseringsprocessen', 'parent_id' => 16]);
		Goal::create(['id' => 22, 'title' => '4.6', 'desc' =>' ATT TA I DRIFT OCH ANVÄNDA↵⇥Att utforma och optimera driften↵⇥Utbildning för drift↵⇥Systemunderhåll↵⇥Systemförbättring och -utveckling↵⇥Systemavveckling↵⇥Driftledning', 'parent_id' => 16]);
		Goal::create(['id' => 23, 'title' => 'Nationella mål', 'desc' =>' ', 'parent_id' => 0]);
		Goal::create(['id' => 24, 'title' => 'K1.1', 'desc' =>' visa kunskap om ämnets vetenskapliga grund och beprövade erfarenhet', 'parent_id' => 23]);
		Goal::create(['id' => 25, 'title' => 'K1.2', 'desc' =>' visa insikt i aktuellt  forsknings- och utvecklingsarbete i ämnet', 'parent_id' => 23]);
		Goal::create(['id' => 26, 'title' => 'K2.1', 'desc' =>' visa kunnande inom matematik och naturvetenskap', 'parent_id' => 23]);
		Goal::create(['id' => 27, 'title' => 'K2.2', 'desc' =>' visa brett kunnande inom det valda teknikområdet', 'parent_id' => 23]);
		Goal::create(['id' => 28, 'title' => 'K2.3', 'desc' =>' visa väsentligt fördjupade kunskaper inom vissa delar av området', 'parent_id' => 23]);
		Goal::create(['id' => 29, 'title' => 'F1.1', 'desc' =>' visa förmåga att med helhetssyn kritiskt, självständigt och kreativt identifiera, formulera och hantera ⇥komplexa frågeställningar', 'parent_id' => 23]);
		Goal::create(['id' => 30, 'title' => 'F1.2', 'desc' =>' visa förmåga att delta i forsknings- och utvecklingsarbete och därigenom bidra till kunskapsutvecklingen', 'parent_id' => 23]);
		Goal::create(['id' => 31, 'title' => 'F2', 'desc' =>' visa förmåga att skapa, analysera och kritiskt utvärdera olika tekniska lösningar', 'parent_id' => 23]);
		Goal::create(['id' => 32, 'title' => 'F3', 'desc' =>' visa förmåga att planera och med adekvata metoder genomföra kvalificerade uppgifter inom givna ramar', 'parent_id' => 23]);
		Goal::create(['id' => 33, 'title' => 'F4.1', 'desc' =>' visa förmåga att kritiskt och systematiskt integrera kunskap', 'parent_id' => 23]);
		Goal::create(['id' => 34, 'title' => 'F4.2', 'desc' =>' visa förmåga att modellera, simulera, förutsäga och utvärdera skeenden även med begränsad information', 'parent_id' => 23]);
		Goal::create(['id' => 35, 'title' => 'F5', 'desc' =>' visa förmåga att utveckla och utforma produkter, processer och system med hänsyn till människors ⇥förutsättningar och behov och samhällets mål för ekonomiskt, socialt och ekologiskt hållbar utveckling', 'parent_id' => 23]);
		Goal::create(['id' => 36, 'title' => 'F6', 'desc' =>' visa förmåga till lagarbete och samverkan i grupper med olika sammansättning', 'parent_id' => 23]);
		Goal::create(['id' => 37, 'title' => 'F7.1', 'desc' =>' visa förmåga att i nationella som internationella sammanhang muntligt redogöra för och diskutera sina slutsatser och den kunskap och de argument som ligger till grund för dessa', 'parent_id' => 23]);
		Goal::create(['id' => 38, 'title' => 'F7.2', 'desc' =>' visa förmåga att i såväl nationella som internationella sammanhang skriftligt redogöra för och ⇥diskutera sina slutsatser och den kunskap och de argument som ligger till grund för dessa', 'parent_id' => 23]);
		Goal::create(['id' => 39, 'title' => 'V1', 'desc' =>' visa förmåga att göra bedömningar med hänsyn till relevanta vetenskapliga, samhälleliga och etiska ⇥aspekter samt visa medvetenhet om etiska aspekter på forsknings- och utvecklingsarbete', 'parent_id' => 23]);
		Goal::create(['id' => 40, 'title' => 'V2', 'desc' =>' visa insikt i teknikens möjligheter och begränsningar, dess roll i samhället och människors ansvar för ⇥hur den används, inbegripet sociala och ekonomiska aspekter samt miljö- och arbetsmiljöaspekter', 'parent_id' => 23]);
		Goal::create(['id' => 41, 'title' => 'V3', 'desc' =>' visa förmåga att identifiera sitt behov av ytterligare kunskap och att fortlöpande utveckla sin kompetens', 'parent_id' => 23]);
		Goal::create(['id' => 42, 'title' => 'Lokala mål', 'desc' =>' ', 'parent_id' => 0]);
		Goal::create(['id' => 43, 'title' => 'K1', 'desc' =>' visa goda baskunskaper och färdigheter i matematik, fysik och datavetenskap med desstillämpningar', 'parent_id' => 42]);
		Goal::create(['id' => 44, 'title' => 'K2', 'desc' =>' visa fördjupade kunskaper inom något/några av områdena datavetenskap, elektronik,energiteknik, fysik, matematik, matematisk statistik, radiofysik, rymdfysik, och rymdteknik', 'parent_id' => 42]);
		Goal::create(['id' => 45, 'title' => 'K3', 'desc' =>' visa förmåga att löpande tillgodogöra sig teknisk-vetenskapliga publikationer inom det valda ⇥teknikområdet', 'parent_id' => 42]);
		Goal::create(['id' => 46, 'title' => 'K4', 'desc' =>' visa förståelse för vikten av erfarenhetskunskap och arbetslivsanknytning för den kompletta ⇥ingenjörskompetensen', 'parent_id' => 42]);
		Goal::create(['id' => 47, 'title' => 'K5', 'desc' =>' visa grundläggande kunskap om hur man styr och säkerställer kvaliteten i olika organisationer', 'parent_id' => 42]);
		Goal::create(['id' => 48, 'title' => 'K6', 'desc' =>' visa kunskap om hur man arbetar i projekt samt kunskap om projektledarens roll och villkor', 'parent_id' => 42]);
		Goal::create(['id' => 49, 'title' => 'F1', 'desc' =>' visa vilja och förmåga att utföra en arbetsuppgift inom specificerade, ekonomiska, tidsmässiga och ⇥miljömässiga ramar', 'parent_id' => 42]);
		Goal::create(['id' => 50, 'title' => 'F2', 'desc' =>' visa förmåga att kunna utveckla en arbetsuppgift', 'parent_id' => 42]);
		Goal::create(['id' => 51, 'title' => 'F3', 'desc' =>' visa att den tillägnat sig de ingenjörsfärdigheter som uppfyller arbetslivets krav och behov', 'parent_id' => 42]);
		Goal::create(['id' => 52, 'title' => 'F4', 'desc' =>' visa förmåga att behandla ett problem inom ett brett teknikområde med hjälp av modellering och simulering med aktuella metoder och verktyg', 'parent_id' => 42]);
		Goal::create(['id' => 53, 'title' => 'V1', 'desc' =>' visa förståelse för arbetslivets villkor samt vara medveten om sin roll som förnyare av näringslivet', 'parent_id' => 42]);
		Goal::create(['id' => 54, 'title' => 'V2', 'desc' =>' visa insikt om hur förvärvade kunskaper och färdigheter tillämpas inom näringslivet', 'parent_id' => 42]);
		Goal::create(['id' => 55, 'title' => 'V3', 'desc' =>' visa erfarenhet av att arbeta i projekt både inom högskolan och näringslivet', 'parent_id' => 42]);
		Goal::create(['id' => 56, 'title' => 'V4', 'desc' =>' visa erfarenhet av hur man arbetar med kvalitet inom högskolan och näringslivet', 'parent_id' => 42]);
	}
}
