<?php

use RT\Profiles\Profile;
use Faker\Factory as Faker;

class ProfilesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
	
		$profiles = array(
			array('Beräkningsfysik', 'BER'),
			array('Finasiell modellering', 'FIN'),
			array('Fotonik och nanoteknik', 'FOT'),
			array('Mätteknik med industriell statistik', 'MÄT'),
			array('Sjukhusfysik och medicinsk teknik', 'SJU'),
			array('Rymd och Astrofysik', 'RYM')
		);
		foreach($profiles as $profile) {
			Profile::create([
				'title' => $profile[0],
				'abbr' => $profile[1],
				'desc' => $faker->realText($maxNbChars=800)
			]);
		}
	}
}


