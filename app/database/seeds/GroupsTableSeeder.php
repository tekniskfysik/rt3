<?php
use Acme\Groups\Group;
use Factory\Faker as Faker;

class GroupsTableSeeder extends Seeder {

	public function run()
	{
		$groups = array(
			'admin',
			'ledning',
			'editor',
			'student'
		);
		foreach($groups as $group) {
			Group::create([
				'name' => $group
			]);
		}
	}
}




