<?php

use RT\Courses\Course;
use RT\CourseOccasions\CourseOccasion;

use Faker\Factory as Faker;

class CourseOccasionsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
	
		$courses = Course::all();
		$nbCourses = $courses->count();
		$startYear = date("Y")-7;
		for($i=0; $i<$nbCourses; $i++) {
			switch($i%4) {
				case 0:
					 $start = 0;
				break;
				case 1:
					$start = 10;
				break;
				case 2:
					$start = 20;
				break;
				case 3:
					$start = 30;
				break;
				default:
			}
			$start = $start + $faker->randomElement([0,5]);
			
			$contact_name = $faker->name;
			$contact_email = $faker->email;
			for($j=0; $j<14; $j++) {
				CourseOccasion::Create(array(
					'year' => $startYear+$j,
					'start' => $start,
					'weeks' => $faker->randomElement(array(5,10,20,)),
					'official' => 1,
					'course_id' => $courses[$i]->id,
					'contact_name' => $contact_name,
					'contact_email' => $contact_email
				));
			}
		}
	}
}
