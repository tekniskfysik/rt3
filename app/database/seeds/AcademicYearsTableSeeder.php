<?php

use RT\AcademicYears\AcademicYear;

class AcademicYearsTableSeeder extends Seeder {

	public function run()
	{
		$startYear = date("Y")-7;
		for($i=0; $i<14; $i++) {
			$sy = $startYear+$i;
			$ey = $sy+1;
			AcademicYear::create(['year' => $sy, 'title' => "{$sy}/{$ey}"]);
		}
	}

}
