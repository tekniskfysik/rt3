<?php

use RT\Courses\Course;

class PrerequisitesTableSeeder extends Seeder {

	public function run()
	{
		$courses = Course::all();
		$nbCourses = $courses->count();
		if($nbCourses%2)
			$nbCourses = $nbCourses-1;
			
		for($i=0; $i<$nbCourses; $i=$i+2)
			$courses[$i]->prerequisites()->attach($courses[$i+1]);
	}
}
