<?php

use RT\Courses\Course;
use RT\Departments\Department;
use RT\Levels\Level;
use RT\Categories\Category;

use Faker\Factory as Faker;

class CoursesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		$ects = array(
			'7.5',
			'6',
			'4.5',
			'15',
			'10,5',
			'3'
		);
		
		for($i=0; $i<40; $i++)
		{
			//Title gets reused
			$title = $faker->words($nb=2, $asText=true);
			//belongTo relations
			$depIds = Department::get()->lists('id');
			$levelIds = Level::get()->lists('id');
			
			$course = Course::create([
				'title' => $title,
				'desc' => $faker->realText($maxNbChars = 800),
				'code' => $this->code(),
				'ects' => $faker->randomElement($ects),
				'title_eng' => 'Eng title: ' . $title,
				'department_id' => $faker->randomElement($depIds),
				'level_id' => $faker->randomElement($levelIds)
			]);
			
			//Bind toMany categories (0 to 5 of em)
			$nbOfCats = rand(0,5);
			$catIds = Category::get()->lists('id');
			$usedCat = array();
			for($j=0; $j<$nbOfCats; $j++) {
				$addCat = $faker->randomElement($catIds);
				while(in_array($addCat, $usedCat))
					$addCat = $faker->randomElement($catIds);
				$usedCat[] = $addCat;
				$course->categories()->attach($addCat);
				$course->categories()->updateExistingPivot($addCat, array(
					'ects' => $course->ects - rand(0,2)
				));
			}
		}
	}
	
	private function code()
	{
		$faker = Faker::create();
		$departments = array(
			'FY',
			'EL',
			'RA',
			'MA',
			'TN',
			'MS',
			'EH'
		);

		$smb = $faker->randomDigit;	//Something Before
		$dep = $faker->randomElement($departments);
		$sma = $faker->numerify('###');
		
		return $smb . $dep . $sma;
	}
}
