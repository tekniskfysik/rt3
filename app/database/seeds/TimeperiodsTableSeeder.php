<?php

use RT\TimePeriods\TimePeriod;

class TimeperiodsTableSeeder extends Seeder {

	public function run()
	{
		for($i=0; $i<40; $i=$i+5) {
			$extra = '';
			if(($offset = $i%10)) {
				if($offset > 1) {
					$extra = ', ' . $offset . ' veckor in';
				} else {
					$extra = ', ' . $offset . ' vecka in';
				}
			}
			$period = floor($i/10)+1;
			$title = 'Läsperiod ' . $period . $extra;
			TimePeriod::create(['title' => $title, 'week_offset' => $i]);
		}
	}

}
