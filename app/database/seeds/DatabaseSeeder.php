<?php

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//Every group needs to follow particular order
		$this->call('GroupsTableSeeder');
		$this->call('UsersTableSeeder');
		
		$this->call('LevelsTableSeeder');
		$this->call('DepartmentsTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('CoursesTableSeeder');
		$this->call('CourseOccasionsTableSeeder');
		$this->call('PrerequisitesTableSeeder');
		
		$this->call('ExamsTableSeeder');
		$this->call('AcademicYearsTableSeeder');
		$this->call('TimeperiodsTableSeeder');
		$this->call('ProfilesTableSeeder');
		$this->call('TracksTableSeeder');

	}
}
