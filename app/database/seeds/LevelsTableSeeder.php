<?php
use RT\Levels\Level;
class LevelsTableSeeder extends Seeder {

	public function run()
	{
		Level::create([	
			'title' => 'Grundläggande',
			'title_eng' => 'Introductory'
			]);

		Level::create([	
			'title' => 'Avancerad',
			'title_eng' => 'Advanced'
			]);
	}

}