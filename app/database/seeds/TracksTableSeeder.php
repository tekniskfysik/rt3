<?php

use RT\Tracks\Track;
use Faker\Factory as Faker;

class TracksTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
	
		Track::create(['id' => 1, 'title' =>	'Beräkningsfysik', 'profile_id' =>	1]);
		Track::create(['id' => 2, 'title' =>	'Finensiell modellering', 'profile_id' =>	2]);
		Track::create(['id' => 3, 'title' =>	'Mätteknik med industriell statistik', 'profile_id' =>	3]);
		Track::create(['id' => 4, 'title' =>	'Fotonik', 'profile_id' =>	4]);
		Track::create(['id' => 5, 'title' =>	'Nanoteknik', 'profile_id' =>	4]);
		Track::create(['id' => 6, 'title' =>	'Sjukhusfysik', 'profile_id' =>	5, 
									'desc' => $faker->text($maxNbChars=400)]);
		Track::create(['id' => 7, 'title' =>	'Medicinsk teknik', 'profile_id' =>	5, 
									'desc' => $faker->text($maxNbChars=400)]);
		Track::create(['id' => 8, 'title' =>	'Rymd- och astrofysik', 'profile_id' =>	6]);

	}

}

