<?php

use RT\Categories\Category;

class CategoriesTableSeeder extends Seeder {

	public function run()
	{
		Category::create([
			'id' => 7, 
			'title' => 'Allmänna ingenjörskurser', 
			'abbr' => 'AIK',
			'desc' => 'Syftet med dessa kurser är att stärka studentens kompetens inom områden som anses vara viktiga för den framtida yrkesrollen som civilingenjör. Inom allmänna ingenjörsområdet räknas både icke-tekniska kurser (t.ex. språk, ekonomi, juridik, entreprenörskap, projektledning, kvalitetsteknik, design och miljö), såväl som teknisk/naturvetenskapliga kurser av breddande karaktär utanför programmets ordinarie baskursutbud. Allmänna ingenjörskurser är i många fall på grundnivå. Både allmänna ingenjörskurser av icke-teknisk såväl som teknisk/naturvetenskaplig karaktär bör ingå i examen.']);
		Category::create([
			'id' => 31, 
			'abbr' => 'DV',
			'title' => 'Datavetenskap']);
		Category::create([
			'id' => 33, 
			'abbr' => 'EX',
			'title' => 'Examensarbete', 
			'desc' => 'Examensarbetet omfattar 30 högskolepoäng. Syftet med examensarbetet är att studenten på ett både ingenjörsmässigt och vetenskapligt sätt ska planera, genomföra samt muntligt och skriftligt redovisa ett självständigt projekt inom totala tidsramen av 20 arbetsveckor. Under examensarbetet får studenten i praktiskt arbete tillämpa och utveckla kunskaper och färdigheter som förvärvats under studietiden. Även om arbetet kan vara en del i ett större projekt ska arbetet utföras individuellt. Arbetet ska utföras i ett sammanhang som liknar en möjlig framtida arbetssituation för en civilingenjör/forskare. Det självständiga arbetet kan med fördel förläggas till industrin. Examensarbetet utgör dock en del av universitetsstudierna, och examineras därför av programledningen utsedd lärare/forskare. Den skriftliga rapporten ska språkligt och stilistiskt utformas så att den kvalitetsmässigt motsvarar rapporter inom universitet och industri. Examensarbetet ska ge en fördjupning inom något av teknisk fysikutbildningens profilområden och vars bas utgörs av en eller flera av följande ämnesområden: datavetenskap, energiteknik, fysik, matematik, matematisk statistik, medicinsk teknik, rymdfysik, rymdteknik eller strålningsfysik. För att kunna ta ut en sjukhusfysikerexamen krävs att examensarbetet utförts inom området medicinsk strålningsfysik.']);
		Category::create([
			'id' => 5,
			'abbr' => 'FYS',
			'title' => 'Fysikalisk teori med tillämpningar', 
			'desc' => '<p>I examen ska ingå minst 60 hp baskurser inom fysikalisk teori med tillämpningar. Dessa kurser påbörjas så smått i ettan för att sedan ta stor plats i tvåan och trean.</p>']);
		Category::create([
			'id' => 30,
			'abbr' => 'HU', 
			'title' => 'Hållbar utveckling', 
			'desc' => 'I ett hållbart samhälle får alla människor sina grundläggande behov tillgodosedda (social hållbarhet), utan att jordens naturresurser utarmas (ekonomisk hållbarhet) eller ekosystemtjänsterna förstörs (ekologisk hållbarhet). En hållbar utveckling leder samhället i riktning mot ökad hållbarhet. Typiska lärmål för kurser inom hållbar utveckling är t.ex.: Teknikens roll; naturresurser och ekosystemtjänster; människans miljöpåverkan och naturens gränser; miljödriven innovation; samhällets system; resursfördelning; lokala, regionala och globala förhållningssätt; styrsystem och åtgärdsstrategier; livsstil, attityd och mänskliga behov; modeller ochverktyg; ämnestillämpning.']);
		Category::create([
			'id' => 3, 
			'abbr' => 'MAT',
			'title' => 'Matematiska och beräkningsvetenskapliga metoder och verktyg', 
			'desc' => 'I examen ska ingå minst 67,5 hp baskurser inom matematiska och beräkningsvetenskapliga metoder och verktyg. Minst 12 hp inom området ska utgöras av baskurser inom datavetenskap.']);
		Category::create([
			'id' => 9, 
			'abbr' => 'PROF',
			'title' => 'Profilkurs', 
			'desc' => 'I examen ska ingå minst 45 hp profilkurser eller valbara kurser på avancerad nivå. Fördjupning sker vanligtvis under programmets tredje, fjärde och femte år. Möjligheterna att kombinera en personlig och unik profil är stora. Studenten kan välja mellan att läsa kurser ur en fördefinierad profil, kombinera kurser från flera olika trackr eller i övrigt välja ur ett stort utbud av valbara kurser inom t.ex. datavetenskap, elektronik, fysik, matematik, matematisk statistik, medicinsk teknik, radiofysik, rymdfysik och rymdteknik. Den personliga trackn måste dock väljas så att studenten uppfyller förkunskapskraven på respektive kurs. Förkunskapskrav för respektive kurs garanterar progression mellan fördjupningskurserna. Fördjupningskurser från andra nationella eller internationella utbildningar kan tillgodoräknas i examen. Programledningen rekommenderar att studenten väljer de kurser som passar just denne allra bäst som person.']);
		Category::create([
			'id' => 26, 
			'abbr' => 'PROJ',
			'title' => 'Projektkurs', 
			'desc' => '<b>Definition av projektkurs.</b> En projektkurs är en kurs, eller ett moment i en kurs, som bedrivs i projektform. Detta innebär att:<ul><li>arbetet har ett väldefinierat mål och en tydlig beställare</li><li>arbetet syftar till att förbättra befintlig eller nyutveckla en prototyp, en produkt, ett system, en tjänst eller till att utföra ett förbättringsarbete som genererar ny kunskap</li><li>arbetet görs i en tillfälligt skapad projektorganisation</li><li>arbetet görs inom givna ramar avseende tid, resurs/kostnad och kvalitet/funktionalitet</li><li>roller, aktiviteter och dokumentation styrs av en dokumenterad projektmetodik</li><li>arbetet utförs i grupper om minst 3 studenter eller så ingår studenten/studenterna i befintlig projektorganisation på ett företag. I undantagsfall kan examinator för kurser (i samråd med programansvarig) bevilja undantag från detta villkor.</li></ul></p><p>För ett sammanhängande projekt omfattande minst 7,5 hp ska<ul><li>4-ca 8 studenter ingå i projektgruppen eller så ingår studenten/studenterna i befintlig projektorganisation på ett företag eller motsvarande,</li><li>projektgruppens sammansättning inte vara självvald av studenterna.</li></ul>']);
		Category::create([
			'id' => 29, 
			'abbr' => 'PL',
			'title' => 'Projektledning', 
			'desc' => '<b>Definition av projektledning.</b> En kurs eller moment i projektledning syftar till att förmedla kunskap om teorier, modeller och verktyg för att driva och leda projekt i akademiska, industriella och administrativa sammanhang.']);
		Category::create([
			'id' => 4,
			'abbr' => 'STAT',
			'title' => 'Statistisk analys och grundläggande mätvärdesbehandling',
			'desc' => 'I examen ska ingå minst 12 hp baskurser inom statistisk analys och grundläggande mätvärdesbehandling. Detta område syftar till att ge en förståelse bl.a. för mätosäkerheter, feluppskattningar, hypotesprövningar och statistisk behandling av data.']);
		Category::create([
			'title' => 'Avancerade kurser',
			'abbr' => 'AV'
		]);
		Category::create([
			'id' => 27,
			'abbr' => 'PN', 
			'title' => 'Projektkurs i nära samverkan med näringslivet'
		]);
		Category::create([
			'abbr' => 'PSAM',
			'title' => 'Projektkurs sammanhängande'
		]);
		Category::create([
			'id' => 28,
			'abbr' => 'PS', 
			'title' => 'Projektkurs stor grupp'
		]);
	}
}



