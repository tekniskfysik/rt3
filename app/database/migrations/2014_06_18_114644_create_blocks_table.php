
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blocks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('desc')->nullable();
			$table->string('start_year');
			$table->integer('private')->unsigned();	
			$table->text('note')->nullable();
			$table->timestamps();
			$table->integer('blockable_id')->unsigned();
			$table->string('blockable_type');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blocks');
	}

}
