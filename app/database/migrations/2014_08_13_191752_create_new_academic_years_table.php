<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewAcademicYearsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('academic_years');
		Schema::create('academic_years', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('year')->unique();
			$table->string('title');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('academic_years');
        Schema::create('academic_years', function(Blueprint $table)
        {
            $table->integer('year')->primary();
            $table->string('title');
            $table->timestamps();
        });

	}

}
