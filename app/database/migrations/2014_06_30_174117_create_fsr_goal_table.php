<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFsrGoalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fsr_goal', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('fsr_id')->unsigned()->index();
			$table->foreign('fsr_id')->references('id')->on('fsrs')->onDelete('cascade');
			$table->integer('goal_id')->unsigned()->index();
			$table->foreign('goal_id')->references('id')->on('goals')->onDelete('cascade');
			$table->boolean('approved');
			$table->boolean('approved_by')->unsigned()->nullable();
			$table->boolean('requested');
			$table->integer('requested_by')->unsigned()->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fsr_goal');
	}

}
