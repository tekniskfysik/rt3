<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCourseOccasionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('course_occasions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('year');
			$table->integer('start');
			$table->integer('weeks');
			$table->boolean('official');
			$table->text('note')->nullable();
			$table->string('url_homepage')->nullable();
			$table->string('url_evaluation')->nullable();
			$table->string('url_syllabus')->nullable();
			$table->string('contact_name')->nullable();
			$table->string('contact_email')->nullable();
			$table->integer('course_id')->unsigned()->index();			
			$table->foreign('course_id')->references('id')
				->on('courses');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('course_occasions');
	}

}
