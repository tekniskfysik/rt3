<?php

use Illuminate\Database\Schema\Blueprint;

class CreateGroupsTable
extends BaseMigration
{
    public function up()
    {
        Schema::create("groups", function(Blueprint $table)
        {
            $this
                ->setTable($table)
                ->addPrimary()
                ->addString("name")
                ->addTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists("groups");
    }
}