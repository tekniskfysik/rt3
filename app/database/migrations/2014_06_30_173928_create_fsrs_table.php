<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFsrsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fsrs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('course_id')->unsigned()->index();			
			$table->foreign('course_id')->references('id')
				->on('courses');
			$table->string('desc')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fsrs');
	}

}
