<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->text('desc')->nullable();
			$table->string('code');
			$table->decimal('ects', 3, 1);
			$table->boolean('approved')->nullable();
			$table->integer('closed');
			$table->text('note')->nullable();
			$table->string('title_eng')->nullable();
			$table->text('desc_eng')->nullable();
			$table->string('url_homepage')->nullable();
			$table->string('url_evaluation')->nullable();
			$table->timestamps();
			$table->integer('department_id')->unsigned()->index();			
			$table->foreign('department_id')->references('id')
				->on('departments');
			$table->integer('level_id')->unsigned()->index();			
			$table->foreign('level_id')->references('id')
				->on('levels');
            $table->string('slug');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
