<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrivateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('private_courses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('year');
			$table->decimal('ects', 3,1);
			$table->text('note');
			$table->integer('start');
			$table->integer('user_id');
			$table->integer('weeks');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('private_courses');
	}

}
