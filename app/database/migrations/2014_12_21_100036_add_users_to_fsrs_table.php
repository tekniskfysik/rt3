<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersToFsrsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fsrs', function($table)
		{
            $table->boolean('approved');
            $table->boolean('approved_by')->unsigned()->nullable();
            $table->integer('requested_by')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('revision')->unsigned()->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fsrs', function($table)
		{
            $table->dropColumn('approved');
            $table->dropColumn('approved_by');
            $table->dropColumn('requested_by');
            $table->dropColumn('parent_id');
            $table->dropColumn('revision');
		});
	}

}
