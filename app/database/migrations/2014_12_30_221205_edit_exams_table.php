<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EditExamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exams', function(Blueprint $table)
		{
            $table->dropColumn('ects');

		});
        Schema::table('exams', function(Blueprint $table)
        {

            $table->decimal('ects', 4,1);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('exams', function(Blueprint $table)
        {
            $table->dropColumn('ects');
        });
        Schema::table('exams', function(Blueprint $table)
        {
            $table->decimal('ects', 3,1);
        });
	}

}
