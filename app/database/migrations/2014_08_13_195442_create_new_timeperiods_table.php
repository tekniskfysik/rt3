<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNewTimeperiodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::dropIfExists('time_periods');
        Schema::create('time_periods', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->string('title_eng')->nullable();
            $table->integer('week_offset')->unique();
            $table->timestamps();
        });

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('time_periods');
        Schema::create('time_periods', function(Blueprint $table)
        {
            $table->string('title');
            $table->string('title_eng')->nullable();
            $table->integer('week_offset')->unique()->primary();
            $table->timestamps();
        });
	}

}
