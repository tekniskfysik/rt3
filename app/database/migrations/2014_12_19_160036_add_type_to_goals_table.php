<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToGoalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('goals', function($table)
		{
		    $table->string('type')->nullable();
            $table->string('code')->nullable();
            $table->string('hidden')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('goals', function($table)
		{
		    $table->dropColumn('type');
            $table->dropColumn('code');
            $table->dropColumn('hidden');
		});
	}

}
