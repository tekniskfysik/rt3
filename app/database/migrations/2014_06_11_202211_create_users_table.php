<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 32)->nullable();	
			$table->string('username', 32)->unique();
			$table->string('email', 32)->unique();
			$table->string('password', 64);
			$table->timestamps();

			// required for Laravel 4.1.26
			$table->string('remember_token', 100)->nullable();
		});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::drop('users');
	}

}
