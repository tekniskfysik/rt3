<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlockCourseoccasionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('block_course_occasion', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('block_id')->unsigned()->index();
			$table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
			$table->integer('course_occasion_id')->unsigned()->index();
			$table->foreign('course_occasion_id')->references('id')->on('course_occasions')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('block_course_occasion');
	}

}
