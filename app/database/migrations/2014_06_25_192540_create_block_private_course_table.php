<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlockPrivateCourseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('block_private_course', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('block_id')->unsigned()->index();
			$table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
			$table->integer('private_course_id')->unsigned()->index();
			$table->foreign('private_course_id')->references('id')->on('private_courses')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('block_private_course');
	}

}
