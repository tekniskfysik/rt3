<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTracksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tracks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->boolean('valid')->default(true);
			$table->text('desc')->nullable();
			$table->string('title_eng')->nullable();
			$table->string('desc_eng')->nullable();
			$table->timestamps();
			$table->integer('profile_id')->unsigned()->index();			
			$table->foreign('profile_id')->references('id')
				->on('profiles')->onDelete('cascade');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tracks');
	}

}
