<?php
use Faker\Factory as Faker; 

class ApiTester extends TestCase{

	use Factory;

	protected $fake;

	public function __construct(){
		$this->fake = Faker::create();
	}

	public function setUp(){
		parent::setUp();
		Artisan::call('migrate');
	}

	protected function getJson($uri, $type = 'GET', $parameters = []){
		return json_decode($this->call($type, $uri, $parameters)->getContent());

	}
	
}