<?php

trait Factory{

	protected $times = 1;

	
	protected function times($times){
		$this->times = $times;
		return $this;
	}

	protected function make($type, array $fields = []){
		
		while($this->times--){
			$stub = array_merge($this->getStub(), $fields);
			$type::create($stub);
		}
			
	}

	protected function getStub(){
		throw new BadMethodCallException('Create your own stub');
	}
	
}