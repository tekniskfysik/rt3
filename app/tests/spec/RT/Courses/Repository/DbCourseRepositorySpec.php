<?php

namespace spec\RT\Courses\Repository;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DbCourseRepositorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('RT\Courses\Repository\DbCourseRepository');
    }

    function it_returns_an_empty_array_with_empty_input(){
        $categories = [];
        $this->parseCategoryInput($categories)->shouldReturn([]);
    }

    function it_returns_a_correct_parsed_array_with_a_single_input(){
        $categories = [
            'id' => [1],
            'ects' => [1]
        ];
        $this->parseCategoryInput($categories)->shouldReturn([1 => ['ects' => 1]]);
    }


    function it_returns_a_correct_parsed_array_with_multiple_input(){
        $categories = [
            'id' => [1, 2],
            'ects' => [1, 2]
        ];
        $this->parseCategoryInput($categories)->shouldReturn([
                1 => ['ects' => 1],
                2 => ['ects' => 2]
            ]);
    }

    function it_takes_exceptiion_to_illegal_input(){
        $categories = [
            'id' => [1, 2, 4],
            'ects' => [1]
        ];
        $this->shouldThrow('ErrorException')->during('parseCategoryInput', [$categories]);
    }
}
