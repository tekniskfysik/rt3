<?php

namespace spec\Acme;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CrudButtonCreatorSpec extends ObjectBehavior
{

    function it_returns_correct_data_on_index_page_with_admin_permission(){
        $this->make( 'index', 'admin')->shouldReturn(['All', 'Create']);
    }
    function it_returns_correct_data_on_index_page_with_editor_permission(){
        $this->make( 'index', 'editor')->shouldReturn(['All', 'Create']);
    }
    function it_returns_correct_data_on_index_page_with_default_permission(){
        $this->make('index', 'viewer')->shouldReturn(['All']);
    }

    function it_returns_correct_data_on_show_page_with_admin_permission(){
        $this->make( 'show', 'admin')->shouldReturn(['All', 'Create', 'Show', 'Delete', 'Edit']);
    }
    function it_returns_correct_data_on_show_page_with_editor_permission(){
        $this->make( 'show', 'editor')->shouldReturn(['All', 'Create', 'Show', 'Edit']);
    }
    function it_returns_correct_data_on_show_page_with_default_permission(){
        $this->make('show', 'viewer')->shouldReturn(['All', 'Show']);
    }


    function it_returns_in_correct_order(){
        $this->makeButtons('courses', 'index')->shouldReturn(['Alla' => ['icon' => 'glyphicon-th-list', 'route' => 'courses.index']]);
    }

}
