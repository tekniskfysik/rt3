<?php 
$I = new FunctionalTester($scenario);

$I->am("a RT member");

$I->wantTo("Sign in to my account");

$I->signIn();

$I->assertTrue(Auth::check());

$I->amOnRoute('users.show', Auth::user()->id);






