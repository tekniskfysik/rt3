<?php 
$I = new FunctionalTester($scenario);
$I->am("guest");
$I->wantTo("Sign up for a new RT3 account");
$I->lookForwardTo("be a member");

$I->amOnPage("/");
$I->click("Registrera");
$I->seeCurrentUrlEquals("/register");

$I->fillField("username", "John");
$I->fillField("email", "John@test.se");
$I->fillField("password", "John1234");
$I->fillField("password_confirmation", "John1234");

$I->click("Lägg till");
$I->amOnRoute('users.show', Auth::id());

$I->seeRecord('users', ["username" => "John"]);

$I->assertTrue(Auth::check());

