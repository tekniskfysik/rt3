<?php

/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 11:29
 */

class UsersControllersTest extends TestCase
{

    public function __construct(){
        $this->mock = Mockery::mock('Acme\Users\Repository\UserRepositoryInterface');

    }

    public function tearDown(){
        Mockery::close();
    }

    public function testIndex()
    {

        $this->client->request('GET', 'users');

        $response = $this->get('users');

        $this->assertViewHas('users');

        $users = $response->original->getData()['users'];

        $this->assertInstanceOf('Illuminate\Pagination\Paginator', $users);

    }


}