<?php

/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 11:29
 */
class StatusesControllersTest extends TestCase
{
    public function testIndexWithAuth()
    {
        Auth::loginUsingId(1);

        $response = $this->get('statuses');

        $this->assertViewHas('statuses');

    }

    public function testIndexWithoutAuth()
    {
        Route::enableFilters();

        $this->get('statuses');

        $this->assertRedirectedToRoute('login');

    }

}