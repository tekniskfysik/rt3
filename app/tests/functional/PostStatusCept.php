<?php 
$I = new FunctionalTester($scenario);

$I->am("member");
$I->wantTo('post statuses to my profile');

$I->signIn();

$I->assertTrue(Auth::check());

$I->amOnPage('/statuses');

$status = "My first status";

$I->dontSeeRecord('statuses', ["body" => $status, "user_id" => Auth::id()]);

$I->postAStatus($status);

$I->seeRecord('statuses', ["body" => $status, "user_id" => Auth::id()]);

$I->seeCurrentUrlEquals('/statuses');


