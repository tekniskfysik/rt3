<?php 
$I = new FunctionalTester($scenario);
$I->am('A member');
$I->wantTo('Review all users who are registered');

$I->haveAnAccount(['username' => 'Foo']);
$I->haveAnAccount(['username' => 'Bar']);

$I->amOnPage('/users');
$I->see('Foo');
$I->see('Bar');

