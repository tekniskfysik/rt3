<?php
use \RT\Courses\Repository\DbCourseRepository;
use Way\Tests\Factory;

/**
 * Class UserRepositoryTest
 */
class DbCourseRepositoryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \IntegrationTester
     */
    protected $tester;


    /**
     * @var DbCourseRepository
     */
    protected $repo;

    protected function _before()
    {
        $this->repo = new DbCourseRepository;

    }

    /** @test */
    public function it_creates_a_course()
    {
        $course = $this->repo->createWithRelations($this->getCourseAttributes());
        $this->assertEquals('testkurs', $course->title);
    }

    /** @test */
    public function it_updates_a_course()
    {
        $course = $this->repo->create($this->getCourseAttributes(1));

        $this->repo->updateWithRelations($course, ['title' => 'hej']);

        $this->assertEquals('hej', $course->title);
        $this->assertEquals(6, $course->ects);

    }

    public function it_update_prereqs()
    {
        $anotherCourse = $this->repo->create($this->getCourseAttributes());

        $course = $this->repo->create($this->getCourseAttributes());
        $this->repo->updateWithRelations($course, ['prerequisites' => [$anotherCourse->id]]);

        $this->assertContains($anotherCourse->id, $course->prerequisites->lists('id'));
    }


    private function getCourseAttributes()
    {
        return [
            'title' => 'testkurs',
            'code' => '123',
            'ects' => '6',
            'department_id' => 1,
            'level_id' => 1
        ];

    }


}