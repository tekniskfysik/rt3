<?php


use Laracasts\TestDummy\Factory as TestDummy;
use \Acme\Users\Repository\DbUserRepository;

/**
 * Class UserRepositoryTest
 */
class UserRepositoryTest extends \Codeception\TestCase\Test
{
    /**
     * @var \IntegrationTester
     */
    protected $tester;


    /**
     * @var DbUserRepository
     */
    protected $repo;

    protected function _before()
    {
        $this->repo = new DbUserRepository();
    }

    /** @test */
    public function it_paginates_all_users()
    {
        TestDummy::times(4)->create('Acme\Users\User');

        $results = $this->repo->paginate(2);

        $this->assertCount(2, $results);

    }

    /** @test */
    public function it_finds_a_user_with_statuses_by_their_username()
    {
        // given
        $statuses = TestDummy::times(3)->create('Acme\Statuses\Status');
        $username = $statuses[0]->user->username;

        // when
        $user = $this->repo->findByUsername($username);

        // then
        $this->assertEquals($username, $user->username);

        $this->assertCount(3, $user->statuses);

    }

    /** @test */
    public function it_follows_another_user()
    {
        // given I have 2 users
        list($john, $susan) = TestDummy::times(2)->create('Acme\Users\User');

        // and I ($john) follows another user ($susan)
        $this->repo->follow($susan->id, $john);

        // then I should see that user ($susan) in the list of those that / ($john) follows
        $this->assertCount(1,$john->followedUsers);

        $this->assertTrue($john->followedUsers->contains($susan->id));

        $this->tester->seeRecord('follows', [
            'follower_id' => $john->id,
            'followed_id' => $susan->id
        ]);


    }

    /** @test */
    public function it_unfollows_another_user()
    {
        // given I have 2 users
        $users = TestDummy::times(2)->create('Acme\Users\User');

        // and one user follows another user
        $this->repo->follow($users[1]->id, $users[0]);

        // and then unfollow the same user
        $this->repo->unfollow($users[1]->id, $users[0]);

        // then I should NOT see that user in the list of those that $users[0] follows

        $this->tester->dontSeeRecord('follows', [
            'follower_id' => $users[0]->id,
            'followed_id' => $users[1]->id
        ]);


    }


}