<?php


use Acme\Statuses\StatusRepository;
use Laracasts\TestDummy\Factory as TestDummy;

class StatusRepositoryTest extends \Codeception\TestCase\Test
{
   /**
    * @var \IntegrationTester
    */
    protected $tester;
    protected $repo;

    protected function _before()
    {
    	$this->repo = new StatusRepository;
    }

    

    // tests
    /** @test */
    public function it_gets_all_statuses_for_a_user()
    {
    	$users = TestDummy::times(2)->create('Acme\Users\User');
		TestDummy::times ( 2 )->create ( 'Acme\Statuses\Status', [
				'user_id' => $users [0]->id,
				'body' => 'My status'
		] );
		
		TestDummy::times ( 2 )->create ( 'Acme\Statuses\Status', [
				'user_id' => $users [1]->id 
		] );
		
		$statusesForUser = $this->repo->getAllForUser($users[0]);
		
		$this->assertCount(2, $statusesForUser);

    }
    
    /** @test */
    public function it_saves_a_status_for_a_user(){
    	//Given I have an unsaved status
    	$status = TestDummy::create ( 'Acme\Statuses\Status', [
    			'user_id' => null,
    			'body' => 'My status'
    			] );
    	
    	// And and existing user
    	$user = TestDummy::create('Acme\Users\User');
    	 
    	// When I try to persist this status
    	$savedStatus = $this->repo->save($status, $user->id);
    	
    	// Then it shold be saved
    	$this->tester->seeRecord('statuses', [
    			'body' => 'My status'
		]);
    	
    	// And the status should have correct user id
    	$this->assertEquals($user->id, $savedStatus->user_id);
    }

}