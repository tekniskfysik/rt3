<?php

use \Acme\Users\User;
use \Mockery as m;
use Way\Tests\Factory;

class UserTest extends \Codeception\TestCase\Test
{

    use \Codeception\Specify;
    use Way\Tests\ModelHelpers;

   /**
    * @var \UnitTester
    */
    protected $tester;

    protected $registrationForm;


    /**
     * @var User
     */
    protected $user;

    protected function _before()
    {

    }

    protected function _after()
    {
    }


    public function testCompareIfEqualUsersIsEqual()
    {
        $thisUser = Factory::make('Acme\Users\User', ['id' => 1]);
        $this->assertTrue($thisUser->is($thisUser));
    }

    public function testCompareIfDifferentUsersIsEqual()
    {
        $thisUser = Factory::make('Acme\Users\User', ['id' => 1]);
        $anotherUser = Factory::make('Acme\Users\User', ['id' => 2]);
        $this->assertFalse($thisUser->is($anotherUser));
    }

    public function testHashesPasswordWhenSet(){
        Hash::shouldReceive('make')->once()->andReturn('hashed');
        $user = new User;
        $user->password = 'foo';
        $this->assertEquals('hashed', $user->password);
    }

    public function testHasManyBlocks(){
        $this->assertMorphMany('blocks', 'Acme\Users\User', 'blockable');
    }

    public function testHasManyPrivateCourses(){
        $this->assertHasMany('privatecourses', 'Acme\Users\User');
    }

}