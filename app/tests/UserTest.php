<?php	
class UserTest extends TestCase {
	public function testThatTrueIsTrue()
	{
	  $this->assertTrue(true);
	}

	public function setUp(){
		parent::setUp();
		Artisan::call('migrate');
	}

	public function testUsernameIsRequired()
	{
	  // Create a new User
	  $user = new User;
	  $user->email = "phil@ipbrown.com";
	  $user->password = "password";
	  $user->password_confirmation = "password";
	 
	  // User should not save
	  $this->assertFalse($user->save());
	 
	  // Save the errors
	  $errors = $user->errors()->all();
	 
	  // There should be 1 error
	  $this->assertCount(1, $errors);
	 
	  // The username error should be set
	  $this->assertEquals($errors[0], "Fältet användarnamn är obligatoriskt.");
	}

	public function testAddUser(){
		$user = new User;
	  	$user->email = "phil@ipbrown.com";
	  	$user->username = "phil";
	  	$user->password = "password";
	  	$user->password_confirmation = "password";
	  	$this->assertTrue($user->save());
	}
	public function testPasswordIsRequired()
	{
	  $user = new User;
	  $user->email = "phil@ipbrown.com";
	  $user->username = "afds";
	  $this->assertFalse($user->save());
	  $errors = $user->errors()->all();
	  $this->assertEquals($errors[0], "Fältet lösenord är obligatoriskt.");
	}

	public function testEmailIsRequired()
	{
	  $user = new User;
	  $user->username = "afds";
	  $user->password = "password";
	  $user->password_confirmation = "password";
	  $this->assertFalse($user->save());
	  $errors = $user->errors()->all();
	  $this->assertCount(1, $errors);
	  $this->assertEquals($errors[0], "Fältet email är obligatoriskt.");
	}

	public function testPasswordEqual()
	{
	  $user = new User;
	  $user->email = "phil@ipbrown.com";
	  $user->username = "afdshfghfg";
	  $user->password = "password";
	  $user->password_confirmation = "pasordgfdgfd";
	  $this->assertFalse($user->save());
	  $errors = $user->errors()->all();
	  $this->assertCount(1, $errors);
	  $this->assertEquals($errors[0], "lösenord matchar inte.");
	}

	public function testPasswordLength()
	{
	  $user = new User;
	  $user->email = "phil@ipbrown.com";
	  $user->username = "hej";
	  $user->password = "hej";
	  $user->password_confirmation = "hej";
	  $this->assertFalse($user->save());
	  $errors = $user->errors()->all();
	  $this->assertCount(2, $errors);
	  $this->assertEquals($errors[0], "The lösenord must be at least 8 characters.");
	}


	
}

