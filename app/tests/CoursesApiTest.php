<?php

class CoursesApiTest extends ApiTester{


	/** @test */
	public function test_it_fetches_courses(){
		$this->times(2)->make('Course');
		$this->getJson('api/courses');
		$this->assertResponseOk();

	}


	/** @test */
	public function it_fetches_a_single_course(){
		$this->times(1)->make('Course');
		$course = $this->getJson('api/courses/1')->data;
		$this->assertResponseOk();
		$this->assertObjectHasAttribute('title', $course);

	}

	/** @test */
	public function it_404s_if_a_course_is_not_found(){
		$json = $this->getJson('/api/courses/1');
		$this->assertResponseStatus(404);
		$this->assertObjectHasAttribute('error', $json);
	}

	/** @test */
	public function it_creates_a_new_course_given_valid_parameters(){
		$this->getJson('api/courses', 'POST', $this->getStub());
		$this->assertResponseStatus(201);
	}

	/** @test */
	public function it_throws_a_422_if_a_new_course_request_fails_validation(){
		$this->getJson('api/courses', 'POST');
		$this->assertResponseStatus(422);
	}

	protected function getStub(){
		return [
			'title' => $this->fake->sentence,
			'desc' => $this->fake->sentence,
			'department_id' => $this->fake->randomNumber(1, 10),
			'ects' => $this->fake->randomNumber(1, 10),
			'approved' => true,
			'level_id' => 1,
			'code' => $this->fake->randomNumber(1, 1000)
		];
	}
	
}