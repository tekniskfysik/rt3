<?php

/**
 * Like
 *
 * @property integer $id
 * @property integer $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $likable_id
 * @property string $likable_type
 * @property-read \ $likable
 * @property-read \User $user
 * @method static \Illuminate\Database\Query\Builder|\Like whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Like whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Like whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Like whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Like whereLikableId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Like whereLikableType($value) 
 */
class Like extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['likable_id', 'likable_type', 'user_id'];

	public function likable(){
		return $this->morphTo();
	}

	public function user(){
		return $this->belongsTo('Acme\Users\User');
	}

}