<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 12/08/14
 * Time: 20:37
 */

class BaseModel extends \Eloquent {
    public function scopeRecentUpdated($query){
        return $query->orderBy('updated_at', 'desc')->take(5);
    }

    protected function relationScope($query, $value, $column, $relation = 'course'){
        if ($value == '') return $query;
        return $query->whereHas($relation, function($query) use($value, $column){
            $query->where($column, '=', $value);
        });
    }


    public function nestedRelationScope($query, $value, $column, array $relation){
        if ($value == '') return $query;
        if(count($relation) == 1){
            return $this->relationScope($query, $value, $column, $relation[0]);
        }
        $rel = array_shift($relation);
        return $query->whereHas($rel, function($query) use($value, $column, $relation){
            return $this->nestedRelationScope($query, $value, $column, $relation);
        });
    }


    public function scopeActual($query)
    {
        return $query->where('year', '>', date("Y")-1)->where('year', '<', date("Y")+1);
    }


    /**
     * This method is used to call method in presenter if there exist a presenter for that class.
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        if(method_exists($this, 'present') && method_exists($this->present(), $key)){
            return $this->present()->{$key}();
        }

        return $this->getAttribute($key);
    }



} 