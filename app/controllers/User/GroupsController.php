<?php

use Acme\Groups\Repository\GroupRepositoryInterface as Repo;
use Acme\Groups\GroupForm as Form;

class GroupsController extends CrudController
{

    public function __construct(Repo $repo, Form $form)
    {
        parent::__construct($repo, $form, 'group', 'Grupper');
        $this->beforeFilter('auth.ledning');
        $this->beforeFilter('auth.admin', array('only' => array('destroy', 'create', 'edit', 'store', 'update')));
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \Acme\Groups\Group);
        $presenter->add('name', 'Titel', true);
        return parent::filterIndex($presenter, "Grupper");
    }

    protected function updateRelations($instance){
        $this->repo->updateWithRelations($instance, Input::all());
    }



}