<?php

use Acme\Users\Repository\UserRepositoryInterface;

class UsersController extends CrudController  {

	public function __construct(UserRepositoryInterface $user, \Acme\Users\RegistrationForm $registrationForm)
    {
        parent::__construct($user, $registrationForm, 'user', 'Användare');
        $this->buttonCreator->setQuiet();
    }

    public function index($data = []){
        $instances = \Acme\Users\User::wherePublic(true)->get();

        return View::make("{$this->route}.index",[
                $this->route => $instances,
                'links' => $this->editLinks('index')
            ] + $data);
    }

    public function edit($id){
        if($id != Auth::id()){
            return Redirect::route('users.index');
        }
        return parent::edit($id);
    }

    public function update($id){
        $this->form->setRules([
            'username' => 'required|unique:users,username,' . Auth::id(),
            'email' => 'required|email|unique:users,email,' . Auth::id(),
            'password' => 'confirmed'
        ]);
        return parent::update($id);
    }

	public function showUserProfile($userName){

        $exam = \RT\Exams\Exam::with('categories')->findOrFail(1);

		$user = $this->repo->findByUsername($userName);

        if($user->public)
            return parent::show($user->id, ['exam' => $exam]);
        return Redirect::home();
	}

    protected function redirect($instance){
        return Redirect::route('userprofile_path', $instance->username);
    }


}

