<?php

use ReCaptcha\ReCaptcha;

class RegistrationController extends \BaseController{
	
	private $registrationForm;
	
	function __construct(\Acme\Users\RegistrationForm $registrationForm){
		
		$this->registrationForm = $registrationForm;
		
		$this->beforeFilter('guest');
		
	}
	
	public function create(){
		
		return View::make('registration.create');
		
	}
	
	public function store(){
		$check = $this->captchaCheck();	
		Input::merge(['captcha' => $check]);
			
		$this->registrationForm->validate(Input::all());
		
		$user = $this->execute('Acme\Registration\RegisterUserCommand');
		
		Auth::login($user);
        $data = ['detail' => $user->email, 'name' => $user->username];
        \Illuminate\Support\Facades\Mail::later(0, 'emails.welcome', $data, function($message) use ($user)
        {
            $message->to($user->email, $user->username)->subject('Välkommen!');
        });
		
		Flash::overlay('Ditt konto är nu skapat');
		return Redirect::home();
	}

	public function captchaCheck() {
		$response = Input::get('g-recaptcha-response');
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$secret	  = $_ENV['RE_CAP_SECRET'];

		$recaptcha = new ReCaptcha($secret);
		$resp = $recaptcha->verify($response, $remoteip);
		if ($resp->isSuccess()) {
			return 1;
		} else {
			return 0;
		}
	}
}
