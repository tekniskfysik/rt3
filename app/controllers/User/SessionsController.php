<?php

use Acme\Users\User;
use Acme\Forms\SignInForm;
use ReCaptcha\ReCaptcha;

class SessionsController extends Controller {

	private $SignInForm;

	function __construct(SignInForm $form)
	{		
		$this->SignInForm = $form;
		$this->beforeFilter('guest', ['except' => 'destroy']);
	}

	public function create()
	{
		return View::make('sessions.create');
	}

	public function store() {
		$check = $this->captchaCheck();
		Input::merge(['captcha' => $check]);
		$formData = Input::only('email', 'password');
		$this->SignInForm->validate(Input::all());

		/* If the user type in a username instead of a email-adress */
		$userLoginData = array(
		'username' => Input::get('email'),
		'password' => Input::get('password') 
		);

		if (Auth::attempt($formData) || Auth::attempt($userLoginData)) {
			Flash::message("Du är nu inloggad!");
			return Redirect::intended('users/' . Auth::user ()->id);
		} else {
			Flash::error('Fel uppgifter. <a href="'.url('password/remind').'">Glömt lösenordet?</a>');
			return Redirect::to('login');
		}
	}

	public function destroy($id = null)
	{
		Auth::logout(); // log the user out of our application
		return Redirect::home(); // redirect the user to the login screen
	}

	public function captchaCheck() {
		$response = Input::get('g-recaptcha-response');
		$remoteip = $_SERVER['REMOTE_ADDR'];
		$secret	  = $_ENV['RE_CAP_SECRET'];

		$recaptcha = new ReCaptcha($secret);
		$resp = $recaptcha->verify($response, $remoteip);
		if ($resp->isSuccess()) {
			return 1;
		} else {
			return 0;
		}
	}
}
