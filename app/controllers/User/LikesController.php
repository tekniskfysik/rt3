<?php

class LikesController extends Controller {

	public function index(){

		$likes = Like::whereUserId(Auth::id())->get();
		return Response::json(array('likes' => $likes));

	}

	public function store()
	{
        $id = Input::get('likable_id');
        $type = Input::get('likable_type');
        $data = ['likable_id' => $id, 'likable_type' => $type, 'user_id' => Auth::id()];

        $like = Like::where($data)->first();
        if($like){
            Like::destroy($like->id);
        }else{
            Like::create($data);
        }

        if(Request::isJson()){
            return Response::json(array('success' => true));
        }
        return Redirect::back();
	}

	public function destroy($id)
	{
		Like::destroy($id);
		return Response::json(array('success' => true));	
	}


}
