<?php

use Acme\Statuses\StatusRepository;
use Acme\Forms\PublishStatusForm;
use Laracasts\Flash\Flash;

class StatusesController extends \BaseController
{

    protected $statusRepository;

    protected $publishStatusForm;

    function __construct(PublishStatusForm $publishStatusForm, StatusRepository $statusRepository)
    {
        $this->statusRepository = $statusRepository;
        $this->publishStatusForm = $publishStatusForm;
        $this->beforeFilter('auth');
    }

    /**
     * Display a listing of statuses
     *
     * @return Response
     */
    public function index()
    {
        $statuses = $this->statusRepository->getFeedForUser(Auth::user());

        return View::make('statuses.index', compact('statuses'));
    }

    /**
     * Store a newly created status in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $input['user_id'] = Auth::id();

        $this->publishStatusForm->validate($input);

        $this->execute('Acme\Statuses\PublishStatusCommand', $input);

        Flash::message("Your status has been updated");

        return Redirect::back();
    }

}
