<?php

/**
 * Class FollowersController
 */
class FollowersController extends \BaseController
{

    /**
     * Follow a user
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $input = array_add(Input::get(), 'userId', Auth::id());

        $user = $this->execute('RT\Users\Command\FollowUserCommand', $input);

        Flash::success("You are now following this user");

        return Redirect::back();
    }

    /**
     * Unfollow a user
     *
     * @param $userIdToUnfollow
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($userIdToUnfollow)
    {
        $input = array_add(Input::get(), 'userId', Auth::id());

        $this->execute('RT\Users\Command\UnfollowUserCommand', $input);

        Flash::success("You are now unfollowing this user");

        return Redirect::back();
    }

}
