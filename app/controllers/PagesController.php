<?php

use Acme\Pages\Repository\PageRepositoryInterface as Repository;
use Acme\Pages\PageForm as Form;
use Acme\Pages\Page;

class PagesController extends CrudController {

	/**
	 * Display a listing of pages
	 *
	 * @return Response
	 */

	protected $repo;

    public function __construct(Repository $repo, Form $form)
    {
        parent::__construct($repo, $form, 'page', 'Sidor');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

	public function start(){
        $chart = new \RT\Exams\CategoryChart(new \RT\Exams\Repository\DbExamRepository());
		return View::make('layouts.rt_home', ['PieChart' => $chart->pie()]);
	}

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new Acme\Pages\Page);
        $presenter->add('title', 'Sida', true);
        $presenter->add('slug', 'url', true);
        return parent::filterIndex($presenter);
    }

    public function fromTitle($title){
       return parent::show(Page::whereSlug($title)->firstOrFail()->id);
    }

    public function dashboard(){
        Breadcrumbs::addCrumb('Dashboard');
        return View::make('pages.dashboard');
    }

    public function tutorial(){
        Breadcrumbs::addCrumb('Tutorial');
        return View::make('pages.tutorial');
    }

    public function formTester(){
        dd(Input::all());

    }
}
