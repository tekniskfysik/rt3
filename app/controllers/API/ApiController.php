<?php

class ApiController extends Controller {

	const HTTP_NOT_FOUND = 404;

	protected $statusCode = 200;

	public function getStatusCode(){
		return $this->statusCode;
	}

	public function setStatusCode($statusCode){
		$this->statusCode = $statusCode;
		return $this;
	}

	public function respondNotFound($message = 'Not Found!'){
		return $this->setStatusCode(self::HTTP_NOT_FOUND)->respondWithError($message);
	}

	public function respondInternalError($message = 'Internal Error!'){
		return $this->setStatusCode(500)->respondWithError($message);
	}

	public function respond($data, $headers = []){
		if(Auth::check()) {

		    $authToken = AuthToken::create(Auth::user());
		    $publicToken = AuthToken::publicToken($authToken);

		    $userData = array_merge(
		      Auth::user()->toArray(),
		      array('auth_token' => $publicToken)
		    );

		    $jsConfig['userData'] = $userData;

		    $data = array_merge($data, $jsConfig);


		  }

		return Response::json($data, $this->getStatusCode(), $headers);
	}

	public function respondWithError($message){
		return $this->respond([
			'error' => [
				'message' => $message,
				'status_code' => $this->getStatusCode()
			],
		]);
	}

	public function respondCreated($message){
		return $this
    		->setStatusCode(201)
    		->respond(['message' => $message]);
	}

	protected function respondWithPagination(Illuminate\Pagination\Paginator $items, $data){
        $data = array_merge($data, [
            'paginator' => [
                'total_count' => $items->getTotal(),
                'total_pages' => ceil( $items->getTotal() / $items->getPerPage() ),
                'current_page' => $items->getCurrentPage(),
                'limit' => $items->getPerPage()
                ]
            ]);

        return $this->respond($data);
    }


}
