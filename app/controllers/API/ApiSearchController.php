<?php
 
/**
 * ApiSearchController is used for the "smart" search throughout the site.
 * it returns and array of items (with type and icon specified) so that the selectize.js plugin can render the search results properly
 **/
 
class ApiSearchController extends BaseController {
 
  public function appendValue($data, $type, $element)
  {
    // operate on the item passed by reference, adding the element and type
    foreach ($data as $key => & $item) {
      $item[$element] = $type;
    }
    return $data;   
  }
 
  public function appendURL($data, $prefix)
  {
    foreach ($data as $key => & $item) {
      $item['url'] = url($prefix.'/'.$item['id']);
    }
    return $data;   
  }
 
  public function index()
  {
    $query = e(Input::get('q',''));
 
    if(!$query && $query == '') return Response::json(array(), 400);

    $courses = \RT\Courses\Course::where('title','like','%'.$query.'%')
      ->orderBy('title','asc')
      ->get(array('id', 'title'))
      ->take(5)
      ->toArray();

    $categories = RT\Categories\Category::where('title','like','%'.$query.'%')
      ->orderBy('title','asc')
      ->get(array('id', 'title'))
      ->take(5)
      ->toArray();

    $profiles = RT\Profiles\Profile::where('title','like','%'.$query.'%')
      ->orderBy('title','asc')
      ->get(array('id', 'title'))
      ->take(5)
      ->toArray();
 
    $profiles = $this->appendURL($profiles, 'profiles');
    $courses   = $this->appendURL($courses, 'courses');
    $categories  = $this->appendURL($categories, 'categories');
 
    // Add type of data to each item of each set of results
    $profiles = $this->appendValue($profiles, 'profile', 'class');
    $courses = $this->appendValue($courses, 'course', 'class');
    $categories = $this->appendValue($categories, 'category', 'class');
 
    // Merge all data into one array
    $data = array_merge($courses, $categories, $profiles);
 
    return Response::json(array(
      'data'=>$data
    ));
  }
}