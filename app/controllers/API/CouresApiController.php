<?php

use Acme\Repositories\CourseRepositoryInterface;
use Acme\Transformers\CourseTransformer;
use  \RT\Courses\Course;

class CoursesApiController extends ApiController
{

    protected $courseTransformer;
    protected $repo;

    public function __construct(
        \RT\Courses\Repository\CourseRepositoryInterface $course,
        \RT\Courses\CourseTransformer $courseTransformer)
    {
        $this->repo = $course;
        $this->courseTransformer = $courseTransformer;
        $this->beforeFilter('auth.basic', ['only' => ['update', 'store', 'destroy']]);
    }

    public function show($id)
    {
        $course = $this->repo->find($id);
        if (!$course) {
            return $this->respondNotFound('Course does not exist');
        }
        return $this->respond([
            'data' => $course
        ]);
    }

    public function index()
    {

        $limit = Input::get('limit') ?: 10;
        $limit = $limit < 100 ? $limit : 100;
        $cat_id = Input::get('cat_id') ?: '';
        $q = Input::get('q') ?: '';

        $courses = Course::with('categories')->whereHas('categories', function ($q) use ($cat_id) {
            if ($cat_id == '')
                return $q;
            return $q->where('category_id', '=', $cat_id);
        })
            ->where('title', 'like', '%' . $q . '%')->paginate($limit);


        return $this->respondWithPagination($courses, [
            'data' => $this->courseTransformer->transformCollection($courses->all())]);

    }
}
