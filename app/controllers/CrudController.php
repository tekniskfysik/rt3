<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 22:05
 */


use Laracasts\Validation\FormValidator;
use RT\Repository\BaseRepository;

abstract class CrudController extends \BaseController
{
    /** @var  BaseRepository */
    protected $repo;

    /** @var  FormValidator */
    protected $form;

    protected $model;
    protected $modelTitle;
    protected $route;
    protected $messages;
    protected $buttonCreator;
    protected $group = 'ledning';


    //TODO: add modelname and modelTitle to constructor to remove setModelName method and to breadcrumbs
    function __construct($repo, $form, $modelName, $modelTitle, $route = null)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->form = $form;
        $this->model = $modelName;
        $this->modelTitle = $modelTitle;

        if($route == null){
            $this->route = str_plural($this->model);
        }else{
            $this->route = $route;
        }
        Breadcrumbs::addCrumb($modelTitle, $this->route);

        $this->setMessages();
        $this->buttonCreator = new \Acme\CrudButtonCreator();

    }

    public function filterIndex(\Acme\FilterPresenter $presenter, $json = false, $title = null){

        if($json){
            return json_encode($presenter->json());
        }
        $data = $presenter->present();

        if($title == null){
            $title = $this->modelTitle;
        }

        return View::make('rapyd.pages', $data + ['showFilter' => true, 'title' => $title, 'links' => $this->buttonCreator->render($this->route, 'index', 'editor')]);
    }

    public function setEditGroup($group){
        $this->group = $group;
    }

    /**
     * Show the index page for this model
     *
     * @return \Illuminate\View\View
     */
    public function index($data = [])
    {
        $instances = $this->repo->all();

        return View::make("{$this->route}.index",[
            $this->route => $instances,
            'links' => $this->editLinks('index')
        ] + $data);
    }

    /**
     * Show the detail page for this model
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($id, $data = [])
    {
        $instance = $this->repo->find($id);
        Breadcrumbs::addCrumb($instance->title ?:$instance->id);
				
        return View::make("{$this->route}.show",
            [
                $this->model => $instance,
                'links' => $this->editLinks('show', $id)
            ] + $data);
    }

    /**
     * Return create view for this model
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        Breadcrumbs::addCrumb('Skapa');
        return View::make("{$this->route}.create")->with(static::formData() +
            ['links'=> $this->editLinks('create')]
        );
    }

    /**
     * Return edit view for this model
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $instance = $this->repo->find($id);
        Breadcrumbs::addCrumb($instance->title ?:$instance->id, $id);
        Breadcrumbs::addCrumb('Ändra');
        return View::make("{$this->route}.edit",
            [$this->model => $instance])->with(static::formData() +
                ['links'=> $this->editLinks('edit', $id)]
            );
    }

    /**
     * Store a model instance in storage
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws Laracasts\Validation\FormValidationException
     */
    public function store()
    {
        $instance = $this->storePartial();

        Flash::overlay("{$this->messages['created']}", "Notering");

        return $this->redirect($instance);
    }


    public function storePartial($input = null)
    {
        if($input == null){
            $input = Input::all();
        }
        $this->form->validate($input);

        $instance = $this->repo->create($input);

        $this->updateRelations($instance);

        return $instance;
    }

    /**
     * Update a model instance
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws Laracasts\Validation\FormValidationException
     */
    public function update($id)
    {
        $instance = $this->updatePartial($id);
        Flash::overlay("{$this->messages['updated']}", "Notering");
        return $this->redirect($instance);
    }

    public function updatePartial($id){

        $this->form->validate($input = Input::all());
        $instance = $this->repo->find($id);
        $instance = $this->repo->updateWithIdAndInput($id, $input);
        $this->updateRelations($instance);
        return $instance;
    }

    /**
     * Destroy a model instance
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        Flash::overlay("{$this->messages['deleted']}", "Notering");

        return Redirect::back();
    }

    /**
     * Set a default flash messages
     * Should be overridden in sub-controller.
     */
    protected function setMessages()
    {
        $this->messages = [
            'created' => $this->model . ' tillagd.',
            'updated' => $this->model . ' uppdaterad.',
            'deleted' => $this->model . ' borttagen.'
        ];
    }

    /**
     * Update relation to instance.
     * This method must be overridden in caller class.
     *
     * @param $instance
     */
    protected function updateRelations($instance){}

    protected function redirect($instance){
        return Redirect::route("{$this->route}.show", $instance->id);
    }

    protected function formData(){
        return [];
    }

    /**
     * Help function for show crud buttons in views.
     *
     * @param string $route
     * @param null $id
     * @return mixed
     */
    protected function editLinks($route = 'index', $id = null){
        return $this->buttonCreator->render($this->route, $route, Auth::user() && Auth::user()->inGroup($this->group) ? 'editor' : null, $id);
    }

} 
