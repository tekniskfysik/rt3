<?php

class FsrGoalController extends \BaseController {

	public function show($id){
		
		$fsr = Fsr::findOrFail($id);
		$goals = $fsr->goals;

		foreach (Goal::all() as $key => $value) {
			if(!$goals->contains($value->id)){
				$value->pivot = ['requested' => 0, 'approved' => 0, 'fsr_id' => $fsr->id, 'goal_id' => $value->id];
				$goals->push($value);
			}
		}
		if (Input::json())
		{
			return Response::json(array('goals' => $goals));
		}
	}

	public function store(){
		$fsr_id = Input::get('pivot.fsr_id');
		$goal_id = Input::get('pivot.goal_id');
		$approved = Input::get('pivot.approved');
		$requested = Input::get('pivot.requested');
		
		$fsr = Fsr::findOrFail($fsr_id);
		$fsr->goals()->detach($goal_id);
		if($requested || $approved)
			$fsr->goals()->attach([
				$goal_id => [
					'approved'=>$approved, 
					'requested' => $requested,
					Input::get('info') => Auth::id()
				]
			]);

		return Response::json(array('success' => true,'fsr' => $fsr,  'input' => Input::all()));	
	}
}