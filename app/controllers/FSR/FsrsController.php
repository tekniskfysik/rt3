<?php

use Goals\Fsrs\FsrForm as Form;
use RT\Courses\Repository\CourseRepositoryInterface as CourseRepo;
use Goals\Fsrs\Repository\FsrRepositoryInterface as Repo;
use Goals\Fsrs\FsrFilterViewPresenter as FilterPresenter;

class FsrsController extends CrudController
{
    protected $courseRepo;

    public function __construct(Repo $repo,
                                Form $form, CourseRepo $courseRepo)
    {
        $this->courseRepo = $courseRepo;
        parent::__construct($repo, $form, 'fsr', 'FSR:s');
        $this->beforeFilter('auth.ledning', array('only' => array('destroy')));
        $this->beforeFilter('auth.editor', ['except' => ['show']]);
    }

    protected function redirect($instance){
        return Redirect::route("fsrs.course", $instance->course_id);
    }

    public function index($data = []){
        $presenter = new \Goals\Fsrs\CourseFsrFilterViewPresenter();
        //return parent::filterIndex($presenter, false, '');

        $filter = $presenter->filter();
        $set =  $presenter->set();
        $cids = $set->data->lists('id');


        $matrix[0] = "<p class='well'>Inga resultat att visa</p>";
        $matrix[1] = "<p class='well'>Inga resultat att visa</p>";
        if(count($cids)){
            $sum = new \RT\Courses\CourseSummarizer($cids);
            $matrix[0] = $sum->printMatrix([0], 'fsr');
            $matrix[1] = $sum->printMatrix([2], 'fsr');
        }
        return View::make('fsrs.index',  ['filter' => $filter, 'matrix' => $matrix, 'set'=> $set, 'showFilter' => true, 'title' => 'Fsrs', 'links' => $this->buttonCreator->render($this->route, 'index', 'editor')]);
    }

    public function overview($data = []){
        return parent::filterIndex(new FilterPresenter());
    }

    public function courseIndex($id = null, $data = [])
    {
        $title = $this->courseRepo->find($id)->title;
        Breadcrumbs::addCrumb($title, route('fsrs.course', $id));
        return parent::filterIndex(new FilterPresenter($id), false, $title);
    }

    public function show($id, $data = []){
        $instance = $this->repo->find($id);
        Breadcrumbs::addCrumb($instance->course->title, route('fsrs.course', $instance->course->id));
        $pieChart = \Goals\Fsrs\FsrChart::pieTest();
        return parent::show($id, compact('pieChart'));
    }

    public function edit($id, $data = []){
        $instance = $this->repo->find($id);
        Breadcrumbs::addCrumb($instance->course->title, route('fsrs.course', $instance->course->id));
        Breadcrumbs::addCrumb($instance->title ?:$instance->id, route('fsrs.show', $id));
        Breadcrumbs::addCrumb('Ändra');
        return View::make("{$this->route}.edit",
            [$this->model => $instance])->with(static::formData() +
                ['links'=> $this->editLinks('edit', $id)]
            );
    }

    public function export($ids)
    {
        \Goals\Fsrs\FsrExporter::export();
    }

    protected function formData()
    {
        return ['courseRepo' => $this->courseRepo];
    }

    public function accept($id)
    {

        if (Auth::user()->isLedning()) {

            $instance = $this->repo->find($id);
            if ($instance->parent_id != $instance->id) {
                $this->repo->destroy($id);
                $id = $instance->parent_id;
            }
            $instance = $this->repo->find($id);
            $this->repo->updateWithIdAndInput($instance->id, [
                'approved' => 1,
                'approved_by' => Auth::id(),
                'parent_id' => $instance->id,
                'revision' => 0]);
            Flash::overlay("{$this->messages['updated']}", "Notering");
        }


        Flash::overlay("Kunde inte uppdateras..", "Notering");
        return $this->redirectRoute('fsrs.index');

    }

    public function store()
    {
        $instance = $this->storePartial();
        if (Auth::user()->isLedning()) {
            $this->repo->updateWithIdAndInput($instance->id, [
                'approved' => 1,
                'approved_by' => Auth::id(),
                'parent_id' => $instance->id
            ]);
        } else {
            $this->repo->updateWithIdAndInput($instance->id, [
                'requested_by' => Auth::id(),
                'parent_id' => $instance->id
            ]);
        }
        Flash::overlay("{$this->messages['created']}", "Notering");

        return $this->redirect($instance);
    }

    public function update($id)
    {

        if (Auth::user()->isLedning()) {
            $instance = $this->repo->find($id);
            if ($instance->parent_id != $instance->id) {
                $this->repo->destroy($id);
                $id = $instance->parent_id;
            }
            $instance = $this->updatePartial($id);
            $this->repo->updateWithIdAndInput($instance->id, [
                'approved' => 1,
                'approved_by' => Auth::id(),
                'parent_id' => $instance->id,
                'revision' => 0]);
        } else {
            $parentInstance = $this->repo->find($id);
            if ($parentInstance->parent_id != $parentInstance->id) {
                $this->repo->destroy($parentInstance->id);
                $parentInstance = $this->repo->find($parentInstance->parent_id);
            }
            if ($parentInstance->approved) {
                $instance = $this->storePartial();
                $this->repo->updateWithIdAndInput($instance->id, [
                    'requested_by' => Auth::id(),
                    'approved_by' => 0,
                    'approved' => 0,
                    'parent_id' => $parentInstance->id,
                    'revision' => $parentInstance->revision + 1,
                    'course_id' => $parentInstance->course_id
                ]);
            } else {
                $instance = $this->updatePartial($id);
            }
        }


        Flash::overlay("{$this->messages['updated']}", "Notering");
        return $this->redirect($instance);
    }


    protected function updateRelations($instance)
    {
        $this->repo->updateWithRelations($instance, Input::all());
    }

}
