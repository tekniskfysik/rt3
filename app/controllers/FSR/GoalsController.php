<?php

use Goals\Goals\GoalForm as Form;
use Goals\Goals\Repository\GoalRepositoryInterface as Repo;

class GoalsController extends CrudController {


public function __construct(Repo $repo, Form $form){
    parent::__construct($repo, $form, 'goal', 'Mål');

    }

    public function index($data = []){
        $data = \Goals\Goals\GoalFilterViewPresenter::present();
        return View::make('rapyd.pages', $data + ['title' => "Mål", 'links' => $this->buttonCreator->render($this->route, 'index', 'editor')]);
    }


}
