<?php

Event::listen('auth.login',function($event) {
	Log::info(Auth::id() . ' logged in.',array('context'=>'Auth.login'));
 });


Event::listen('users.create',function($user) {
	Log::info(Auth::user()->username . ' has created a new account in.',array('context'=>'users.create'));

	$data = array('detail' => 'Välkommen till röda tråden', 'name' => $user->username);
	Mail::send('emails.welcome',$data,function($message) use ($user)
	{
 		$message->to($user->email, $user->username)->subject('Välkommen till RT!');  
 	});

 });


	Event::Listen('RT.Registration.Events.UserRegistered', function ($event){
		
		echo ('Send a notification email');
	});



Event::listen('auth.token.valid', function($user)
{
  //Token is valid, set the user on auth system.
  Auth::setUser($user);
}); 