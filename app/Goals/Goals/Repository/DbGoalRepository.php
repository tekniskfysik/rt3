<?php namespace Goals\Goals\Repository;

use Acme\Repository\DbBaseRepository;


class DbGoalRepository extends DbBaseRepository implements GoalRepositoryInterface{
    protected $modelClassName = "Goals\Goals\Goal";
}