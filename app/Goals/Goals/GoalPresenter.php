<?php

namespace Goals\Goals;

use Acme\BasePresenter;

class GoalPresenter extends BasePresenter
{
    function setRoute()
    {
        $this->route = 'goals';
    }

    public function goalType(){
        return Goal::$goalTypes[$this->entity->type];
    }

    public function shortTitle(){
        $lim = 120;
        if(strlen($this->entity->title) > $lim)
            return substr($this->title, 0,$lim) . "...";
        return $this->title;
    }




} 