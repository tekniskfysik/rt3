<?php

namespace Goals\Goals;


class GoalFilterViewPresenter {

    public static function present($admin = true){

        $filter = \DataFilter::source(new Goal);
        $filter->attributes(array('class'=>'form-inline', 'id' => 'courseOccasionFilterForm'));
        $filter->add('title','Title', 'text');
        $filter->add('type','Typ', 'select')->scope('filterByType')->options(['' => 'Alla typer'] + Goal::$goalTypes  );


        $grid = \DataGrid::source($filter);  //same source types of DataSet
        $grid->attributes(['class' => 'table table-condensed table-hover']);

        $grid->add('id','Id');
        $grid->add('parent_id','Pid');
        $grid->add('code','Kod');
        $grid->add('shortTitle','Mål');
        $grid->add('goalType','Typ');

        $grid->paginate(15); //pagination

        if($admin){
            $grid->add('actionButtons|html','');
        }
        return compact('filter', 'grid');
    }


}


