<?php namespace Goals\Goals;
use Laracasts\Presenter\PresentableTrait;

/**
 * Goal
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property integer $parent_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Fsr[] $fsrs
 * @method static \Illuminate\Database\Query\Builder|\Goal whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Goal whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Goal whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Goal whereParentId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Goal whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Goal whereUpdatedAt($value) 
 */
class Goal extends \BaseModel
{
	use PresentableTrait;
	protected $presenter = 'Goals\Goals\GoalPresenter';
	protected $fillable = ['title', 'desc', 'parent_id', 'type'];
	public static $goalTypes = [ 0 => 'Lokala',  1 => 'Nationella', 2 => 'CDIO'];

	public function fsrs(){
		return $this->belongsToMany('Goals\Fsrs\Fsr')
								->withPivot('approved', 'requested', 'approved_by', 'requested_by')
								->withTimeStamps();
	}

	public function scopeFilterByType($query, $value){
		if ($value == '')
			return $query;
		return $query->where('type', '=', $value);
	}

  public function getLabelAttribute($value){
		if($this->type == 0)
			return '<a href="'.$this->url.'"><span data-toggle="tooltip" title="'.$this->title.'" class="label label-default">' . "L" . $this->code .'</span></a>';
			else if($this->type == 1)
				return '<a href="'.$this->url.'"><span data-toggle="tooltip" title="'.$this->title.'" class="label label-primary">' . "N" . $this->code .'</span></a>';

		return '<a href="'.$this->url.'"><span data-toggle="tooltip" title="'.$this->title.'" class="label label-info">' . "C".$this->code .'</span></a>';
	}

	public function getStubAttribute(){
		return substr(Goal::$goalTypes[$this->type],0,1) . $this->code;
	}

	public function parent(){
		return $this->belongsTo('Goals\Goals\Goal', 'parent_id', 'id');
	}
	
	public function childs(){
		return $this->hasMany('Goals\Goals\Goal', 'parent_id', 'id');
	}
}

