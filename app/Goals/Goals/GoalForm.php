<?php namespace Goals\Goals;

use Laracasts\Validation\FormValidator;
class GoalForm extends FormValidator{

    /*
     * Validation form for the block form
    */
    protected $rules = [
        'title' => 'required',
        'type' => 'required'
    ];


}
