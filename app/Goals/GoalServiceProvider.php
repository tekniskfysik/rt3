<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 08/08/14
 * Time: 16:28
 */

namespace Goals;

use Illuminate\Support\ServiceProvider;
use \Route;


class GoalServiceProvider extends ServiceProvider{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Goals\Goals\Repository\GoalRepositoryInterface', 'Goals\Goals\Repository\DbGoalRepository');
        $this->app->bind('Goals\Fsrs\Repository\FsrRepositoryInterface', 'Goals\Fsrs\Repository\DbFsrRepository');
    }

    public function boot(){
        Route::resource('goals', 'GoalsController');
        Route::get('fsrs/overview/', array('as' => 'fsrs.overview', 'uses' => 'FsrsController@overview'));
        Route::resource('fsrs', 'FsrsController');
        Route::post('fsrs/accept/{fsrs}', array('as' => 'fsrs.accept', 'uses' => 'FsrsController@accept'));
        Route::get('fsrs/export/{ids}', array('as' => 'fsrs.export', 'uses' => 'FsrsController@export'));
        Route::get('courses/{id}/fsrs', array('as' => 'fsrs.course', 'uses' => 'FsrsController@courseIndex'));


        Route::get('fsrtest', function () {
            //JavaScript::put(['comment' => ['user_id' => Auth::id(), 'commentable_type' => 'Course', 'commentable_id' => 1 ]]);
            return View::make('angular._fsr');
        });

    }
}