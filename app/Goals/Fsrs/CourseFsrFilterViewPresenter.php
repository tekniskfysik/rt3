<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 15/01/15
 * Time: 19:34
 */

namespace Goals\Fsrs;


use RT\Courses\CourseFilterViewPresenter;

class CourseFsrFilterViewPresenter extends CourseFilterViewPresenter{

    public function grid($source){
        $grid = \DataGrid::source($source);  //same source types of DataSet
        $grid->attributes(['class' => 'table table-striped table-condensed table-hover']);
        $grid->add('{{ link_to_route("fsrs.course", $title, $id) }}|html', 'Titel', 'title');
        $grid->add('goalLabels|html', 'Mål');
        return $grid;
    }


} 