<?php namespace Goals\Fsrs\Repository;

use Acme\Repository\DbBaseRepository;
use Goals\Fsrs\Fsr;


class DbFsrRepository extends DbBaseRepository implements FsrRepositoryInterface{
    protected $modelClassName = "Goals\Fsrs\Fsr";


    public function createWithRelations(array $attributes)
    {
        $fsr = parent::create($attributes);
        $this->updateRelations($fsr, $attributes);
        return $fsr;
    }

    public function updateWithRelations(Fsr $fsr, array $attributes)
    {
        $fsr->update($attributes);
        $this->updateRelations($fsr, $attributes);
    }

    private function updateRelations(Fsr $fsr, array $attributes)
    {
        if (array_key_exists('goals', $attributes)) {
            $fsr->goals()->sync($attributes['goals']);
        }
        else{
            $fsr->goals()->sync(array());
        }
    }

}