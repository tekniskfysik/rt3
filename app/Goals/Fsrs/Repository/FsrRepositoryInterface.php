<?php namespace Goals\Fsrs\Repository;

use \Acme\Repository\BaseRepository;
use Goals\Fsrs\Fsr;

interface FsrRepositoryInterface extends BaseRepository{

    public function createWithRelations(array $attributes);

    public function updateWithRelations(Fsr $fsr, array $attributes);

} 