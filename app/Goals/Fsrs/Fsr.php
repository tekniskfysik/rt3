<?php namespace Goals\Fsrs;
use Laracasts\Presenter\PresentableTrait;
use Carbon\Carbon;

/**
 * Fsr
 *
 * @property integer $id
 * @property string $title
 * @property integer $course_id
 * @property string $desc
 * @property integer hide
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Goal[] $goals
 * @property-read \Course $course
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereCourseId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Fsr whereUpdatedAt($value) 
 */
class Fsr extends \BaseModel {

    use PresentableTrait;
    protected $presenter = 'Goals\Fsrs\FsrPresenter';

	protected $fillable = ['title', 'desc', 'course_id', 'approved_by', 'requested_by', 'parent_id', 'revision', 'approved', 'hide'];

	public function goals(){
		return $this->belongsToMany('Goals\Goals\Goal')
			->withPivot('approved', 'requested', 'approved_by', 'requested_by')
			->withTimeStamps();
	}

	public function course(){
        return $this->belongsTo('RT\Courses\Course');
    }
    public function approvedUser(){
        return $this->belongsTo('Acme\Users\User', 'approved_by');
    }

    public function requestedUser(){
        return $this->belongsTo('Acme\Users\User', 'requested_by');
    }

    public function parent(){
        return $this->belongsTo('Goals\Fsrs\Fsr', 'parent_id', 'id');
    }

    public function childs(){
        return $this->hasMany('Goals\Fsrs\Fsr', 'parent_id', 'id');
    }

    public function scopeFilterByCategory($query, $value){
        if ($value == '')
            return $query;

        return $query->whereHas('course', function($query) use($value){
            return $query->whereHas('categories', function($query) use ($value){
                $query->where('category_id', '=', $value);
            });
        });
    }

    public function scopeFilterByDepartment($query, $value){
        if ($value == '')
            return $query;

        return $query->whereHas('course', function($query) use($value){
            $query->where('department_id', '=', $value);
        });
    }

    public function scopeFilterByGoal($query, $value){
        if ($value == '')
            return $query;

        return $query->whereHas('goals', function($query) use($value){
            $query->where('goal_id', '=', $value);
        });
    }



}