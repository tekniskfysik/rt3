<?php

namespace Goals\Fsrs;

use Acme\FilterPresenter;
use Acme\Presenter;
use Goals\Goals\Goal;
use RT\Categories\Category;
use RT\Courses\Course;
use RT\Departments\Department;

class FsrFilterViewPresenter implements FilterPresenter{

    private $course_id;
    public function __construct($course_id = null)
    {
        $this->course_id = $course_id;
    }

    public function present(){
        $filter = $this->filter();

        $grid = \DataGrid::source($filter);  //same source types of DataSet
        $grid->attributes(['class' => 'table table-condensed table-hover']);
        $grid->add('parent_id','', true);
        $grid->add('title','Titel', true);
        $grid->add('hide','Dold');

        if(!$this->course_id){
            $grid->add('{{ link_to_route("fsrs.course", $course->title, $course->id)}}','Kurs', false);
        }

        $grid->add('{{ implode(" ", $goals->lists("label")) }}', 'Mål');
        $grid->add('status|html','Status');
        $grid->add('updated_at','Ändrad', true);

        if($this->course_id){
            if(\Auth::user()->isLedning()){
                $grid->add('actionButtons|html','');
                $grid->add('approveButton|html','');
            }
            else{
                $grid->add('editButtons|html','');
            }
        }


        $grid->paginate(15); //pagination
        return compact('filter', 'grid');
    }


    public function filter()
    {
        $source = Fsr::with('course.categories', 'course.department', 'goals');
        if(! \Auth::user()->isLedning()){
            $source = Fsr::with('course.categories', 'course.department', 'goals');
        }
        if($this->course_id){
            $source = $source->where('course_id', '=', $this->course_id);
        }
        $filter = \DataFilter::source($source);

        $filter->attributes(array('class'=>'form-inline', 'id' => 'courseOccasionFilterForm'));

        if(!$this->course_id){
            $filter->add('course.title','Kurs', 'select')->options(['' => 'Alla kurser'] + Course::lists('title', 'id')  );
            $filter->add('category','Kategori', 'select')->scope('filterByCategory')->options(['' => 'Alla kategorier'] + Category::lists('title', 'id')  );
            $filter->add('department','Institution', 'select')->scope('filterByDepartment')->options(['' => 'Alla institutioner'] + Department::lists('title', 'id')  );
        }

        //$filter->add('hidden','Dold', 'select')->options(['' => 'Visa alla', 1 => 'Visa dolda', 0 => 'Visa giltiga']);

       $filter->add('code','Mål', 'select')->scope('filterByGoal')->options(['' => 'Alla mål'] + Goal::lists('code', 'id')  );
        $filter->add('approved','Godkänd', 'select')->options(['' => 'Status', 1 => 'Godkänd', 0 => 'Ej godkänd'])
            ->attributes(array('data-width'=>"150px" ));
        return $filter;
    }

    public function json(array $columns = [])
    {
        // TODO: Implement json() method.
    }
}


