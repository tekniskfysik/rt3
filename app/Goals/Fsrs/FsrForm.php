<?php namespace Goals\Fsrs;

use Laracasts\Validation\FormValidator;
class FsrForm extends FormValidator{

    /*
     * Validation form for the block form
    */
    protected $rules = [
        'course_id' => 'required'
    ];


}
