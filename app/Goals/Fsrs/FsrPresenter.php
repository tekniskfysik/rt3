<?php

namespace Goals\Fsrs;

use Acme\BasePresenter;

class FsrPresenter extends BasePresenter
{
    function setRoute()
    {
        $this->route = 'fsrs';
    }

    public function courseTitle(){
        return $this->course->title;
    }

    public function status(){
        if($this->entity->approvedUser)
            return '<span class="label label-success">'.$this->entity->approvedUser->username.'</span>';
        else if($this->entity->requestedUser())
            return '<span class="label label-danger">'.$this->entity->requestedUser->username.'</span>';
        return "";
    }

    public function button($title = null, $url = "", $method = 'POST', $class = 'btn btn-link'){
        $form = \Form::open(['method' => $method, 'url' => $url, 'style' => 'margin:0px; margin-top:-2px; padding:0px; clear: both; float:right;']);
        $form .= '<button style="margin:0; padding:0" type="submit" class="' . $class . '">';
        $form .= '<i class="glyphicon glyphicon-ok" style="color:green"></i>';
        $form .= $title;
        $form .= '</button>';
        $form .= \Form::close();
        return $form;
    }

    public function editButtons(){
        return "<div style='float:right'>" .$this->showButton() . " " . $this->editButton() . "</div>";
    }

    public function approveButton(){
        if(!$this->entity->approved){
            return $this->button("", $this->url('accept'));
        }

    }

} 