<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 26/12/14
 * Time: 17:05
 */

namespace Goals\Fsrs;

use Goals\Goals\Goal;
use RT\Courses\Course;

class FsrExporter {

    private static function exportCourse($summary, $courseId, $type, $printFsr = true)
    {
        $goals = Goal::whereType($type)->whereHidden(0)->get();
        $fsrs = Fsr::with('goals', 'course')->whereCourseId($courseId)->get();
        $course = Course::findOrFail($courseId);

        if ($printFsr) {
            $row[] = $course->title;
        } else {
            $row[] = "Titel";
        }
        foreach ($goals as $goal) {
            $row[] = $goal->code;
        }

        if ($printFsr || $summary->getHighestRow() < 2) {
            $summary->appendRow($row);
            $summary->row($summary->getHighestRow(), function ($row) {
                $row->setFontWeight('bold');
            });
        }

        $sum = [];
        foreach ($fsrs as $fsr) {
            $row = array();
            $row[] = substr($fsr->title, 0, 70);
            foreach ($goals as $goal) {
                if (in_array($goal->id, $fsr->goals->lists('id'))) {
                    $row[] = 1;
                    if (array_key_exists($goal->id, $sum)) {
                        $sum[$goal->id]++;
                    } else {
                        $sum[$goal->id] = 1;
                    }
                } else {
                    $row[] = 0;
                }
            }
            if ($printFsr) {
                $summary->appendRow($row);
            }
        }
        $row = array();
        if ($printFsr) {
            $row[] = 'Summa';
        } else{
            $row[] = $course->title;
        }

        foreach ($goals as $goal) {
            if (array_key_exists($goal->id, $sum)) {
                $row[] = $sum[$goal->id];
            } else {
                $row[] = 0;
            }
        }
        $summary->appendRow($row);
        if ($printFsr)
            $summary->row($summary->getHighestRow(), function ($row) {
                $row->setFontWeight('bold');
            });

        if ($printFsr)
            $summary->appendRow(['']);

        return $summary;

    }

    public static function export($course_ids = [1, 2, 4])
    {
        $excel = \Excel::create('export', function ($excel) use ($course_ids) {
            foreach (Goal::$goalTypes as $key=>$type) {
                $excel->sheet($type, function ($sheet) use ($course_ids, $key) {
                    foreach ($course_ids as $id) {
                        $sheet = self::exportCourse($sheet, $id, $key, false);
                    }
                    $sheet->appendRow(['']);
                    foreach ($course_ids as $id) {
                        $sheet = self::exportCourse($sheet, $id, $key, true);
                    }
                });
            }
        });

        return $excel->export('xls');
    }
} 