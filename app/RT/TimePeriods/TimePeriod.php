<?php namespace RT\TimePeriods;
use Laracasts\Presenter\PresentableTrait;

/**
 * Timeperiod
 *
 * @property string $title
 * @property string $title_eng
 * @property integer $week_offset
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Timeperiod whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Timeperiod whereTitleEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Timeperiod whereWeekOffset($value) 
 * @method static \Illuminate\Database\Query\Builder|\Timeperiod whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Timeperiod whereUpdatedAt($value) 
 */
class TimePeriod extends \BaseModel {

    protected $fillable = ['title', 'week_offset'];

    use PresentableTrait;
    protected $presenter = 'RT\TimePeriods\TimePeriodPresenter';

    public function getStartAttribute($value){
        return 'Läsperiod ' . ($value * 2 + 1);
    }

    public function occasions()
    {
        return $this->hasMany('RT\CourseOccasions\CourseOccasion');
    }

}
