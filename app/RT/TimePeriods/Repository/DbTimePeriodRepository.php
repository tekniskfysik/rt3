<?php namespace RT\TimePeriods\Repository;

use Acme\Repository\DbBaseRepository;


class DbTimePeriodRepository extends DbBaseRepository implements TimePeriodRepositoryInterface{
    protected $modelClassName = "RT\TimePeriods\TimePeriod";
}