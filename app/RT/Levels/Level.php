<?php namespace RT\Levels;

/**
 * Level
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $title_eng
 * @property string $desc_eng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $label
 * @method static \Illuminate\Database\Query\Builder|\Level whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereTitleEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereDescEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Level whereUpdatedAt($value) 
 */
class Level extends \Eloquent {

	protected $fillable = [];

	public function getLabelAttribute($value){
        return '<span data-toggle="tooltip" title="'.$this->title.'" class="label label-warning">' . substr($this->title, 0, 1) .'</span>';
    }

}