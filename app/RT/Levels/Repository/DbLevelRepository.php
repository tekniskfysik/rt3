<?php namespace RT\Levels\Repository;

use Acme\Repository\DbBaseRepository;


class DbLevelRepository extends DbBaseRepository implements LevelRepositoryInterface{
    protected $modelClassName = "RT\Levels\Level";

}