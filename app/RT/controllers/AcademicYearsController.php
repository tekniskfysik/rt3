<?php

use RT\AcademicYears\Repository\AcademicYearRepositoryInterface as Repository;
use RT\AcademicYears\AcademicYearForm as Form;

class AcademicYearsController extends \CrudController
{

    public function __construct(Repository $repo,
                                Form $form)
    {
        parent::__construct($repo, $form, 'academicyear', 'Läsår');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    protected function setMessages()
    {
        $this->messages = [
            'created' => 'Läsår tillagd.',
            'updated' => 'Läsår uppdaterad.',
            'deleted' => 'Läsår borttagen.'
        ];
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \RT\AcademicYears\AcademicYear);
        $presenter->add('year', 'År', true);
        return parent::filterIndex($presenter);
    }

    protected function redirect($instance)
    {
        return Redirect::route($this->route . '.index');
    }

}
