<?php

use \RT\PrivateCourses\Repository\PrivateCourseRepositoryInterface as Repository;
use \RT\PrivateCourses\PrivateCourseForm as Form;
use RT\Categories\Repository\CategoryRepositoryInterface as CategoryRepo;

class PrivateCoursesController extends CrudController
{
    protected $categoryRepo;
    protected $group = 'editor';

    public function __construct(Repository $repo, Form $form, CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
        parent::__construct($repo, $form, 'privatecourse', 'Egna kurser');
        //$this->beforeFilter('auth', array('only' => array('edit', 'update', 'create', 'store', 'destroy')));
    }

    public function index($data = [])
    {
        return $this->userIndex(Auth::user()->username);
    }


    private function isOwner($id){
        $instance = $this->repo->find($id);
        return $instance->user_id == Auth::id();
    }


    public function update($id){
        if($this->isOwner($id)){
            return parent::update($id);
        }
        Flash::overlay("Du kan bara ändra dina egna kurser", "Fel");
        return Redirect::route('privatecourses.index');
    }

    public function destroy($id){
        if($this->isOwner($id)){
            return parent::destroy($id);
        }
        Flash::overlay("Du kan bara ta bort dina egna kurser", "Fel");
        return Redirect::route('privatecourses.index');
    }


    public function userIndex($username)
    {
        $user = \Acme\Users\User::whereUsername($username)->first();
        $privatecourses = $user->privateCourses;

        $presenter = new \Acme\BaseFilterPresenter(RT\PrivateCourses\PrivateCourse::whereUserId($user->id));
        $presenter->add('title', 'Kurs', true);
        $presenter->add('year', 'År', true);
        $presenter->add('ects', 'Hp', true);
        $presenter->add('start', 'Start', true);
        $presenter->add('weeks', 'Veckor', true);
        $presenter->add('actionButtons|html', '');

        return parent::filterIndex($presenter);



        /** @var  $user Acme\Users\User */

        return View::make('privatecourses.index', compact('privatecourses'));
    }

    protected function formData()
    {
        return ['categoryRepo' => $this->categoryRepo];
    }

    protected function updateRelations($instance){
		$instance->blocks()->sync(Input::get('blocks') ?: array());
        $this->repo->updateWithRelations($instance, Input::all());
    }

}
