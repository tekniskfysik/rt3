<?php

use \RT\Blocks\Repository\BlockRepositoryInterface as Block;
use \RT\CourseOccasions\Repository\CourseOccasionRepositoryInterface as CourseOccasion;
use RT\PrivateCourses\Repository\PrivateCourseRepositoryInterface as PrivateCourse;

class BlockActionsController extends Controller
{
    /**@var Block * */
    private $block;

    /** @var  CourseOccasion */
    private $courseOccasion;

    /**@var PrivateCourse */
    private $privateCourse;

    private $block_id;

    private $occasion_id;

    public function __construct(Block $block, CourseOccasion $courseOccasion, PrivateCourse $privateCourse)
    {
        $this->block = $block;
        $this->courseOccasion = $courseOccasion;
        $this->privateCourse = $privateCourse;

        $this->block_id = Input::get('block_id');
        $this->occasion_id = Input::get('occasion_id');
    }

    public function addCourseOccasionToBlock()
    {
        $occasion = $this->courseOccasion->find($this->occasion_id);
        $this->block->removeCourseOccasion($occasion, $this->block_id); //To only get one unique course occasion in each block
        $this->block->addCourseOccasion($occasion, $this->block_id);
        return Redirect::back();
    }

    public function moveCourseOccasionInBlock()
    {
        $removeOccasion = $this->courseOccasion->find(Input::get('remove_occasion_id'));
        $this->block->removeCourseOccasion($removeOccasion, $this->block_id);

        $occasion = $this->courseOccasion->find($this->occasion_id);
        $this->block->addCourseOccasion($occasion, $this->block_id);
        return Redirect::back();
    }

    public function removeCourseOccasionFromBlock()
    {
        $occasion = $this->courseOccasion->find($this->occasion_id);
        $this->block->removeCourseOccasion($occasion, $this->block_id);
        return Redirect::back();
    }

    public function addPrivateCourseToBlock()
    {
        $occasion = $this->privateCourse->find($this->occasion_id);
        $block = $this->block->addCourseOccasion($occasion, $this->block_id);
        return Redirect::back();
    }

    public function removePrivateCourseFromBlock()
    {
        $occasion = $this->privateCourse->find($this->occasion_id);
        $block = $this->block->removeCourseOccasion($occasion, $this->block_id);
        return Redirect::back();
    }

}
