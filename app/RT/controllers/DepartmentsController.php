<?php

use RT\Departments\Repository\DbDepartmentRepository as Repository;
use RT\Departments\DepartmentForm as Form;

class DepartmentsController extends \CrudController {

    public function __construct(Repository $repo,
                                Form $form)
    {
        parent::__construct($repo, $form, 'department', 'Institutioner');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \RT\Departments\Department);
        $presenter->add('title', 'Institution', true);
        return parent::filterIndex($presenter);
    }

    protected function setMessages()
    {
        $this->messages = [
            'created' =>    'Institution tillagd.',
            'updated' =>    'Institution uppdaterad.',
            'deleted' =>    'Institution borttagen.'
        ];
    }

}
