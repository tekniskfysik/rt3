<?php

use RT\TimePeriods\Repository\TimePeriodRepositoryInterface as Repository;
use RT\TimePeriods\TimePeriodForm as Form;

class TimePeriodsController extends \CrudController {

    public function __construct(Repository $repo,
                                Form $form)
    {
        parent::__construct($repo, $form, 'timeperiod', 'Läsperioder');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new RT\TimePeriods\TimePeriod);
        $presenter->add('title', 'Läsperiod', true);
        $presenter->add('week_offset', 'Offset', true);
        return parent::filterIndex($presenter);
    }


    protected function setMessages()
    {
        $this->messages = [
            'created' =>    'Läsperiod tillagd.',
            'updated' =>    'Läsperiod uppdaterad.',
            'deleted' =>    'Läsperiod borttagen.'
        ];
    }

    protected function redirect($instance){
        return Redirect::route($this->route . '.index');
    }

}
