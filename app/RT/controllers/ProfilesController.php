<?php
use RT\Profiles\Repository\ProfileRepositoryInterface as Repository;
use RT\Profiles\ProfileForm as Form;
class ProfilesController extends CrudController {

	public function __construct(Repository $repo, Form $form)
    {
        parent::__construct($repo, $form, 'profile', 'Profiler');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function admin($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \RT\Profiles\Profile());
        $presenter->add('title', 'Profil', true);
        return parent::filterIndex($presenter);
    }

}
