<?php

use RT\Courses\Repository\CourseRepository;
use RT\Courses\CourseTransformer;
use \RT\Courses\Repository\CourseRepositoryInterface;
use \RT\Courses\CourseForm;
use RT\Categories\Repository\CategoryRepositoryInterface as CategoryRepo;
class CoursesController extends CrudController
{

    protected $categoryRepo;
    protected $courseTransformer;

    protected $group = 'editor';
    public function __construct(CourseRepositoryInterface $repo, CourseTransformer $courseTransformer,
                                CourseForm $form, CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
        $this->courseTransformer = $courseTransformer;
        parent::__construct($repo, $form, 'course', 'Kurser');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
        $this->beforeFilter('auth.ledning', array('only' => array('create')));
        $this->beforeFilter('auth.editor', array('only' => array('update', 'edit')));
    }

    public function fromCode($code){
        return $this->show(\RT\Courses\Course::whereCode($code)->firstOrFail()->id);
    }

    public function index($data = []){

        return $this->filterIndex(new RT\Courses\CourseFilterViewPresenter());
    }

    public function show($id, $data = [])
    {
        JavaScript::put(['comment' => ['user_id' => Auth::id(), 'commentable_id' => $id, 'commentable_type' => 'RT\Courses\Course'],
            'like' => ['user_id' => Auth::id(), 'likable_type' => 'RT\Courses\Course', 'likable_id' => $id]]);

        $goals = $this->repo->getGoals($id);
        return parent::show($id, compact('goals'));
    }

    public function edit($id){
        if(!$this->isEditor($id))
        {
            Flash::error('Du har inte behörighet att ändra denna kurs.');
            return Redirect::back();
        }
        return parent::edit($id);
    }

    protected function formData()
    {
        return ['categoryRepo' => $this->categoryRepo];
    }

    public function update($id)
    {
        $this->form = $this->form->excludeCourseCode(Input::get('code'));
        return parent::update($id);
    }


    protected function setMessages()
    {
        $this->messages = [
            'created' => 'Kurs tillagd.',
            'updated' => 'Kurs uppdaterad.',
            'deleted' => 'Kurs borttagen.'
        ];
    }

    protected function updateRelations($instance){
        $this->repo->updateWithRelations($instance, Input::all());
    }

    private function isEditor($course_id){
        if(Auth::user()->isLedning()){
            return true;
        }
        if(in_array($course_id, Auth::user()->courses->lists('id'))){
            return true;
        }
        return false;
    }

}
