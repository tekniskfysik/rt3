<?php
use RT\Tracks\Repository\TrackRepositoryInterface as Repository;
use RT\Tracks\TrackForm as Form;
class TracksController extends CrudController {

    public function __construct(Repository $repo, Form $form)
    {
        parent::__construct($repo, $form, 'track', 'Spår');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(\RT\Tracks\Track::with('profile'));
        $presenter->add('title', 'Spår', true);
        $presenter->add('profile.link', 'Tillhör profil');
        return parent::filterIndex($presenter);
    }

    /**
     * @param $track RT\Tracks\Track
     */
    protected function updateRelations($track)
    {
        $track->courses()->sync(Input::get('courses')?:array());
        $track->recommendedCourses()->sync(Input::get('recommended_courses')?:array());
    }

    protected function setMessages()
    {
        $this->messages = [
            'created' => 'Spår tillagt.',
            'updated' => 'Spår uppdaterat.',
            'deleted' => 'Spår borttaget.'
        ];
    }
}
