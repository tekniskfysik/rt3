<?php
use RT\CourseOccasions\CourseOccasion;
use \RT\CourseOccasions\Repository\CourseOccasionRepositoryInterface;
use \RT\CourseOccasions\CourseOccasionForm;
use RT\Courses\Repository\CourseRepositoryInterface as CourseRepo;
class CourseOccasionsController extends CrudController {

    protected $courseRepo;

	public function __construct(CourseOccasionRepositoryInterface $repo,
CourseOccasionForm $form, CourseRepo $courseRepo)
    {
        $this->courseRepo = $courseRepo;
        parent::__construct($repo, $form, 'courseoccasion', 'Kurstillfällen');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function index($data = []){
        if(Input::get('search') == null) {
        	if(date("m") < 7)
        		$year = date("Y")-1;
        	else
        		$year = date("Y");
        		
        	return Redirect::route('courseoccasions.index', ['year'=> $year, 'official' => 1,  'search' => 1]);
        }
        return $this->filterIndex(new \RT\CourseOccasions\CourseOccasionFilterViewPresenter(), false);
    }

    protected function setMessages(){
        $this->messages = [
            'created' => 'Kurstillfälle tillagt.',
            'updated' => 'Kurstillfälle uppdaterat.',
            'deleted' => 'Kurstillfälle borttaget.'
        ];
    }


    public function show($id, $data = [])
    {
        $occ = $this->repo->find($id);
        $goals = $this->courseRepo->getGoals($occ->course_id);
        return parent::show($id, compact('goals'));
    }

    protected function formData()
    {
        return ['courseRepo' => $this->courseRepo];
    }


}
