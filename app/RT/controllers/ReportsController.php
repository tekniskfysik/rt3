<?php

use RT\Reports\Repository\ReportRepositoryInterface as Repository;
use RT\Reports\ReportForm as Form;
class ReportsController extends \CrudController
{

    public function __construct(Repository $repo, Form $form)
    {
        parent::__construct($repo, $form, 'report', 'Rapporter');
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new RT\Reports\Report);
        $presenter->add('category', 'Kategori', true);
        $presenter->add('message|substr[0,20]', 'Meddelande', true);
        $presenter->add('status', 'Status', true);
        $presenter->add('created_at', 'Datum', true);
        $presenter->add('email', 'Mail', true);
        $presenter->add('url_to_page', 'SIDA', true);

        return parent::filterIndex($presenter);
    }

    public function show($id, $data = []){
        return $this->edit($id);
    }


    public function store()
    {
        $instance = $this->storePartial();
        Flash::overlay("Tack för ditt meddelande.", "Notering");
        return \Illuminate\Support\Facades\Redirect::back();
    }
}
