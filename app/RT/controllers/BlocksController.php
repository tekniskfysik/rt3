<?php

use \RT\Blocks\Repository\BlockRepositoryInterface as Repository;
use \RT\Blocks\BlockForm as Form;
use \RT\Blocks\Block;
use \RT\CourseOccasions\CourseOccasion;

/**
 * Class BlocksController
 */
class BlocksController extends CrudController
{
    public function __construct(Repository $repo, Form $form)
    {
        parent::__construct($repo, $form, 'block', 'Blockscheman');
        $this->beforeFilter('auth', array('only' => array('index')));
    }

    public function store(){
        $input = Input::all();
        if($input['blockable_id'] == 0){
            $input['blockable_id'] = Auth::id();
            $input['blockable_type'] = 'Acme\Users\User';
        }
        else{
            $input['blockable_type'] = 'RT\Tracks\Track';
		}

        $instance = parent::storePartial($input);

        if(array_has($input, 'block_id') && $input['block_id'] != ""){
			$block = Block::where('blockable_type', '=', 'RT\Tracks\Track')->where('title', '=', 'Basblock')->firstOrFail();
			$newId = [];
			$year = $input['start_year'] - $block->courseoccasions->lists('year')[0];
			$ids = $block->courseoccasions->lists('course_id');
			$years = $block->courseoccasions->lists('year');
			$period = $block->courseoccasions->lists('start');

			for ($i = 0; $i < count($block->courseoccasions->lists('id')); $i++) {	
				$newId[$i] = CourseOccasion::where('course_id', '=', $ids[$i])->where('year', '=', $year + $years[$i])
					->where('start', '=', $period[$i])->pluck('id');
			}
			$this->repo->attachOccasionsToBlock($instance, $newId);
		}
        return $this->redirect($instance);
    }



	protected function updateRelations($instance){}

    public function index($data = null, $source = null){

        if($source == null){
            if(!Auth::user()->isLedning()){
                return $this->userIndex(Auth::user()->username);
            }
            $source = Block::whereBlockableType('RT\Tracks\Track');
        }
        $source = $source->with('blockable');


        $presenter = new \Acme\BaseFilterPresenter($source);
        $presenter->add('title', 'Block', true);
        $presenter->add('blockable.link', '');
        $presenter->add('updated_at','', true);
        return parent::filterIndex($presenter);
    }


    public function userIndex($username)
    {
        $user = \Acme\Users\User::whereUsername($username)->first();
        $source = Block::whereBlockableType('Acme\Users\User')->where('blockable_id', '=', $user->id);
        return $this->index(null, $source);
    }


    /**
     * Display the specified block.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id, $data = [])
    {
        $links = $this->buttonCreator->render($this->route, 'show', 'editor', $id);

        JavaScript::put([
            'block_url'=> URL::route('blockJson', $id),
            'base_url' => URL::route('home'),
            'block_id' => $id
            ]);

        $block = Block::findOrFail($id);

        Breadcrumbs::addCrumb($block->title);

        $courses = [];
        foreach($block->courseOccasions as $occ){
            $courses[] = $occ->course_id;
	}
	$privatecourses = [];
	foreach($block->privateCourses as $priv) {
		$privatecourses[] = $priv->id;
	}
		
	//Makes site with javascript
	
	if (!$block->private || $block->blockable_id == Auth::id() || Auth::user()->isLedning()) {
		return View::make('blocks.d3', compact('blocks', 'links', 'block', 'courses', 'privatecourses'));
	}

	return Redirect::to('/');
    }

    /**
     * Return edit view for this block 
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $instance = $this->repo->find($id);

	if ($instance->blockable_id == Auth::id() || Auth::user()->isLedning()) { 
			Breadcrumbs::addCrumb($instance->title ?:$instance->id, $id);
			Breadcrumbs::addCrumb('Ändra');
			return View::make("{$this->route}.edit",
				[$this->model => $instance])->with(static::formData() +
					['links'=> $this->editLinks('edit', $id)]
				);
	}
	\Flash::error("Du kan inte redigera blockscheman som inte är dina.");
	return Redirect::back();
    }

    public function all()
    {
        $blocks = Block::all();

        return View::make('blocks.index', compact('blocks'));
    }


}
