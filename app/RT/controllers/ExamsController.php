<?php

use RT\Exams\Repository\ExamRepositoryInterface as Repository;
use RT\Exams\ExamForm as Form;
use RT\Categories\Repository\CategoryRepositoryInterface as CategoryRepo;

class ExamsController extends \CrudController {

    protected $categoryRepo;


    public function __construct(Repository $repo,
                                Form $form, CategoryRepo $categoryRepo)
    {
        $this->categoryRepo = $categoryRepo;
        parent::__construct($repo, $form, 'exam', 'Examens');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function index($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \RT\Exams\Exam);
        $presenter->add('title', 'Titel', true);
        return parent::filterIndex($presenter);
    }

    protected function updateRelations($instance){
        $this->repo->updateWithRelations($instance, Input::all());
    }

    protected function setMessages()
    {
        $this->messages = [
            'created' =>    'Examen tillagd.',
            'updated' =>    'Examen uppdaterad.',
            'deleted' =>    'Examen borttagen.'
        ];
    }

    protected function formData()
    {
        return ['categoryRepo' => $this->categoryRepo];
    }

}
