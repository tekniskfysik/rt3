<?php

use \RT\Categories\Repository\CategoryRepositoryInterface as Repository;
use \RT\Categories\CategoryForm as Form;
class CategoriesController extends CrudController {

    protected $courseRepo;
    public function __construct(Repository $repo,
                                Form $form, \RT\Courses\Repository\CourseRepositoryInterface $courseRepo)
    {
        $this->courseRepo = $courseRepo;
        parent::__construct($repo, $form, 'category', 'Kategorier');
        $this->beforeFilter('auth.admin', array('only' => array('destroy')));
    }

    public function admin($data = null){
        $presenter = new \Acme\BaseFilterPresenter(new \RT\Categories\Category());
        $presenter->add('title', 'Kategori', true);
        return parent::filterIndex($presenter);
    }

    public function index($data = null){
        $chart = new \RT\Exams\CategoryChart(new \RT\Exams\Repository\DbExamRepository());
        return parent::index(['PieChart' => $chart->pie()]);
    }

    public function create()
    {
        return parent::create()->with(['courseRepo' => $this->courseRepo]);
    }

    protected function formData(){
        return ['courseRepo' => $this->courseRepo];
    }

    protected function updateRelations($instance){
        $this->repo->updateWithRelations($instance, Input::all());
    }

    protected function setMessages()
    {
        $this->messages = [
            'created' =>    'Kategori tillagd.',
            'updated' =>    'Kategori uppdaterad.',
            'deleted' =>    'Kategori borttagen.'
        ];
    }


}
