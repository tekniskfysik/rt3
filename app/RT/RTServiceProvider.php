<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 08/08/14
 * Time: 16:28
 */

namespace RT;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\ServiceProvider;
use \Route;
use RT\Blocks\Block;
use RT\CourseOccasions\CourseOccasion;
use RT\Courses\Course;
use RT\PrivateCourses\PrivateCourse;

/**
 * Class RTServiceProvider
 * @package RT
 */
class RTServiceProvider extends ServiceProvider{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('RT\Courses\Repository\CourseRepositoryInterface', 'RT\Courses\Repository\DbCourseRepository');
        $this->app->bind('RT\CourseOccasions\Repository\CourseOccasionRepositoryInterface', 'RT\CourseOccasions\Repository\DbCourseOccasionRepository');
        $this->app->bind('RT\Blocks\Repository\BlockRepositoryInterface',  'RT\Blocks\Repository\DbBlockRepository');
        $this->app->bind('RT\PrivateCourses\Repository\PrivateCourseRepositoryInterface',  'RT\PrivateCourses\Repository\DbPrivateCourseRepository');
        $this->app->bind('RT\Profiles\Repository\ProfileRepositoryInterface',  'RT\Profiles\Repository\DbProfileRepository');
        $this->app->bind('RT\Tracks\Repository\TrackRepositoryInterface',  'RT\Tracks\Repository\DbTrackRepository');
        $this->app->bind('RT\Categories\Repository\CategoryRepositoryInterface',  'RT\Categories\Repository\DbCategoryRepository');
        $this->app->bind('RT\Departments\Repository\DepartmentRepositoryInterface',  'RT\Departments\Repository\DbDepartmentRepository');
        $this->app->bind('RT\AcademicYears\Repository\AcademicYearRepositoryInterface',  'RT\AcademicYears\Repository\DbAcademicYearRepository');
        $this->app->bind('RT\TimePeriods\Repository\TimePeriodRepositoryInterface',  'RT\TimePeriods\Repository\DbTimePeriodRepository');
        $this->app->bind('RT\Exams\Repository\ExamRepositoryInterface',  'RT\Exams\Repository\DbExamRepository');
        $this->app->bind('RT\Reports\Repository\ReportRepositoryInterface',  'RT\Reports\Repository\DbReportRepository');
    }

    public function boot(){

        Route::get('blocks/editCourse', array('uses' => 'BlocksController@editCourse'));

        Route::get('blocks/{id}/exportFsr', ['as' => 'blocks.exportFsr', function($id){
            $courses = Block::find($id)->courseOccasions->lists('course_id');
            \Goals\Fsrs\FsrExporter::export($courses);
        }]);

        Route::resource('academicyears', 'AcademicYearsController');
        Route::resource('timeperiods', 'TimePeriodsController');
        Route::resource('departments', 'DepartmentsController');
        Route::resource('profiles', 'ProfilesController');
        Route::resource('tracks', 'TracksController');
        Route::resource('courseoccasions', 'CourseOccasionsController');
        Route::resource('categories', 'CategoriesController');
        Route::resource('blocks', 'BlocksController');
        Route::resource('exams', 'ExamsController');
        Route::resource('privatecourses', 'PrivateCoursesController');
        Route::resource('courses', 'CoursesController');
        Route::resource('reports', 'ReportsController');

        Route::get('profiles-admin', [
            'as' => 'profiles.admin',
            'uses' => 'ProfilesController@admin'
        ]);

        Route::get('categories-admin', [
            'as' => 'categories.admin',
            'uses' => 'CategoriesController@admin'
        ]);

        Route::post('addCourseOccasionToBlock', [
            'as' => 'addCourseOccasionToBlock',
            'uses' => 'BlockActionsController@addCourseOccasionToBlock',
        ]);

        Route::post('moveCourseOccasionInBlock', [
            'as' => 'moveCourseOccasionInBlock',
            'uses' => 'BlockActionsController@moveCourseOccasionInBlock',
        ]);

        Route::post('removeCourseOccasionFromBlock', [
            'as' => 'removeCourseOccasionFromBlock',
            'uses' => 'BlockActionsController@removeCourseOccasionFromBlock',
        ]);

        Route::post('addPrivateCourseToBlock', [
            'as' => 'addPrivateCourseToBlock',
            'uses' => 'BlockActionsController@addPrivateCourseToBlock',
        ]);

        Route::post('removePrivateCourseFromBlock', [
            'as' => 'removePrivateCourseFromBlock',
            'uses' => 'BlockActionsController@removePrivateCourseFromBlock',
        ]);



        Route::get('blocks/all', array('uses' => 'BlocksController@all'));
        Route::get('blocks/addCourseOccasion', array('uses' => 'BlocksController@addCourseOccasion'));


        Route::get('courseApp', function () {
            return \View::make('angular.courseApp');
        });


        Route::get('courseInfo/{id}', function($id){
            return Courses\Course::with('occasions')->findOrFail($id);
        });

	Route::get('blockJson/{id}', ["as" => "blockJson", function($id){
		return Block::with('courseOccasions.course.categories')->with('courseOccasions.course.prerequisites')->with('privateCourses')->findOrFail($id);
	}]);

	Route::post('blockCourseOccasionModal', function(){

		$block_id = Input::get('block_id');
		if(Input::get('type') == 'private') {
			$course_id = Input::get('occasion_id');
			$privatecourse = PrivateCourse::findOrFail($course_id);
			$block = Block::findOrFail($block_id);
			
			return \View::make('privatecourses.modals.block', ['privatecourse' => $privatecourse, 'block_id' => $block->id]);
		} else { 
			$occasion_id = Input::get('occasion_id');
			$courseoccasion =  CourseOccasion::findOrFail($occasion_id);
			$block = Block::findOrFail($block_id);
			
			return \View::make('courseoccasions.modals.block', ['courseoccasion' => $courseoccasion, 'block_id' => $block->id]);
		}

	});

	Route::post('removeCourseOccasionFromBlockModal', function() {
		$occasion_id = Input::get('occasion_id');
		$block_id = Input::get('block_id');
		$title = Input::get('title');

		if(Input::get('type') == 'private') {
			return \View::make('blocks.modals.removePrivateCourse', ['occasion_id' => $occasion_id, 'block_id' => $block_id, 'title' => $title]);
		} else {
			return \View::make('blocks.modals.removeCourse', ['occasion_id' => $occasion_id, 'block_id' => $block_id, 'title' => $title]);
		}
	});

        Route::post('courseOccasionList', function(){
            $start = Input::get('start');
            $year = Input::get('year');
            $blockId = Input::get('block_id');
            //We give the whole timeperiod, assuming $start is [0, 10, 20, 30]
            $courseoccasions = CourseOccasion::with('course')
           		->where('year', '=', $year)
            	->whereRaw('start >= ? AND start < ?', [$start, $start+10])
            	->get();
            	
           return \View::make('blocks.partials.add-occasion-list', ['courseoccasions' => $courseoccasions, 'block_id' => $blockId]);
        });

    }
}
