<?php namespace RT\CourseOccasions;

use Laracasts\Validation\FormValidator;
class CourseOccasionForm extends FormValidator{

    /*
     * Validation form for the course form
    */

    protected $rules = [
        'course_id' => 'required',
        'contact_email' => 'email',
        'year' => 'required',
        'start' => 'required',
        'weeks' => 'required'
    ];


}
