<?php namespace RT\CourseOccasions\Repository;
use RT\CourseOccasions\CourseOccasion;
use Acme\Repository\DbBaseRepository;
use \Cache;

class DbCourseOccasionRepository extends DbBaseRepository implements CourseOccasionRepositoryInterface{
    protected $modelClassName = "RT\CourseOccasions\CourseOccasion";

    public function getAllSortedByYear()
    {
        $courseoccasions = Cache::remember('courseoccasions', 10, function()
        {
            return CourseOccasion::with('course')->orderBy('year')->get();
        });
        return $courseoccasions;
    }
}