<?php namespace RT\CourseOccasions\Repository;
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 21:36
 */
use \Acme\Repository\BaseRepository;
interface CourseOccasionRepositoryInterface extends BaseRepository{
    public function getAllSortedByYear();

} 