<?php namespace RT\CourseOccasions;

use \HTML;
use Laracasts\Presenter\PresentableTrait;
use RT\Blocks\Block;
use RT\Blocks\BlockableCourseInterface;

/**
 * CourseOccasion
 *
 * @property integer $id
 * @property integer $year
 * @property integer $start
 * @property integer $weeks
 * @property boolean $official
 * @property string $note
 * @property string $url_homepage
 * @property string $url_evaluation
 * @property string $url_syllabus
 * @property string $contact_name
 * @property string $contact_email
 * @property integer $course_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Course $course
 * @property-read mixed $full_title
 * @property-read mixed $title
 * @property-read mixed $period
 * @property-read mixed $link
 * @property-read mixed $year_link
 * @property-read mixed $add_button
 * @property-read mixed $level
 * @property-read \Illuminate\Database\Eloquent\Collection|\Block[] $blocks
 * @property-read mixed $remove_button
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereYear($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereStart($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereWeeks($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereOfficial($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereNote($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereUrlHomepage($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereUrlEvaluation($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereUrlSyllabus($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereContactName($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereContactEmail($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereCourseId($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\CourseOccasion whereUpdatedAt($value) 
 * @method static \RT\CourseOccasions\ actual()
 */
class CourseOccasion extends \BaseModel
    implements BlockableCourseInterface {

    use PresentableTrait;
    protected $presenter = 'RT\CourseOccasions\CourseOccasionPresenter';

    //protected $touches = array('blocks');

	protected $fillable = ['course_id', 'year', 'start', 'weeks', 'contact_name', 'contact_email', 'url_syllabus', 'url_evaluation', 'official'];

	public function course()
    {
		return $this->belongsTo('RT\Courses\Course')->select(['id','title', 'ects', 'level_id', 'department_id', 'code', 'desc']);
    }

    public function blocks(){
        return $this->belongsToMany('RT\Blocks\Block');
    }

    public function timeperiod(){
        return $this->belongsTo('RT\TimePeriods\TimePeriod', 'start', 'week_offset');
    }

    public function scopeOrderByCourse($query){
        return $query->join('courses as c', 'c.id', '=', 'course_occasions.course_id')
            ->orderBy('c.title', 'asc');
    }

    public function scopeFilterByCategory($query, $value){
        return $this->nestedRelationScope($query, $value, 'category_id', ['course', 'categories']);
    }

    public function scopeFilterByGoal($query, $value){
        return $this->nestedRelationScope($query, $value, 'goal_id', ['course', 'fsrs', 'goals']);
    }

    public function scopeFilterByDepartment($query, $value){
        return $this->relationScope($query, $value, 'department_id');
    }

    public function scopeFilterByLevel($query, $value){
      return $this->relationScope($query, $value, 'level_id');
    }

    public function getFullTitleAttribute($value) {
		return $this->course->title . '. ' . $this->year . ", " . $this->start;
	}

    public function getTitleAttribute($value) {
		return $this->course->title . ", " . $this->year;
	}

    public function addToBlock(Block $block)
    {
        $block->courseOccasions()->attach($this->id);
    }

    public function removeFromBlock(Block $block)
    {
        $block->courseOccasions()->detach($this->id);
    }

    public function getBlockData()
    {
        return [
            'start' => $this->start * 5,
            'length' => $this->weeks,
            'speed' => $this->course->ects / $this->weeks * 100,
            'id' => $this->id,
            'title' => $this->title,
            'row' => 0,
            'year' => $this->year,
            'ects' => $this->course->ects,
            'form' => '',
            'link' => $this->link,
            'removeRoute' => 'removeCourseOccasionFromBlock',
            'color' => 'yellow'
        ];
    }
}
