<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 23/08/14
 * Time: 19:56
 */

namespace RT\CourseOccasions;


use Acme\FilterPresenter;
use Goals\Goals\Goal;
use Illuminate\Support\Collection;
use RT\AcademicYears\AcademicYear;
use RT\Categories\Category;
use RT\Courses\Course;
use RT\Departments\Department;
use RT\Levels\Level;
use RT\TimePeriods\TimePeriod;

class CourseOccasionFilterViewPresenter implements FilterPresenter {

    public function filter(){
        $filter = \DataFilter::source(CourseOccasion::with('course.categories', 'course.department', 'timeperiod', 'course.fsrs.goals'));
        $filter->attributes(array('class'=>'form-inline', 'id' => ''));
        $filter->add('course_id', 'Kurs', 'select')->options(['' => ' Kurs'] + Course::orderBy('title')->lists('title', 'id'));
        $filter->add('year','År', 'select')->options(['' => 'Läsår'] + AcademicYear::orderBy('title')->lists('title', 'year'))
            ->attributes(array('data-width'=>"100px" ));
        $filter->add('start','Läsperiod', 'select')->options(['' => 'Läsperiod'] + TimePeriod::orderBy('title')->lists('title', 'week_offset')  )
            ->attributes(array('data-width'=>"150px" ));
        $filter->add('category','Kategori', 'select')->scope('filterByCategory')->options(['' => ' Kategorier'] + Category::orderBy('title')->lists('title', 'id')  )
            ->attributes(array('data-width'=>"200px" ));
        $filter->add('department','Institution', 'select')->scope('filterByDepartment')->options(['' => 'Institution'] + Department::orderBy('title')->lists('title', 'id'));
        $filter->add('official','Verifierad', 'select')->options(['' => 'Status', '0' => 'Ej verifierad', '1' => 'Verifierad'])
            ->attributes(array('data-width'=>"100px" ));

        return $filter;
    }

    public function json(array $columns = []){
        $data =  $this->filter()->source->get();
        $coll = new Collection();
        foreach($data as $row){
            $coll->push( (object) ['title' => $row->title, 'approved' => $row->approved, 'timeperiod' => $row->timeperiod->title, 'year'=> $row->year, 'weeks'=>$row->weeks]);
        }
        return $coll->toJson();
    }

    public function grid($source){
        $grid = \DataGrid::source($source);
        $grid->attributes(['class' => 'table table-condensed table-hover table-striped']);
        $grid->add('approvedButton|html', '');
        $grid->add('link|html','Kurs');
        $grid->add('year','År', true);
        $grid->add('timeperiod.title','Läsperiod', 'start');
        $grid->add('weeks','Längd');
        $grid->add('course.ects','Poäng');
        $grid->add('{{ implode(" ", $course->categories->lists("label")) }}','Kategorier'); 

        if(\Auth::user() && \Auth::user()->isLedning()){
            $grid->add('actionButtons|html','');
        }
        if(\Auth::check()){
            $grid->add('addToBlockButton|html','');
        }
        $grid->paginate(15);
        return $grid;
    }
    public function present(){
        $filter = $this->filter();
        $grid = $this->grid($filter);
        return compact('filter', 'grid');
    }


}


