<?php

namespace RT\CourseOccasions;

use Acme\BasePresenter;
use RT\Blocks\Repository\BlockRepositoryInterface;
use RT\Blocks\Repository\DbBlockRepository;

class CourseOccasionPresenter extends BasePresenter
{

    function setRoute()
    {
        $this->route = 'courseoccasions';
    }

    public function period()
    {
        return $this->start / 2 + 1;
    }

    public function addToBlockButton(){
        $blockRepo = new DbBlockRepository();
        $trackBlocks = $blockRepo->getTrackBlocks(5);
        $userBlocks = \Auth::user()->blocks;
        $view = \View::make('blocks.partials.add-occasion-select-block', [
            'blocks' => $trackBlocks->merge($userBlocks),
            'occasion_id' => $this->id,
            'route' => 'addCourseOccasionToBlock'
        ]);
        return $view->render();
    }

    public function approvedButton(){
        if($this->entity->official)
            return '<span data-toggle="tooltip" title="Godkänd" class="label label-success">G</span>';
        else
            return '<span data-toggle="tooltip" title="Ej godkänd" class="label label-danger">U</span>';
    }

}