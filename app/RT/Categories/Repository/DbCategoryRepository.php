<?php namespace RT\Categories\Repository;

use Acme\Repository\DbBaseRepository;
use RT\Categories\Category;


class DbCategoryRepository extends DbBaseRepository implements CategoryRepositoryInterface{
    protected $modelClassName = "RT\Categories\Category";

    public function updateWithRelations(Category $category, array $attributes)
    {
        $category->update($attributes);

        $this->updateRelations($category, $attributes);
    }

    private function updateRelations(Category $category, array $attributes)
    {

        if (array_key_exists('course', $attributes)) {
            $categories = $this->parseCategoryInput($attributes['course']);
            $category->courses()->sync($categories);
        }
    }

    public function createWithRelations(array $attributes)
    {
        $category = parent::create($attributes);

        $this->updateRelations($category, $attributes);

        return $category;
    }

    public function parseCategoryInput($categories)
    {
        $parsedArray = [];

        if($categories == [])
            return $parsedArray;

        $numberOfIds = count($categories['id']);
        $numberOfEcts =  count($categories['ects']);

        if($numberOfEcts != $numberOfIds)
            throw new \ErrorException('Illegal argument');

        for($i = 0; $i < $numberOfIds; $i++){
            $ects = $categories['ects'][$i];
            if($ects == 0)
                continue;
            $id =   $categories['id'][$i];
            $parsedArray[$id] =  ['ects' => $ects];
        }
        return $parsedArray;
    }

}