<?php namespace RT\Categories\Repository;

use \Acme\Repository\BaseRepository;
use RT\Categories\Category;

interface CategoryRepositoryInterface extends BaseRepository{
    public function updateWithRelations(Category $category, array $attributes);
    public function createWithRelations(array $attributes);

} 