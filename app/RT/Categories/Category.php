<?php namespace RT\Categories;
use Laracasts\Presenter\PresentableTrait;

/**
 * Category
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $title_eng
 * @property string $desc_eng
 * @property string $abbr
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $courses
 * @property-read \Illuminate\Database\Eloquent\Collection|\Exam[] $exams
 * @property-read mixed $link
 * @property-read mixed $stub
 * @property-read mixed $label
 * @property-read mixed $url
 * @method static \Illuminate\Database\Query\Builder|\Category whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereTitleEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereDescEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereAbbr($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Category whereUpdatedAt($value) 
 */
class Category extends \BaseModel {

    use PresentableTrait;
    protected $presenter = 'RT\Categories\CategoryPresenter';

	// Don't forget to fill this array
	protected $fillable = ['title', 'desc', 'abbr', 'image'];

	public function courses(){
			return $this->belongsToMany('RT\Courses\Course')->withPivot('ects');
    }

	public function privateCourses(){
			return $this->belongsToMany('RT\PrivateCourses\PrivateCourse')->withPivot('ects');
	}

    public function exams(){
			return $this->belongsToMany('RT\Exams\Exam')->withPivot('ects');
    }

    public function getLinkAttribute($value){
        return '<a href="' . $this->url . '">'. $this->title .'</a>';
    }

    public function getStubAttribute($value){
        return '<a class="btn btn-default btn-xs" tooltip="'.$this->title.'" href="' . $this->url . '">'. substr($this->title,0,3) .'</a>';
    }

    public function getLabelAttribute($value){
        return '<a href="'.$this->url.'"><span data-toggle="tooltip" title="'.$this->title.'" class="label label-default">' . $this->abbr .'</span></a>';
    }

    public function getUrlAttribute($value){
    	return route('categories.show', $this->id);
    }
}
