<?php namespace RT\Departments;
use Laracasts\Presenter\PresentableTrait;

/**
 * Departments
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $title_eng
 * @property string $desc_eng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $courses
 * @method static \Illuminate\Database\Query\Builder|\Departments whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereTitleEng($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereDescEng($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Departments whereUpdatedAt($value)
 */
class Department extends \BaseModel {

    use PresentableTrait;
    protected $presenter = 'RT\Departments\DepartmentPresenter';

	public static $messages = array(
        'required' => 'Man måste ange :attribute.',
    );

	// Don't forget to fill this array
	protected $fillable = ['title', 'desc', 'title_eng', 'desc_eng'];

	 public function courses(){
        return $this->hasMany('RT\Courses\Course');
    }

    protected function setModelName()
    {
        $this->model = 'department';
    }
}