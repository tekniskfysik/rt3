<?php

namespace RT\Departments;

use Acme\BasePresenter;
use RT\Blocks\Repository\BlockRepositoryInterface;
use RT\Blocks\Repository\DbBlockRepository;

class DepartmentPresenter extends BasePresenter
{

    function setRoute()
    {
        $this->route = 'departments';
    }


} 