<?php namespace RT\Departments\Repository;

use Acme\Repository\DbBaseRepository;


class DbDepartmentRepository extends DbBaseRepository implements DepartmentRepositoryInterface{
    protected $modelClassName = "RT\Departments\Department";

}