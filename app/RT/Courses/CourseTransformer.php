<?php namespace RT\Courses;



use Acme\Transformers\Transformer;

class CourseTransformer extends Transformer {
	 
	 public function transform($course){
    	return [
    		'title' => $course['title'], 
    		'id' => $course['id'],
    		'ects' => $course['ects'], 
    		'level' => $course['levelLabel'],
    		//'categories' => $course['labels']
    		'categories' => implode(" ", $course['categories']->lists('stub'))
    	];
    }
}
 
   