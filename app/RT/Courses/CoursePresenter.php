<?php namespace RT\Courses;

use Acme\BasePresenter;
use \Form;

class CoursePresenter extends BasePresenter {

    function setRoute()
    {
        $this->route = 'courses';
    }


    public function categoriesLinks(){
        $links = $this->categories->lists('link');
        return implode(", ", $links);
    }

    public function categoriesLabels(){
        $labels = $this->categories->lists('label');
        return implode(" ", $labels);
    }

    public function occasionsLinks(){
        $links = $this->occasions->lists('yearLink');
        return implode(", ", $links);
    }

    public function levelLabel(){
        return $this->level->label;
    }

    public function likedByAuthenticatedUser(){
        return in_array($this->entity->id, \Auth::User()->likedCourses->lists('likable_id'));
    }

    public function likeButton(){

        if($this->likedByAuthenticatedUser())
            $color = 'red';
        else{
            $color = 'black';
        }

        $data = Form::open(array('route' => 'api.likes.store'))
            . Form::hidden('likable_id', $this->id)
            . Form::hidden('likable_type', 'RT\Courses\Course');
        $data .= '  <button type="submit" class="btn btn-link pull-right" style="padding: 0px; color: '.$color.'">
                    <i class="glyphicon glyphicon-heart"></i>
                    </button>';
        $data .= Form::close();

        return $data;
    }



    public function labels(){
        return $this->levelLabel . ' ' . $this->categoriesLabels;
    }

    public function levelButton(){
        return '<a href="#"><span class="badge" data-toggle="tooltip" title="'.$this->level->title.'">'.substr($this->level->title, 0, 1).'</span></a>';
    }
}