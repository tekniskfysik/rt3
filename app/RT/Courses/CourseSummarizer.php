<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 31/12/14
 * Time: 16:07
 */

namespace RT\Courses;


use Goals\Goals\Goal;
use Illuminate\Support\Collection;
use RT\Courses\Repository\CourseRepositoryInterface;
use RT\Exams\Exam;
use RT\PrivateCourses\PrivateCourse;

class CourseSummarizer {

    protected $courses;
	protected $privatecourses;
	
    public function __construct($course_ids = [], $private_course_ids = []){
        $this->courses = Course::whereIn('id', $course_ids)->with('categories')->get();
		$this->privatecourses = PrivateCourse::whereIn('id', $private_course_ids)->with('categories')->get();
    }

    public function ects(){
        $sum = 0;
        foreach($this->courses as $course){
            $sum += $course->ects;
        }
		foreach($this->privatecourses as $privatecourse) {
			$sum += $privatecourse->ects;
		}
        return $sum;
    }


    public function category($exam_id = 1){
        $s = new Collection();
        $exam = Exam::with('categories')->findOrFail($exam_id);
        
        if(count($exam->categories) == 0)
        	return null;
        	
        foreach ($exam->categories as $cat) {
            $s->put($cat->id, (object)array('req' => floatval($cat->pivot->ects), 'sum' => 0,'category' => $cat, 'courses' => new Collection()));
        }
        foreach($this->courses as $course){
            foreach($course->categories as $cat){
                if($s->has($cat->id)){
                    $s->get($cat->id)->courses->push($course);
                    $s->get($cat->id)->sum += $cat->pivot->ects;
                }
            }
        }
        foreach($this->privatecourses as $course){
            foreach($course->categories as $cat){
                if($s->has($cat->id)){
                    $s->get($cat->id)->courses->push($course);
                    $s->get($cat->id)->sum += $cat->pivot->ects;
                }
            }
        }
        return $s;
    }

    public function plotCategory(){

        $series[0]['type'] = 'bar';
        $series[1]['type'] = 'bar';
        $series[0]['name'] = 'Krav';
        $series[1]['name'] = 'Summa';
        $xaxis = array();
        
        //Just not showing anything if no category related to course exsist (this must be added to private courses!)
		if( ($categories = $this->category()) == null) {
			return null;
		}
					
        foreach($this->category() as $cat){
            $series[0]['data'][] = $cat->req; //req
            $series[1]['data'][] = $cat->sum;
            $xaxis[] = substr($cat->category->title, 0, 40);
        }

        $data = [
            'series' => $series,
            'xaxis' => [['title' => '', 'categories' => $xaxis]],
            'yaxis' => 'Hp'
        ];
        $chart = new \Acme\Services\HighChart();
        return $chart->shared()->plot($data);
    }

    public function goals($types){
        $matrix = [];

        $courseMap = [];
        $courses = [];
        $courseCount = 0;

        $goalMap = [];
        $goals = [];
        $goalCount = 0;

        $allGoals = Goal::whereIn('type', $types)->lists('id');

        foreach($this->courses as $course){
            $courseMap[$course->id] = $courseCount;
            $courses[$courseCount] = $course->id;
            $matrix[$courseCount] = [];
            foreach($course->fsrs as $fsr){
                foreach($fsr->goals as $goal){
                    if(in_array($goal->id, $allGoals)){
                        if(!array_has($goalMap, $goal->id)){
                            $goalMap[$goal->id] = $goalCount;
                            $goals[$goalCount] = $goal->id;
                            $goalCount++;
                        }
                        $g = $goalMap[$goal->id];
                        if( !array_has($matrix[$courseCount], $g)){
                            $matrix[$courseCount][$g] = 0;
                        }
                        $matrix[$courseCount][$g]++;
                    }
                }
            }
            $courseCount++;
        }
        return compact(['matrix', 'goals', 'courses']);
    }

    public static function transpose($matrix){
        $out = [];
        foreach($matrix as $i => $row){
            foreach($row as $j => $col){
                $out[$j][$i] = $col;
            }
        }
        return $out;
    }

    public function plotGoalMatrix($types = [1, 2], $transposed = false){
        $chart = new \Acme\Services\HighChart();
        $a = $this->goals($types);
        $series = [];
        if(count($a['goals'])){

            $courses = Course::whereIn('id', array_values($a['courses']))->get();
            $goals = Goal::whereIn('id', array_values($a['goals']))->get();

            $matrix = $a['matrix'];
            if($transposed){
                $matrix = $this->transpose($matrix);
            }

            foreach($matrix as $i => $row){
                $data = [];
                foreach($row as $j => $col){
                    $data[] = [$j, $col];
                }
                if($transposed){
                    $series[] = ['data' => $data, 'type' => 'column', 'name' => $goals->find($a['goals'][$i])->code];
                }else{
                    $series[] = ['data' => $data, 'type' => 'column', 'name' => $courses->find($a['courses'][$i])->code];
                }
            }
            if($transposed){
                $xaxis = $courses->lists('code');
            }
            else{
                $xaxis = $goals->lists('code');
            }
            $data = [
                'series' => $series,
                'xaxis' => [['title' => 'X', 'categories' => $xaxis]],
            ];
        }else{
               return null;
        }


       return $chart->stacked()->shared()->plot($data);
    }

    public function printMatrix($types = [2], $route = 'course'){
        $table = '<table class="table table-condensed table-responsive table-hover" >';
        $data = $this->goals([$types]);
        $courses = Course::whereIn('id', $data['courses'])->get();
        $goals = Goal::whereIn('type', [$types])->whereHidden(0)->get();

        $table .= "<thead>";
        $table .= "<tr><td></td></td>";
        foreach($goals as $ii => $goal){
            $countt[$ii] = 0;
            $summ[$ii] = 0;

            $table .= "<td><span data-toggle='tooltip' title='{$goal->title}'>{$goal->code}</span></td>";

        }
        $table .= "</tr>";
        $table .= "</thead>";

        foreach($data['matrix'] as $i => $course){

            $sum = 0;
            $count = 0;
            $cid = $data['courses'][$i];
            $c = $courses->find($cid);
            if($route == 'fsr'){
                $table .= "<tr><td>{$c->code} - ". link_to_route('fsrs.course', $c->title, $c->id) . "</td>";
            }else{
                $table .= "<tr><td>{$c->code} - {$c->link}</td>";
            }
            foreach($goals as $ii => $goal){
                $table .= "<td>";
                $v = "-";
                foreach($course as $j => $val){
                    $gid = $data['goals'][$j];
                    if($goal->id == $gid){
                        $summ[$ii] += $val;
                        $countt[$ii] ++;
                        $v = $val;
                        $sum += floatval($val);
                        $count++;
                        break;
                    }
                }
                $table .= $v."</td>";
            }
            $table .= "<td style='border-left:2px solid'>{$count}</td>";
            //$table .= "<td>{$sum}</td>";
            $table .= "</tr>";
        }
        $table .= "<tfoot>";
        $table .="<tr style='border-top: 2px solid'><td><b>Antal kurser</b></td>";
        foreach($goals as $ii => $goal){
            $table .= "<td>{$countt[$ii]}</td>";
        }
        $table .= "</tr>";
        /*$table .="<tr><td><b>Sum</b></td>";
        foreach($goals as $ii => $goal){
            $table .= "<td>{$summ[$ii]}</td>";
        }
        $table .= "</tr>";*/
        $table .= "</tfoot>";

        $table .= "</table>";
        return $table;
    }



}
