<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 23/08/14
 * Time: 19:13
 */

namespace RT\Courses;


use Acme\FilterPresenter;
use Goals\Goals\Goal;
use Illuminate\Database\Eloquent\Collection;
use RT\Categories\Category;
use RT\Departments\Department;
use RT\Levels\Level;
use RT\Tracks\Track;
use Zofe\Rapyd\Facades\DataSet;

class CourseFilterViewPresenter implements FilterPresenter
{

    public function present()
    {
        $filter = $this->filter();

        $grid = $this->grid($filter);

        return compact('filter', 'grid');
    }

    public function grid($source){
        $grid = \DataGrid::source($source);  //same source types of DataSet
        $grid->attributes(['class' => 'table table-striped table-condensed table-hover']);
        $grid->add('link|html', 'Titel', 'title');
        $grid->add('ects', 'Poäng', true);
        $grid->add('level.label', 'Nivå');
        $grid->add('{{ implode(" ", $categories->lists("label")) }}', 'Kategorier');
				$grid->orderBy('title', 'asc');

        if (\Auth::user() && \Auth::user()->isLedning()) {
            $grid->add('actionButtons|html', '');
        }  if(\Auth::check()) {
            $grid->add('likeButton|html', '');
        }
        $grid->paginate(15);
        return $grid;
    }

    public function filter()
    {
        $filter = \DataFilter::source(Course::with('department', 'categories', 'level', 'tracks'));
        $filter->attributes(array('class'=>'form-inline', 'id' => 'test'));
        $filter->add('id', 'Kurs', 'select')->options(['' => ' Kurs'] + Course::orderBy('title')->lists('title', 'id'))
            ->attributes(array('data-live-search'=>"true"));
        $filter->add('categories.title','Kategori', 'select')->scope('filterByCategory')->options(['' => ' Kategorier'] + Category::orderBy('title')->lists('title', 'id')  )
            ->attributes(array('data-live-search'=>"true", ));
        $filter->add('tracks.title','Profil', 'select')->scope('filterByTrack')->options(['' => ' Profil'] + Track::orderBy('title')->lists('title', 'id')  )
            ->attributes(array('data-live-search'=>"true", ));
        $filter->add('level', 'Nivå', 'select')->options(['' => 'Nivå'] + Level::orderBy('title')->lists('title', 'id'))
            ->attributes(array('data-width'=>"150px", 'data-live-search'=>"true", ));
        //$filter->add('goals','Verifierad', 'select')->scope('filterByGoal')->options(['' => 'Mål'] + Goal::lists('code', 'id'))
        //    ->attributes(array('data-live-search'=>"true", ));
        $filter->add('department', 'Institution', 'select')->options(['' => 'Institution'] + Department::orderBy('title')->lists('title', 'id'))
            ->attributes(array('data-live-search'=>"true", ));

        return $filter;
    }

    public function json(array $columns = [])
    {
        return $this->set()->data->toJson();
    }

    public function set(){
        $filter = $this->filter();
        $filter->build();
        $set = DataSet::source($filter);
        $set->paginate(15);
        $set->build();
        return $set;
    }
}
