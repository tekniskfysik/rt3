<?php namespace RT\Courses;

use LaravelBook\Ardent\Ardent;
use Laracasts\Presenter\PresentableTrait;
/**
 * Course
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $code
 * @property float $ects
 * @property boolean $approved
 * @property string $note
 * @property string $title_eng
 * @property string $desc_eng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $department_id
 * @property integer $level_id
 * @property-read \Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Fsr[] $fsrs
 * @property-read \Level $level
 * @property-read \Illuminate\Database\Eloquent\Collection|\courseOccasion[] $occasions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Track[] $tracks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Track[] $recommendedInTracks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $prerequisites
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $qualifies
 * @property-read \Illuminate\Database\Eloquent\Collection|\Like[] $likes
 * @method static \Illuminate\Database\Query\Builder|\Course whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereDesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereEcts($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereApproved($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereTitleEng($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereDescEng($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereDepartmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Course whereLevelId($value)
 */
class Course extends \BaseModel
{
    use PresentableTrait;
    protected $presenter = 'RT\Courses\CoursePresenter';

    protected $fillable = ['title', 'desc', 'department_id', 'level_id', 'code', 'ects', 'approved', 'closed', 'url_evaluation', 'url_homepage'];

    public function department()
    {
        return $this->belongsTo('RT\Departments\Department');
    }

    public function fsrs()
    {
        return $this->hasMany('Goals\Fsrs\Fsr');
    }

    public function level()
    {
        return $this->belongsTo('RT\Levels\Level');
    }

    public function occasions()
    {
        return $this->hasMany('RT\CourseOccasions\CourseOccasion')->orderBy('year');
    }

    public function tracks()
    {
        return $this->belongsToMany('RT\Tracks\Track');
    }

    public function recommendedInTracks()
    {
        return $this->belongsToMany('RT\Tracks\Track', 'track_recommended_course');
    }

    public function categories()
    {
        return $this->belongsToMany('RT\Categories\Category')->withPivot('ects');
    }

    public function prerequisites()
    {
        return $this->belongsToMany('RT\Courses\Course', 'prerequisites', 'course_id', 'prereq_id')->withTimestamps();
    }

    public function qualifies()
    {
        return $this->belongsToMany('RT\Courses\Course', 'prerequisites', 'prereq_id', 'course_id')->withTimestamps();
    }

    public function likes()
    {
        return $this->morphMany('Like', 'likable');
    }

    public function getGoalLabelsAttribute(){
        $goals = [];
        foreach($this->fsrs as $fsr){
            $goals  += $fsr->goals->lists('id');
        }
        if(count($goals)){
           return implode(" ", \Goals\Goals\Goal::whereIn('id', $goals)->orderBy('id')->get()->lists("label")) ;
        }
        return null;
    }

    public function getNumberOfOccasionsAttribute(){
        return count($this->occasions->lists('id'));
    }

    public function scopeFilterByTrack($query, $value){
        return $this->relationScope($query, $value, 'track_id', 'tracks');
    }

    public function scopeFilterByCategory($query, $value){
        return $this->relationScope($query, $value, 'category_id', 'categories');
    }

    public function scopeFilterByGoal($query, $value){
        return $this->nestedRelationScope($query, $value, 'goal_id', ['fsrs', 'goals']);
    }
}
