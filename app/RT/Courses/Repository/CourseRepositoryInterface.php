<?php namespace RT\Courses\Repository;

use RT\Courses\Course;
use Acme\Repository\BaseRepository;

interface CourseRepositoryInterface extends BaseRepository {
    public function updateWithRelations(Course $course, array $attributes);
    public function createWithRelations(array $attributes);
    public function getGoals($id);
}