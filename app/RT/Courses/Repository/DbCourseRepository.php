<?php namespace RT\Courses\Repository;

use RT\Courses\Course;
use Acme\Repository\DbBaseRepository;
use \Input;
use SebastianBergmann\Exporter\Exception;

class DbCourseRepository extends DbBaseRepository implements CourseRepositoryInterface
{

    protected $modelClassName = "RT\Courses\Course";


    public function createWithRelations(array $attributes)
    {
        $course = parent::create($attributes);

        $this->updateRelations($course, $attributes);

        return $course;
    }

    public function updateWithRelations(Course $course, array $attributes)
    {
        $course->update($attributes);

        $course->touch();

        $this->updateRelations($course, $attributes);
    }



    private function getAttribute($key, $array, $default = []){
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        return $default;
    }

    private function updateRelations(Course $course, array $attributes = [])
    {

        $course->prerequisites()->sync($this->getAttribute('prerequisites', $attributes));
		
		// Needs to be checked if it can be commented
		// Everything works without it tho..
	
        //if(\Auth::user()->isLedning()){
        //    $course->users()->sync($this->getAttribute('users', $attributes));
        //}
        $course->tracks()->sync($this->getAttribute('tracks', $attributes));
        $course->recommendedInTracks()->sync($this->getAttribute('recommendedInTracks', $attributes));
        $categories = $this->parseCategoryInput($this->getAttribute('category', $attributes));
        $course->categories()->sync($categories);
    }


    public function parseCategoryInput($categories)
    {
        $parsedArray = [];

        if($categories == [])
            return $parsedArray;

        $numberOfIds = count($categories['id']);
        $numberOfEcts =  count($categories['ects']);

        if($numberOfEcts != $numberOfIds)
            throw new \ErrorException('Illegal argument');

        for($i = 0; $i < $numberOfIds; $i++){
            $ects = $categories['ects'][$i];
            if($ects == 0)
                continue;
            $id =   $categories['id'][$i];
            $parsedArray[$id] =  ['ects' => $ects];
        }
        return $parsedArray;
    }

    public function getGoals($id){
        $course = $this->find($id);
        $goals = array();
        foreach($course->fsrs as $fsr){
        $goals  += $fsr->goals->lists('id');
        }
        if(count($goals)){
            $goals = \Goals\Goals\Goal::whereIn('id', $goals)->get();
        }
        return $goals;
    }

}
