<?php namespace RT\Courses;

use Laracasts\Validation\FormValidator;
class CourseForm extends FormValidator{

    /*
     * Validation form for the course form
    */

    protected $rules = [
        'title' => 'required',
        'code' => 'required|alpha_num|unique:courses',
        'department_id' => 'required',
        'ects' => 'required',
    ];

	protected $messages =[
		'title.required' => 'Titel på kursen krävs',
		'code.required' => 'Fältet kod är obligatoriskt.',
		'department_id.required' => 'Fältet instutition är obligatoriskt.',
		'ects.required' => 'Fältet poäng är obligatoriskt.'
	];


    public function excludeCourseCode($code)
    {
        $this->rules['code'] = 'required|alpha_num';
        return $this;
    }

}
