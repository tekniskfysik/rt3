<?php namespace RT\Profiles;
use Laracasts\Presenter\PresentableTrait;

/**
 * Profiles
 *
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $title_eng
 * @property string $desc_eng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Track[] $tracks
 * @method static \Illuminate\Database\Query\Builder|\Profile whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereTitleEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereDescEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Profile whereUpdatedAt($value) 
 */
class Profile extends \BaseModel {

	protected $fillable = ['title', 'desc', 'title_eng', 'desc_eng', 'abbr', 'image'];

    use PresentableTrait;
    protected $presenter = 'RT\Profiles\ProfilePresenter';

	 public function tracks(){
			return $this->hasMany('RT\Tracks\Track');
    }

}