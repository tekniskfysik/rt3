<?php namespace RT\Profiles;

use Laracasts\Validation\FormValidator;
class ProfileForm extends FormValidator{

    /*
     * Validation form for the profile form
    */
    protected $rules = [
        'title' => 'required'
    ];


}
