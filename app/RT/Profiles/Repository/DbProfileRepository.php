<?php namespace RT\Profiles\Repository;

use Acme\Repository\DbBaseRepository;

class DbProfileRepository extends DbBaseRepository implements ProfileRepositoryInterface{
    protected $modelClassName = "RT\Profiles\Profile";

}