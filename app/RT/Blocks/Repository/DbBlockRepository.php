<?php namespace RT\Blocks\Repository;
use Illuminate\Support\Facades\Auth;
use RT\Blocks\Block;
use RT\Blocks\BlockableCourseInterface;
use Acme\Repository\DbBaseRepository;

class DbBlockRepository extends DbBaseRepository implements BlockRepositoryInterface{
    protected $modelClassName = "RT\Blocks\Block";



    public function addCourseOccasion(BlockableCourseInterface $occasion, $block_id)
    {
        $block = $this->find($block_id);
        if($this->isOwner($block) || Auth::user()->isLedning()){
            $occasion->addToBlock($block);
            \Flash::success($occasion->link . ' lades till i ' . $block->link . '.');
            return;
        }
        \Flash::error($occasion->link . ' kunde inte läggas till i ' . $block->link . '.');
        return $block;

    }

    public function removeCourseOccasion(BlockableCourseInterface $occasion, $block_id)
    {
        $block = $this->find($block_id);
        if($this->isOwner($block) || Auth::user()->isLedning()){
            $occasion->removeFromBlock($block);
            \Flash::success($occasion->link . ' togs bort från ' . $block->link . '.');
            return $block;
        }
        \Flash::error($occasion->link . ' kunde inte tas bort från ' . $block->link . '.');
        return $block;
    }

    public function attachOccasionsToBlock(Block $block, array $occasionIds){
        foreach($occasionIds as $id){
            $block->courseoccasions()->attach($id);
        }
    }

    private function isOwner(Block $block){
        if($block->blockable_id == Auth::id() && $block->blockable_type == 'Acme\Users\User')
            return true;
        return false;
    }

    public function getTrackBlocks($limit = 10)
    {
        if($limit = 0)
            return Block::ownedByTrack()->orderBy('updated_at')->get();
        return Block::ownedByTrack()->orderBy('updated_at')->take($limit)->get();
    }


}
