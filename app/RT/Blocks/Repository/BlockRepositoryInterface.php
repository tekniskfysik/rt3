<?php namespace RT\Blocks\Repository;
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 21:36
 */
use \Acme\Repository\BaseRepository;
use RT\Blocks\Block;
use RT\Blocks\BlockableCourseInterface;

interface BlockRepositoryInterface extends BaseRepository{

    public function addCourseOccasion(BlockableCourseInterface $course, $block_id);

    public function removeCourseOccasion(BlockableCourseInterface $course, $block_id);

    public function getTrackBlocks();

    public function attachOccasionsToBlock(Block $block, array $occasionIds);

} 