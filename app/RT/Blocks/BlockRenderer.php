<?php namespace RT\Blocks;


class BlockRenderer
{
    protected $courses;
    protected $matrix;


    public function render($courses)
    {
        $years = $this->splitYears($courses);
        foreach($years as $key=>$year){
            $years[$key]['courses'] = $this->renderYear($years[$key]['courses']);
            $years[$key]['rows'] = $this->calculateNumberOfRows();
        }
        return $years;
    }

    public function renderYear($courses){
        $this->matrix = $this->createMatrix(40);
        $this->courses = $courses;
        $this->prepareBlock();
        return $this->courses;
    }

    private function splitYears($courses){
        $years = array();
        foreach($courses as $course){
            if(! isset($years[$course['year']]))
                $years[$course['year']]['courses'] = [];
            array_push($years[$course['year']]['courses'], $course);
        }
        return $years;
    }
    private function createMatrix($elements)
    {
        $matrix = array();
        for ($i = 0; $i < $elements; $i++) {
            array_push($matrix, array());
        }
        return $matrix;
    }

    private function getFreeRow($course)
    {
        $emptyRows = 0;
        for ($row = 0; $row < 1000 && $emptyRows < $course['speed']; $row++) {
            if ($this->isEmptyRow($course, $row))
                $emptyRows++;
            else {
                $emptyRows = 0;
            }
        }
        $course['row'] = $row - $course['speed'];
        return $course;
    }

    private function isEmptyRow($course, $row)
    {
        $sum = 0;
        for ($col = $course['start']; $col < ($course['start'] + $course['length']); $col++) {
            if (!isset($this->matrix[$col][$row])) {
                array_push($this->matrix[$col], 0);
            }
            $sum += $this->matrix[$col][$row];

        }
        return $sum == 0;
    }

    private function allocateRows($course)
    {
        for ($week = $course['start']; $week < ($course['start'] + $course['length']); $week++) {
            for ($i = $course['row']; $i < ($course['row'] + $course['speed']); $i++) {
                $this->matrix[$week][$i] = $course['id'];
            }
        }
    }

    private function prepareBlock()
    {

        for ($i = 0; $i < count($this->courses); $i++) {
            $this->courses[$i] = $this->getFreeRow($this->courses[$i], $this->matrix);
            $this->allocateRows($this->courses[$i], $this->matrix);
        }

    }

    private function calculateNumberOfRows()
    {
        $maxRows = 0;
        for ($i = 0; $i < count($this->matrix); $i++) {
            $rows = count($this->matrix[$i]);
            if ($rows > $maxRows) {
                $maxRows = $rows;
            }
        }
        return $maxRows;
    }


}




