<?php namespace RT\Blocks;

use Laracasts\Validation\FormValidator;
class BlockForm extends FormValidator{

    /*
     * Validation form for the block form
    */
    protected $rules = [
        'title' => 'required',
        'start_year' => 'required'
    ];
}
