<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 29/08/14
 * Time: 10:00
 */

namespace RT\Blocks;

use Acme\BasePresenter;

class BlockPresenter extends BasePresenter
{

    function setRoute()
    {
        $this->route = 'blocks';
    }
}