<?php namespace RT\Blocks;
use Illuminate\Support\Collection;
use \Auth;
use Laracasts\Presenter\PresentableTrait;
use RT\Categories\Category;
use RT\CourseOccasions\CourseOccasion;
use RT\Exams\Exam;
use RT\PrivateCourses\PrivateCourse;

/**
 * Block
 *
 * @property integer $id
 * @property string $title
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $blockable_id
 * @property string $blockable_type
 * @property-read \ $blockable
 * @property-read \Track $track
 * @property-read \Illuminate\Database\Eloquent\Collection|\PrivateCourse[] $privatecourses
 * @property-read \Illuminate\Database\Eloquent\Collection|\CourseOccasion[] $courseoccasions
 * @property-read mixed $url
 * @property-read mixed $link
 * @method static \Illuminate\Database\Query\Builder|\Block whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereNote($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereBlockableId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Block whereBlockableType($value) 
 * @method static \Block my() 
 */
class Block extends \BaseModel{

	use PresentableTrait;
	protected $presenter = 'RT\Blocks\BlockPresenter';

	protected $fillable = ['blockable_id', 'blockable_type', 'title', 'start_year', 'private', 'desc'];

	public function scoperecentUpdated($query) {
		return $query->orderBy('updated_at', 'desc')->where('blockable_type', '=', 'RT\Tracks\Track')
			->where('private', '=', 0)->take(5);
	}

	public function blockable(){
		return $this->morphTo();
	}

	// Added this to edit blocks in administration. Kind of an ugly hack. Use blockable in other cases. 
	public function track(){
		return $this->belongsTo('RT\Tracks\Track', 'blockable_id');
	}

	public function privateCourses(){
		return $this->belongsToMany('RT\PrivateCourses\PrivateCourse')->orderBy('year')->orderBy('start');
	}

	public function courseOccasions(){
		return $this->belongsToMany('RT\CourseOccasions\CourseOccasion')->orderBy('year')->orderBy('start');
	}


	/*
	 * Scopes
	 */ 

	public function scopeMy($query)
    {
        return $query->where('blockable_type', '=', 'Acme\Users\User')->where('blockable_id', '=', Auth::id());
    }

    public function scopeOwnedByTrack($query){
        return $query->whereBlockableType('RT\Tracks\Track');
    }


    /*
     * Attributes
     */



    /* 
     * Methods
     */

	public function totalEcts(){
		$sum = 0;
		foreach ($this->courseOccasions as $courseOccasion) {
			$sum += $courseOccasion->course->ects;
		}
		return $sum;
	}

	public function categorySummary(){
		$s = new Collection;

		foreach (Category::all() as $cat){
			$s->put($cat->id, (object)array('id'=> $cat->id, 'req' => 0, 'sum' => 0, 'percent' => 0,'title' => $cat->title, 'courses' => array()));
		}

		$exam = Exam::findOrFail(1);
		foreach ($exam->categories as $cat) {
			$s->get($cat->id)->req += $cat->pivot->ects;
		}

		foreach ($this->courseOccasions as $courseOccasion) {
			foreach ($courseOccasion->course->categories as $cat) {
				$s->get($cat->id)->sum += $cat->pivot->ects;
				$course = $courseOccasion->course;
				$url = '<a href="'.route('courseoccasions.show', $courseOccasion->id).'">'.$course->title. ', '.$courseOccasion->year.' ('.$cat->pivot->ects.')</a>';
				array_push($s->get($cat->id)->courses, $url);
			}	
		}
		foreach (Category::all() as $cat){
			if($s->get($cat->id)->req != 0)
				$s->get($cat->id)->percent = round($s->get($cat->id)->sum / $s->get($cat->id)->req*100) . "%";
		}

		return $s;
	}

	public function moveYears() {

	}

}
