<?php

namespace RT\Blocks;


interface BlockableCourseInterface
{
    public function addToBlock(Block $block);

    public function removeFromBlock(Block $block);

    public function getBlockData();

} 