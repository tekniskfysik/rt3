<?php namespace RT\PrivateCourses\Repository;
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 21:36
 */
use RT\PrivateCourses\PrivateCourse;
use \Acme\Repository\BaseRepository;
interface PrivateCourseRepositoryInterface extends BaseRepository{

    public function updateWithRelations(PrivateCourse $privateCourse, array $attributes);
} 
