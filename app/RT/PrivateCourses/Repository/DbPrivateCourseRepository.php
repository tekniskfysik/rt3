<?php namespace RT\PrivateCourses\Repository;
use Acme\Repository\DbBaseRepository;

use RT\PrivateCourses\PrivateCourse;

class DbPrivateCourseRepository extends DbBaseRepository implements PrivateCourseRepositoryInterface{
    protected $modelClassName = "RT\PrivateCourses\PrivateCourse";

    public function updateWithRelations(PrivateCourse $privateCourse, array $attributes)
    {
        $privateCourse->update($attributes);

        $privateCourse->touch();

        $this->updateRelations($privateCourse, $attributes);
    }

    private function updateRelations(PrivateCourse $privateCourse, array $attributes = [])
    {
        $categories = $this->parseCategoryInput($this->getAttribute('category', $attributes));
        $privateCourse->categories()->sync($categories);
    }

    private function getAttribute($key, $array, $default = []){
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        return $default;
    }

    public function parseCategoryInput($categories)
    {
        $parsedArray = [];

        if($categories == [])
            return $parsedArray;

        $numberOfIds = count($categories['id']);
        $numberOfEcts =  count($categories['ects']);

        if($numberOfEcts != $numberOfIds)
            throw new \ErrorException('Illegal argument');

        for($i = 0; $i < $numberOfIds; $i++){
            $ects = $categories['ects'][$i];
            if($ects == 0)
                continue;
            $id =   $categories['id'][$i];
            $parsedArray[$id] =  ['ects' => $ects];
        }
        return $parsedArray;
    }
}
