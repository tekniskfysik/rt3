<?php

namespace RT\PrivateCourses;

use Acme\BasePresenter;

class PrivateCoursePresenter extends BasePresenter
{

    public function categoriesLabels(){
        $labels = $this->categories->lists('label');
        return implode(" ", $labels);
	}

    function setRoute()
    {
        $this->route = 'privatecourses';
    }


} 
