<?php namespace RT\PrivateCourses;
use Illuminate\Support\Facades\Auth;
use Laracasts\Presenter\PresentableTrait;
use RT\Blocks\Block;
use RT\Blocks\BlockableCourseInterface;

/**
 * PrivateCourse
 *
 * @property integer $id
 * @property string $title
 * @property integer $year
 * @property float $ects
 * @property string $note
 * @property integer $start
 * @property integer $user_id
 * @property integer $weeks
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Block[] $blocks
 * @property-read mixed $link
 * @property-read mixed $remove_button
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereYear($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereEcts($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereNote($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereStart($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereUserId($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereWeeks($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\PrivateCourse whereUpdatedAt($value) 
 */
class PrivateCourse extends \BaseModel implements BlockableCourseInterface{

	protected $fillable = ['title', 'ects', 'year', 'start', 'weeks', 'user_id'];

    use PresentableTrait;
    protected $presenter = 'RT\PrivateCourses\PrivateCoursePresenter';

    public function blocks(){
        return $this->belongsToMany('RT\Blocks\Block');
    }

    public function categories()
    {
        return $this->belongsToMany('RT\Categories\Category')->withPivot('ects');
    }
    /**
     * Update user id to authenticated user before saving.
     *
     * @param array $options
     * @return bool|void
     */
    public function save(array $options = array())
    {
        $this->user_id = \Auth::id();
        parent::save();
    }

    public function getLinkAttribute($value){
		return '<a href="' . route('privatecourses.show', $this->id) . '">'. $this->title .'</a>';
	}



    public function getRemoveButtonAttribute($value){
		return HTML::deleteButton('', route('privatecourses.destroy', $this->id), 'btn-xs');
	}

    private function isOwner(){
        return Auth::id() == $this->user_id;
    }

    public function addToBlock(Block $block)
    {
        if($this->isOwner()){
            if($this->isNotInBlock($block))
                $block->privateCourses()->attach($this->id);
        }

    }

    private function isNotInBlock(Block $block){

        return !array_key_exists($this->id, $block->privateCourses()->lists('private_course_id', 'private_course_id'));
    }

    public function removeFromBlock(Block $block)
    {
        if($this->isOwner()){
            $block->privateCourses()->detach($this->id);
        }

    }

    public function getBlockData()
    {
        return[
            'start' => $this->start * 5,
            'length' => $this->weeks,
            'speed' => $this->ects / $this->weeks * 100,
            'id' => $this->id,
            'title' => 'sd',
            'row' => 0,
            'year' => $this->year,
            'ects' => $this->ects,
            'form' => '',
            'link' => $this->link,
            'removeRoute' => 'removePrivateCourseFromBlock',
            'color' => 'red'
        ];

    }
}
