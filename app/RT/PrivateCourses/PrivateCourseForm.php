<?php namespace RT\PrivateCourses;

use Laracasts\Validation\FormValidator;
class PrivateCourseForm extends FormValidator{

    /*
     * Validation form for the form
    */
    protected $rules = [
        'title' => 'required',
        'year' => 'required|integer|min:1990|max:2050',
        'weeks' => 'required|integer|min:0|max:40',
        'start' => 'required|integer|min:0|max:40',
    ];

	protected $messages = [
		'title.required' => 'Titel på kursen krävs.',
		'year.required' => 'Fältet år är obligatoriskt.',
		'weeks.required' => 'Fältet veckor är obligatoriskt.',
		'start.required' => 'Startår på kursen krävs.'
	];
}
