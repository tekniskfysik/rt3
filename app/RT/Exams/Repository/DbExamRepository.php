<?php namespace RT\Exams\Repository;

use Acme\Repository\DbBaseRepository;
use RT\Exams\Exam;


class DbExamRepository extends DbBaseRepository implements ExamRepositoryInterface{
    protected $modelClassName = "RT\Exams\Exam";

    public function updateWithRelations(Exam $exam, array $attributes)
    {
        $exam->update($attributes);

        $this->updateRelations($exam, $attributes);
    }

    private function updateRelations(Exam $exam, array $attributes)
    {

        if (array_key_exists('category', $attributes)) {
            $categories = $this->parseCategoryInput($attributes['category']);
            $exam->categories()->sync($categories);
        }
    }

    public function createWithRelations(array $attributes)
    {
        $exam = parent::create($attributes);

        $this->updateRelations($exam, $attributes);

        return $exam;
    }

    public function parseCategoryInput($categories)
    {
        $parsedArray = [];

        if($categories == [])
            return $parsedArray;

        $numberOfIds = count($categories['id']);
        $numberOfEcts =  count($categories['ects']);

        if($numberOfEcts != $numberOfIds)
            throw new \ErrorException('Illegal argument');

        for($i = 0; $i < $numberOfIds; $i++){
            $ects = $categories['ects'][$i];
            if($ects == 0)
                continue;
            $id =   $categories['id'][$i];
            $parsedArray[$id] =  ['ects' => $ects];
        }
        return $parsedArray;
    }
}