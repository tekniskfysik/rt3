<?php namespace RT\Exams\Repository;

use \Acme\Repository\BaseRepository;
use RT\Exams\Exam;

interface ExamRepositoryInterface extends BaseRepository{
    public function updateWithRelations(Exam $exam, array $attributes);
    public function createWithRelations(array $attributes);
} 