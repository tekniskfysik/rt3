<?php namespace RT\Exams;
use Laracasts\Presenter\PresentableTrait;

/**
 * Exam
 *
 * @property integer $id
 * @property string $title
 * @property integer $year
 * @property float $ects
 * @property string $code
 * @property string $note
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $categories
 * @method static \Illuminate\Database\Query\Builder|\Exam whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereYear($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereEcts($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereCode($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereNote($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Exam whereUpdatedAt($value) 
 */
class Exam extends \BaseModel {
    
	protected $fillable = ['title', 'ects', 'note', 'year'];

	public function categories(){
	    return $this->belongsToMany('RT\Categories\Category')->withPivot('ects')->orderBy('ects', 'desc');;
	}

    use PresentableTrait;
    protected $presenter = 'RT\Exams\ExamPresenter';


    public function blocks()
    {
        return $this->morphMany('RT\Blocks\Block', 'blockable');
    }

}