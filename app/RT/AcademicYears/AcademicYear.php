<?php namespace RT\AcademicYears;
use Laracasts\Presenter\PresentableTrait;

/**
 * AcademicYear
 *
 * @property integer $year
 * @property string $title
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\AcademicYear whereYear($value) 
 * @method static \Illuminate\Database\Query\Builder|\AcademicYear whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\AcademicYear whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\AcademicYear whereUpdatedAt($value) 
 */
class AcademicYear extends \BaseModel {

    use PresentableTrait;
    protected $presenter = 'RT\AcademicYears\AcademicYearPresenter';


	protected $fillable = ['year', 'title'];




}