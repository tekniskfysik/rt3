<?php namespace RT\AcademicYears;

use Acme\BasePresenter;
use \Form;

class AcademicYearPresenter extends BasePresenter {

    function setRoute()
    {
        $this->route = 'academicyears';
    }
}