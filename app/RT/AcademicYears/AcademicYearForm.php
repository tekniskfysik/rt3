<?php namespace RT\AcademicYears;

use Laracasts\Validation\FormValidator;
class AcademicYearForm extends FormValidator{

    protected $rules = [
        'title' => 'required'
    ];


}
