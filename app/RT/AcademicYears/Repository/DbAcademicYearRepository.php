<?php namespace RT\AcademicYears\Repository;

use Acme\Repository\DbBaseRepository;

class DbAcademicYearRepository extends DbBaseRepository implements AcademicYearRepositoryInterface
{
    protected $modelClassName = "RT\AcademicYears\AcademicYear";

}