<?php namespace RT\Reports;

use Laracasts\Validation\FormValidator;
class ReportForm extends FormValidator{

    /*
     * Validation form for the profile form
    */
    protected $rules = [
        'message' => 'required',
        'email' => 'email'
    ];

}
