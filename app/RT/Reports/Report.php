<?php namespace RT\Reports;

use Laracasts\Presenter\PresentableTrait;

class Report extends \BaseModel {

    use PresentableTrait;
    protected $presenter = 'RT\Reports\ReportPresenter';


	// Don't forget to fill this array
	protected $fillable = ['message', 'email', 'category', 'status', 'comment', 'url_to_page'];
}