<?php namespace RT\Reports\Repository;

use Acme\Repository\DbBaseRepository;

class DbReportRepository extends DbBaseRepository implements ReportRepositoryInterface{
    protected $modelClassName = "RT\Reports\Report";

}