<?php

namespace RT\Reports;

use Acme\BasePresenter;

class ReportPresenter extends BasePresenter
{

    function setRoute()
    {
        $this->route = 'reports';
    }

    public function title(){
        return $this->entity->category . " (" . $this->entity->email . ") - " . $this->entity->status;
    }

    public function actionButtons(){
        return "<div style='float:right'>" . " " . $this->editButton() . " " . $this->removeButton() . "</div>";
    }


} 