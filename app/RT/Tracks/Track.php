<?php namespace RT\Tracks;
use Laracasts\Presenter\PresentableTrait;

/**
 * Track
 *
 * @property integer $id
 * @property string $title
 * @property boolean $valid
 * @property string $desc
 * @property string $title_eng
 * @property string $desc_eng
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $profile_id
 * @property-read \Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $courses
 * @property-read \Illuminate\Database\Eloquent\Collection|\Course[] $recommendedCourses
 * @property-read \Illuminate\Database\Eloquent\Collection|\Block[] $blocks
 * @method static \Illuminate\Database\Query\Builder|\Track whereId($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereTitle($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereValid($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereDesc($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereTitleEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereDescEng($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereCreatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereUpdatedAt($value) 
 * @method static \Illuminate\Database\Query\Builder|\Track whereProfileId($value) 
 */
class Track extends \BaseModel
{

	protected $fillable = ['title', 'desc', 'profile_id'];

    use PresentableTrait;
    protected $presenter = 'RT\Tracks\TrackPresenter';

	public function profile()
    {
        return $this->belongsTo('RT\Profiles\Profile');
    }

    public function courses(){
			return $this->belongsToMany('RT\Courses\Course');
    }

    public function recommendedCourses(){
        return $this->belongsToMany('RT\Courses\Course', 'track_recommended_course');
    }

    public function blocks(){
		return $this->morphMany('RT\Blocks\Block', 'blockable');
	}

}