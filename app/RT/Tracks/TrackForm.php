<?php namespace RT\Tracks;

use Laracasts\Validation\FormValidator;
class TrackForm extends FormValidator{

    /*
     * Validation form for the track form
    */
    protected $rules = [
        'title' => 'required'
    ];


}
