<?php namespace RT\Tracks\Repository;

use Acme\Repository\DbBaseRepository;


class DbTrackRepository extends DbBaseRepository implements TrackRepositoryInterface
{
    protected $modelClassName = "RT\Tracks\Track";

}