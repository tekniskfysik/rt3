<?php

use Illuminate\Support\Collection;
use Illuminate\View\View;

class NavigationViewComposer
{

    public function compose(View $view)
    {

        $menu = new Collection;

        $profilesSub = new Collection;
        $profilesSub->push((object)['title' => 'Alla', 'link' => URL::route('profiles.index')]);
        $profilesSub->push((object)['type' => 'divider']);
        foreach (RT\Profiles\Profile::all() as $profile) {
            $profilesSub->push((object)['title' => $profile->title, 'link' => URL::route('profiles.show', $profile->id)]);
        }
        $menu->push((object)['title' => 'Profiler', 'menu' => $profilesSub, 'type' => 'menu']);
        $menu->push((object)['title' => 'Kategorier', 'link' => URL::route('categories.index')]);


        $courses = new Collection;
        $courses->push((object)['title' => 'Kurser', 'link' => URL::route('courses.index')]);
        $courses->push((object)['title' => 'Kurstillfällen', 'link' => URL::route('courseoccasions.index')]);
        //$courses->push((object)['title' => 'Kurssök', 'link' => 'courseApp']);
        $menu->push((object)['title' => 'Kurser', 'menu' => $courses, 'type' => 'menu']);


        $view->menu = $menu;
    }
}

