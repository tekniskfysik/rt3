@extends('layouts.default')
@section('title', 'Logga in')

@section('content')

	<div class="row">
		<div class="col-md-6">
		
			{{ Form::open(array('url' => 'login')) }}

				<div class="form-group">
					{{ $errors->first('email') }}
					{{ $errors->first('password') }}
				</div>

				<div class="form-group">
					{{ Form::label('email', 'Email/Alias') }}
					{{ Form::text('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'awesome@awesome.com']) }}
				</div>

				<div class="form-group">
					{{ Form::label('password', 'Lösenord') }}
					{{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'my secret password']) }}
				</div>

				<div class="form-group">
					<div class="g-recaptcha" data-sitekey="{{ $_ENV['RE_CAP_SITE'] }}"></div>
				</div>

				<div class="form-group">
					{{ Form::submit('Logga in', ['class' => 'btn btn-primary']) }}
				</div>

			{{ Form::close() }}

		</div>
	</div>

<a class="btn btn-primary" href="{{url('password/remind')}}">Glömt lösenordet?</a>
@stop
