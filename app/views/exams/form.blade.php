<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('desc', 'Beskrivning') }}
	{{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('desc', 'Poäng (hp)') }}
    {{ Form::text('ects', null, array('class' => 'form-control')) }}
</div>


@include('categories.partials.categories-form', ['categories' => $exam->categories])