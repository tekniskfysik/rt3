@extends('layouts.default')
@section('title', 'Uppdatera -' . $exam->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($exam, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('exams.update', $exam->id)))}}
	@include('exams.form')
	{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}

<hr>

<h2>Blockscheman</h2>
@foreach($exam->blocks as $block)
<li>{{$block->link}}</li>
@endforeach

{{ Form::open(array('url' => 'blocks')) }}
{{ Form::hidden('blockable_type', 'RT\Exams\Exam') }}
{{ Form::hidden('blockable_id', $exam->id) }}
<label for="select" class="col-lg-6 control-label"></label>
<div class="col-md-2">
    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Titel (år)']) }}
</div>

<div class="col-md-2">
    {{ Form::submit('Skapa nytt blockschema', ['class' => 'btn btn-success pull-right'])}}
</div>

{{ Form::close() }}


@stop