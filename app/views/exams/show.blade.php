@extends('layouts.default')
@section('title', 'Examen - '.$exam->title)
@section('content')
<p>{{$exam->ects}} hp</p>
<p>{{$exam->desc}} </p>

<h3>Examenskrav</h3>
<ul>
    @foreach($exam->categories as $category)
    <li>
        {{$category->title}} - {{$category->pivot->ects}}
    </li>
    @endforeach
</ul>

<h3>Basblockscheman</h3>
<ul>
    @foreach($exam->blocks as $block)
    <li>
        {{$block->link}}
    </li>
    @endforeach
</ul>

@stop