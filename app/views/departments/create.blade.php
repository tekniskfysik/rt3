@extends('layouts.default')
@section('title', 'Lägg till institution')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'departments')) }}
	@include('departments.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop