<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('desc', 'Desc') }}
	{{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>