@extends('layouts.default')
@section('title', 'Uppdatera -' . $department->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($department, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('departments.update', $department->id)))}}
	@include('departments.form')
	{{ Form::submit('Uppdatera institution', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop