@extends('layouts.default')
@section('title', 'Institution - '.$department->title)
@section('content')
{{$department->desc}}
<h2>Kurser</h2>
<p>
@foreach($department->courses as $course)
	<li>
		<a href="{{ URL::to('courses/' . $course->id) }}">{{{$course->title}}}</a>
	</li>
@endforeach
</p>

@stop