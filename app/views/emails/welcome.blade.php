<!DOCTYPE html>
<html lang="sv">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>Välkommen till Röda Tråden</h2>
 
    <div>
      Ditt konto är skapat med följande uppgifter
    </div>
    <div>E-post: {{ $detail }}</div>
    <div>Användarnamn: {{ $name}} </div>
  </body>
</html>