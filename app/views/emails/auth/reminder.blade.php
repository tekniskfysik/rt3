<!DOCTYPE html>
<html lang="sv">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h1>Återställ lösenord</h1>
        <p>Detta är ett automatisk mail skickat från tekniskfysik.se.</p>

		<p>
            För att återställa ditt lösenord följer du bara länken här: {{ URL::to('password/reset', array($token)) }}.<br/>
            Länken fungerar bara i {{ Config::get('auth.reminder.expire', 60) }} minutes.
		</p>
        <p>
            Med vänliga hälsningar
            Programledningen för Teknisk Fysik, Umeå universitet
        </p>
	</body>
</html>
