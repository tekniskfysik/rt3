@extends('layouts.default')
@section('title', 'Skapa')
@section('content')

{{ Form::open( array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'POST', 'route' => array('blocks.store')))}}
<div class="form-group">
	<div class="col-lg-12">
		{{ Form::formGroup(Form::text('title', null, array('class' => 'form-control')), 'Namn på blockschema') }}
	</div>
	<div class="col-lg-12">
    		{{ Form::formGroup(Form::textarea('desc', null, array('class' => 'form-control')), 'Beskrivning') }}
	</div>
	<div class="col-lg-12">
		{{ Form::formGroup(Form::selectYear('start_year',date("Y")-7, date("Y")+2, date("Y"), array('class' => 'form-control')), 'Startår') }}
	</div>
	<div class="col-lg-12">
		{{ Form::hidden('private', 0) }}
		{{ Form::formGroup(Form::checkbox('private'), 'Privat') }}
	</div>
	<div class="col-lg-12">
		{{ Form::formGroup(Form::checkbox('block_id', 'RT\Tracks\Track', true), 'Importera basblock')}}
	</div>
</div>
@if(Auth::user()->isLedning())
		{{ Form::formGroup(Form::select('blockable_id', [0 => "Personligt"] + RT\Tracks\Track::lists('title','id'), null, array('class' => 'form-control')), 'Profil') }}
@else
	{{ Form::hidden('blockable_id', 0) }}
@endif
	{{ Form::submit('Skapa', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop
