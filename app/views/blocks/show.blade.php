@extends('layouts.default')
@section('title', $block->title)

@section('sidebar')

@stop

@section('content')

    @foreach($years as $key=>$year)
    <div class="row">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header">
                    {{$key}} - {{$key + 1}}
                    <a class="pull-right" data-toggle="collapse" data-parent=".block" href="#collapse{{$key}}"><i class="glyphicon glyphicon-resize-vertical"></i></a>
                </div>
                <div class="collapse in" id="collapse{{$key}}">
                    <div class="block-year" style="height:{{$year['rows']+2}}px">
                        @foreach($year['courses'] as $occasion)
                            @include('blocks.partials.block-item')
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach


    <button data-toggle="modal" data-target="#createPrivateCourse" type="button" class="btn btn-success">Skapa ny egen kurs</button>
    @include('blocks.partials.add-occasion-select-occasion', ['occasions' => Auth::user()->privateCourses, 'route' => 'addPrivateCourseToBlock', 'block_id' => $block->id])


@include('privatecourses.modals.createPrivateCourse')



<hr>

<table class="table table-striped table-condensed" id="accordion">
    @foreach ($block->categorySummary()->all() as $category)
        <tr>
            <td>
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$category->id}}">{{$category->title}}, {{$category->sum}}</a>
            </td>
            <td width="40%"><div class="progress" style="margin: 0">
                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: {{$category->percent}}%">
                    <span>{{$category->percent}}</span>
                </div></div>
            </td>
        </tr>
        <tr id="collapse{{$category->id}}" class="collapse">
            <td colspan="2">
                {{ implode($category->courses, ', ' )}}
            </td>
        </tr>
    @endforeach
</table>

@stop
