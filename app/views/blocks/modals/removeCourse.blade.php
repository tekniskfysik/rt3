<div class="row">
    <div class="col-md-12">
			<p>Är du säker på att du vill ta bort {{{ $title }}} från blockschemat?</p>
    </div>
	<div class="col-md-3">	
		{{ Form::open(array('route' => 'removeCourseOccasionFromBlock')) }}
		{{ Form::hidden('occasion_id', $occasion_id) }}
		{{ Form::hidden('block_id', $block_id) }}

		<button type="submit" class="btn btn-primary">
			Ta bort
		</button>

		{{ Form::close() }}
	</div>
	<div class="col-md-3">
		<button type="button" class="btn btn-primary" data-dismiss="modal">Avbryt</button>
	</div>
</div>

