@extends('modals.master')
@section('modal-id', 'createBlock')
@section('modal-title', "Skapa block")
@section('modal-body')
    {{ Form::open(array('url' => 'blocks')) }}
    @include('blocks.edit-form')
    {{ Form::hidden('blockable_type', 'Acme\Users\User') }}
    {{ Form::hidden('blockable_id', Auth::id()) }}
    {{ Form::submit('Skapa', ['class' => 'btn btn-success'])}}
    {{ Form::close() }}
@stop




