@extends('layouts.default')
@section('title', $block->title)
@section('content')

@if($block->private)
	<h2>Privat<h2>
@endif

<p>{{$block->desc}}</p>

<div id="studyBlock"></div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

@if(count($courses) || count($privatecourses))
	@include('courses.partials.course-summary', ['courses' => $courses, 'privatecourses' => $privatecourses])
@endif	

@stop
