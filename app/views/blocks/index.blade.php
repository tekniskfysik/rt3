@extends('layouts.default')
@section('title', 'Blockscheman')
@section('content')

	{{ HTML::table(array('link'), null, $blocks, 'blocks', true, true, null, null, null) }}

	<button data-toggle="modal" data-target="#createBlock" type="button" class="btn btn-success">Nytt schema</button>

    @include('blocks.modals.createBlock')
	
@stop