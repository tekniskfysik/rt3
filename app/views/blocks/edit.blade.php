@extends('layouts.default')
@section('title', 'Uppdatera -' . $block->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($block, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('blocks.update', $block->id)))}}
	{{ Form::formGroup(Form::text('title',null, array('class' => 'form-control')), 'Titel') }}
	{{ Form::formGroup(Form::textarea('desc', null, array('class' => 'form-control')), 'Beskrivning') }}
	{{ Form::formGroup(Form::selectYear('start_year',date("Y")-7, date("Y")+2, date("Y"), array('class' => 'form-control')), 'Startår') }}

	@if($block->blockable_type == 'RT\Tracks\Track')
		{{ Form::formGroup(Form::select('blockable_id', RT\Tracks\Track::lists('title','id'), null, array('class' => 'form-control')), 'Profil') }}
	@endif

	{{ Form::hidden('private', 0) }}
	{{ Form::formGroup(Form::checkbox('private'), 'Privat') }}

	{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop
