<fieldset>
    <div class="form-group">
        <label for="select" class="col-lg-2 control-label">Titel</label>
        <div class="col-lg-10">
            <input type="text" name="title" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="select" class="col-lg-2 control-label">Importera basblock</label>

        <div class="col-lg-10">
            {{Form::select('block_id', ["" => "Inget"] + RT\Blocks\Block::whereBlockableType('RT\Exams\exam')->lists('title', 'id'), null, ['class' => 'form-control'])}}
        </div>
    </div>
</fieldset>
