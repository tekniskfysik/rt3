{{ Form::open(array('class' => 'form-horizontal', 'route' => $route)) }}
    {{ Form::hidden('occasion_id', $occasion_id) }}
    {{ Form::hidden('block_id', $block_id) }}
    {{ Form::hidden('remove_occasion_id', isset($remove_occasion_id)? $remove_occasion_id : -1) }}
    {{ Form::submit($title, ['class' => '', 'style' => 'background:none!important; border:none; padding:0!important; ' ]) }}
{{ Form::close() }}