{{ Form::open(array('route' => $route)) }}
{{ Form::hidden('occasion_id', $privatecourse_id) }}
{{ Form::hidden('block_id', $block->id) }}

<button type="submit" class="btn btn-primary" data-confirm="Delete this item?">
    Ta bort
</button>

{{ Form::close() }}
