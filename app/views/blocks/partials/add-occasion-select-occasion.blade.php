<div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
        {{isset($title) ? $title : "Lägg till kurs"}}
        <span class="caret"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>

    <ul class="dropdown-menu" role="menu">
        @foreach($occasions as $occasion)
        <li> <a style="cursor:crosshair;" href="">@include('blocks.partials.add-occasion-form', ['occasion_id' => $occasion->id, 'title' => $occasion->title]) </a></li>
        @endforeach
    </ul>
</div>