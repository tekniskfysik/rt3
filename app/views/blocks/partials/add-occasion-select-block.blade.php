<div class="btn-group">
    <button type="button" class="btn btn-link my-tooltip my-dropdown" style="padding: 0" data-original-title="Lägg till i schema" data-toggle="dropdown">
        <span class="glyphicon glyphicon-plus"></span>
        <span class="sr-only">Toggle Dropdown</span>
    </button>

    <ul class="dropdown-menu" role="menu">
        @foreach($blocks as $block)
        <li> <a style="cursor:crosshair;" href="">@include('blocks.partials.add-occasion-form', ['block_id' => $block->id, 'title' => $block->title]) </a></li>
        @endforeach
    </ul>
</div>


