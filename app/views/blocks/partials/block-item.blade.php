<div class="block-item"
     style="border-color: {{$occasion['color']}}; left:{{$occasion['start']*2.5+0.2}}%; width:{{$occasion['length']*2.5-0.4}}%; top:{{$occasion['row']+1}}px; height: {{$occasion['speed']-2}}px">
    <div class="block-content">
        {{$occasion['link']}},
        {{$occasion['ects']}} hp
        @include('blocks.partials.remove-occasion-form', ['occasion_id' => $occasion['id'], 'route' => $occasion['removeRoute']])
    </div>
</div>