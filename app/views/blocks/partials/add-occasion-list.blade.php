<table class="table table-hover table-condensed table-striped" >

    <tbody>
        @foreach($courseoccasions->sortBy('title') as $occasion)
        <tr>
            <td>
                {{$occasion->link}}
            </td>
            <td>
                {{ Form::open(array('class' => 'form-horizontal', 'route' => 'addCourseOccasionToBlock', 'style' => 'padding:0px; margin:0')) }}
                {{ Form::hidden('occasion_id', $occasion->id) }}
                {{ Form::hidden('block_id', $block_id) }}
                {{ Form::hidden('remove_occasion_id', isset($remove_occasion_id)? $remove_occasion_id : -1) }}
                <button class="btn btn-link" style="padding: 0px; margin:0" type="submit"><span class="glyphicon glyphicon-plus"></span></button>
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
