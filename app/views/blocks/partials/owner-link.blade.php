	Ägare:
	@if(get_class($block->blockable) == "User")
		<a href="{{URL::route('users.show', $block->blockable->id)}}">{{$block->blockable->username}}</a>
	@else
		<a href="{{URL::route('tracks.show', $block->blockable->id)}}">{{$block->blockable->title}}</a>
	@endif