<div class="col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="{{route($route.'.index')}}">{{$title}}</a></h3>
        </div>
        <div class="panel-body">
            @include('layouts.partials.recentList', ['instances' => $instances,
            'route' => $route])
        </div>
    </div>
</div>