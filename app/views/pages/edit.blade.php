@extends('layouts.default')
@section('title', 'Redigera sida')
@section('content')

{{ Form::model($page, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('pages.update', $page->id)))}}
@include('pages.form')
{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop
