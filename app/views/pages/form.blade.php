<fieldset>

    {{Form::formGroup(Form::text('title', null, array('class' => 'form-control')), 'Titel')}}
    {{Form::formGroup(Form::textarea('content', null, array('class' => 'form-control')), 'Innehåll')}}
    {{Form::formGroup(Form::text('slug', null, array('class' => 'form-control')), 'url-slug')}}

</fieldset>

