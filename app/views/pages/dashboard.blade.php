@extends('layouts.default')
@section('title', 'Dashboard')
@section('content')

@if(Auth::user()->isLedning())
    <h2>Senaste ändringar</h2>
    <div class="row">
        @include('pages.widget', ['title' => 'Rapporter', 'instances' => RT\Reports\Report::recentUpdated()->get(), 'route' => 'reports'])
        @include('pages.widget', ['title' => 'Fsr', 'instances' => Goals\Fsrs\Fsr::recentUpdated()->get(), 'route' => 'fsrs'])
    </div>
	<div class="row">
        @include('pages.widget', ['title' => 'Kurser', 'instances' => RT\Courses\Course::recentUpdated()->get(), 'route' => 'courses'])
        @include('pages.widget', ['title' => 'Kurstillfällen', 'instances' => RT\CourseOccasions\CourseOccasion::recentUpdated()->get(), 'route' => 'courseoccasions'])
	</div>
    <div class="row">
        @include('pages.widget', ['title' => 'Kategori', 'instances' => RT\Categories\Category::recentUpdated()->get(), 'route' => 'categories'])
        @include('pages.widget', ['title' => 'Block', 'instances' => RT\Blocks\Block::recentUpdated()->get(), 'route' => 'blocks'])
    </div>
    <div class="row">
        @include('pages.widget', ['title' => 'Profiler', 'instances' => RT\Profiles\Profile::recentUpdated()->get(), 'route' => 'profiles'])
        @include('pages.widget', ['title' => 'Spår', 'instances' => RT\Tracks\Track::recentUpdated()->get(), 'route' => 'tracks'])
    </div>

    <div class="row">
        @include('pages.widget', ['title' => 'Användare', 'instances' => Acme\Users\User::recentUpdated()->get(), 'route' => 'users'])
    </div>
@elseif(Auth::user()->isEditor() && count(Auth::user()->courses))
    <div class="row">
        @include('pages.widget', ['title' => 'Kurser', 'instances' => RT\Courses\Course::whereIn('id', Auth::user()->courses->lists('id'))->get(), 'route' => 'courses'])
        @include('pages.widget', ['title' => 'Kurstillfällen', 'instances' => RT\CourseOccasions\CourseOccasion::whereIn('course_id', Auth::user()->courses->lists('id'))->recentUpdated()->get(), 'route' => 'courseoccasions'])
    </div>
@endif
@stop