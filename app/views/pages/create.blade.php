@extends('layouts.default')
@section('title', 'Lägg till sida')
@section('content')

    {{ Form::open(['url' => 'pages', 'files' => true, 'method' => 'post']) }}
    @include('pages.form')
    {{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
    {{ Form::close() }}

@stop

