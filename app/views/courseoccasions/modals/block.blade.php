<div class="row">
    <div class="col-md-6">
        @include('courses.partials.info', ['course' => $course = $courseoccasion->course])
        {{ $courseoccasion->course->present()->categoriesLabels() }}
        <br>
        <br>
	<div class="col-md-4">
        @include('blocks.partials.add-occasion-select-occasion', [
            'occasions' =>  $courseoccasion->course->occasions,
            'route' => 'moveCourseOccasionInBlock',
            'remove_occasion_id' => $courseoccasion->id,
            'title' => 'Flytta',
            'block_id' => $block_id])
	</div>

	<div class="col-md-4">
        @include('blocks.partials.remove-occasion-form', [
            'route' => 'removeCourseOccasionFromBlock',
            'occasion_id' => $courseoccasion->id,
            'block' => RT\Blocks\Block::findOrFail($block_id)])
	</div>
    </div>
    <div class="col-md-6">
        @include('courseoccasions.partials.courseoccasion')
    </div>
</div>






