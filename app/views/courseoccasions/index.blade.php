@extends('layouts.default')
@section('title', 'Kurstillfällen')
@section('content')
<div class="page-header">
    <h1>Kurser <small>sök bland alla kurstillfällen</small></h1>
</div>
{{ HTML::dataTable(array('link', 'year', 'period'), array('Kurs', 'Poäng', 'Start'), $courseoccasions, 'courseoccasions') }}


@stop	