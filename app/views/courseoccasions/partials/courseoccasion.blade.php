<?php
		$percent = $course->ects/($courseoccasion->weeks*1.5)*100;
?>

<p>{{$courseoccasion->link}}</p>
<p>Start: {{{$courseoccasion->timeperiod->title}}}	</p>
<p>Veckor: {{{$courseoccasion->weeks}}}	</p>
<p>Studietakt: {{{ $percent }}}% </p>
<p>Kontaktperson: {{{$courseoccasion->contact_name}}}
    - {{{$courseoccasion->contact_email}}}
</p>

<p>
    @if($courseoccasion->official)
    <span class="label label-success">Godkänd</span>
    @else
    <span class="label label-danger">Ej godkänd</span>
    @endif
</p>









