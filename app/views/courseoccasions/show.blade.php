@extends('layouts.default')
@section('title', $courseoccasion->course->title ." (". $courseoccasion->year . ")" )

@section('sidebar')
    @include('courses.partials.info', ['course' => $courseoccasion->course])
    <hr>
    @include('courseoccasions.partials.courseoccasion', ['course' => $courseoccasion->course])
    @if(Auth::check())
        @include('blocks.partials.add-occasion-select-block', ['blocks' => Auth::user()->blocks, 'occasion_id' => $courseoccasion->id, 'route' => 'addCourseOccasionToBlock'])
    @endif

    <hr>
    <ul class="list-group">
        @foreach($courseoccasion->course->occasions as $courseoccasion)
        <li>{{$courseoccasion->link}}</li>
        @endforeach
    </ul>
@stop

@section('sidebar-master')
    @include('courses.partials.course', ['course' => $courseoccasion->course])
@stop

