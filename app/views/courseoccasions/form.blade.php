<div class="form-group">
	{{ Form::label('course_id', 'Kurs') }}
	{{ Form::select('course_id', [''=> 'Välj kurs'] + $courseRepo->lists('title', 'id'), null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('year', 'Läsår') }}
	{{ Form::select('year', [''=> 'Välj tidsperiod'] + \RT\AcademicYears\AcademicYear::lists('title', 'year'), null, array('class' => 'form-control')) }}

</div>

<div class="form-group">
	{{ Form::label('start', 'Start') }}
	{{ Form::select('start', [''=> 'Välj tidsperiod'] + \RT\TimePeriods\TimePeriod::all()->sortBy('week_offset')->lists('title', 'week_offset'), null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('weeks', 'Veckor') }}
	{{ Form::selectRange('weeks', 1, 20, null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('contact_name', 'Kontaktperson') }}
	{{ Form::text('contact_name', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('contact_email', 'Email') }}
	{{ Form::text('contact_email', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('official', 'Official') }}
	{{ Form::hidden('official', 0) }}
	{{ Form::checkbox('official') }}
</div>
