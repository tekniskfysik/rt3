@extends('layouts.default')
@section('title', 'Uppdatera -' . $courseoccasion->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($courseoccasion, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('courseoccasions.update', $courseoccasion->id)))}}
	@include('courseoccasions.form')
	{{ Form::submit('Uppdatera kurs', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop



