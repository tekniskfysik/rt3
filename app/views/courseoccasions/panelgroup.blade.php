<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          2014 Läsperiod 1
          <a href="" class="btn btn-xs btn-primary pull-right" data-toggle="tooltip" title="Lägg till kurs i blockschema" data-placement="right"><i class="glyphicon glyphicon-plus"></i></a>
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        <b>Start: </b>2<br>
        <b>Veckor:</b> 10<br>
        <a href=""> Michael Bradley </a><br>
        <a href="">Kurshemsida</a><br>
        <a href="">Kursplan</a><br>
        <button class='btn btn-default'>Add</button>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          2015 Läsperiod 1
          <a href="" class="btn btn-xs btn-primary pull-right" data-toggle="tooltip" title="Lägg till kurs i blockschema" data-placement="right"><i class="glyphicon glyphicon-plus"></i></a>
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
          Alla kurstillfällen
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse in">
      <div class="panel-body">
          {{ HTML::table(array('year', 'start', 'addButton'), null, $courseoccasions, 'courseoccasions', allowed("courses.edit"), allowed("courses.destroy"), false) }}

      </div>
    </div>
  </div>
</div>