@extends('layouts.default')
@section('title', 'Lägg till kurstillfälle')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'courseoccasions')) }}
	@include('courseoccasions.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop