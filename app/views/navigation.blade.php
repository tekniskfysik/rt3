<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-main">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ URL::to('/') }}" class="navbar-brand">RT<sup>3</sup></a>
        </div>

        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
				@if(Agent::isMobile())
					 @if(Auth::check())
								<li>
										  <a href="{{ URL::route('userprofile_path', Auth::user()->username) }}">
										  <img src="{{$currentUser->present()->gravatar(30)}}" alt="{{ $currentUser->username }}" class="img-circle" style="margin-right: 10px;">
										  {{$currentUser->username}}
										  </a>
								</li>
								<li>
									<a href={{ URL::route('usercourses_path', Auth::user()->username) }}>Egna kurser</a>
								</li>
								<li>
									<a href={{ URL::route('userblocks_path', Auth::user()->username) }}>Blockschema</a>
								</li>
								<li class="nav-divider" style="background-color: #990000;"></li>
					 @else
								<li><a href={{ URL::route('login') }}>Logga in</a></li>
								<li><a href={{ URL::route('register') }}>Registrera</a></li>
								<li class="nav-divider" style="background-color: #990000;"></li>
					 @endif

					 <li>
								<a href={{ URL::route('profiles.index') }}>Profiler</a>
					 </li>
					 <li>
								<a href={{ URL::route('categories.index') }}>Kategorier</a>
					 </li>
					 <li>
								<a href={{ URL::route('courses.index') }}>Kurser</a>
					 </li>

					 @if(Auth::check())
								<li class="nav-divider" style="background-color: #990000;"></li>
								<li><a href={{ URL::route('logout') }}>Logga ut</a></li>
					 @endif

				@else

						  @foreach($menu->all() as $item)
									 @if(isset($item->link) && isset($item->disabled))
												<li class="disabled">
														  <a href="{{ $item->link }}">{{ $item->title }}</a>
												</li>
									 @elseif(isset($item->link))
												<li>
														  <a href="{{ $item->link }}">{{ $item->title }}</a>
												</li>
									 @elseif(isset($item->menu))
												<li class="dropdown">
														  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
														  {{ $item->title }} <span class="caret"></span>
														  </a>
														  <ul class="dropdown-menu">
														  @foreach($item->menu->all() as $subItem)
																	 @if(isset($subItem->link) && isset($subItem->disabled))
																				<li class="disabled">
																						  <a href="{{ $subItem->link }}">{{ $subItem->title }}</a>
																				</li>
																	 @elseif(isset($subItem->link))
																				<li>
																						  <a href="{{ $subItem->link }}">{{ $subItem->title }}</a>
																				</li>
																	 @elseif($subItem->type === 'heading')
																				<li class="dropdown-header">{{ $subItem->title }}</li>
																	 @elseif($subItem->type === 'divider')
																				<li class="divider"></li>
																	 @endif
														  @endforeach
														  </ul>
												</li>
									 @endif
						  @endforeach
				@endif
            </ul>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{$currentUser->present()->gravatar(30)}}" alt="{{ $currentUser->username }}" class="img-circle" style="margin-right: 10px;">
                            {{$currentUser->username}} <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href={{ URL::route('userprofile_path', Auth::user()->username) }}>Profil</a></li>
                            <li><a href={{ URL::route('usercourses_path', Auth::user()->username) }}>Egna kurser</a>
                            </li>
                            <li><a href={{ URL::route('userblocks_path', Auth::user()->username) }}>Blockschema</a></li>
                            <li class="divider"></li>
                            @foreach($currentUser->blocks as $block)
                            <li><a href={{ URL::route('blocks.show', $block->id) }}>{{ $block->title }}</a></li>
                            @endforeach
                            <li class="divider"></li>
                            <li><a href={{ URL::route('users.edit', Auth::id()) }}>Inställningar</a></li>
                            <li><a href={{ URL::route('logout') }}>Logga ut</a></li>
                        </ul>
                    </li>
                        @if(Auth::user()->isEditor())

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" style="margin-top:3px; font-size: 1.5em"
                               data-toggle="dropdown">
                                <span class="glyphicon glyphicon-cog"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ URL::route('dashboard') }}">Dashboard</a></li>
                                <li><a href="{{ URL::route('tutorial') }}">Tutorial</a></li>
                                <li class="divider"></li>
                                @if(Auth::user()->isLedning())
                                    <li><a href="{{ URL::route('profiles.admin') }}">Profiler</a></li>
                                    <li><a href="{{ URL::route('tracks.index') }}">Spår</a></li>
                                    <li><a href="{{ URL::route('blocks.index') }}">Blocks</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::route('categories.admin') }}">Kategorier</a></li>
                                    <li><a href="{{ URL::route('exams.index') }}">Exam</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::route('departments.index') }}">Institutioner</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::route('timeperiods.index') }}">Läsperioder</a></li>
                                    <li><a href="{{ URL::route('academicyears.index') }}">Läsår</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::route('pages.index') }}">Sidor</a></li>
                                    <li><a href="{{ URL::route('groups.index') }}">Grupper</a></li>
                                    <li><a href="{{ URL::route('reports.index') }}">Rapporter</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::route('goals.index') }}">Goals</a></li>
                                @endif
                                @if(Auth::user()->isEditor())
                                    <li><a href="{{ URL::route('fsrs.index') }}">Fsr</a></li>

                                @endif
                            </ul>
                        </li>
                        @endif
                    @else

                    <li><a href={{ URL::route('login') }}>Logga in</a></li>
                    <li><a href={{ URL::route('register') }}>Registrera</a></li>

                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </div>
</div>

