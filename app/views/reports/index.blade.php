@extends('layouts.default')
@section('title', 'Rapporter')
@section('content')
{{ HTML::table(array('email', 'category', 'message', 'status', 'updated_at'), array('Email', 'Kategori', 'Message', 'Status', 'Ändrad'), $reports, 'reports') }}
@stop