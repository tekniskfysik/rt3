<fieldset>
            <div class="form-group">
                {{Form::label('Email')}}
                {{ Form::text('email', null, ['class'=>'form-control', 'placeholder' => 'Ange mailadress om du vill ha svar']) }}
            </div>
            <div class="form-group">
                {{Form::label('Meddelande')}}
                {{ Form::textarea('message', null, ['class'=>'form-control']) }}
            </div>
            <div class="form-group">
                {{Form::label('Kategori')}}
                {{ Form::select('category', ['' => '', 'Kurser' => 'Kurser', 'Examen' => 'Examen', 'Bugg på rt' => 'Bugg på rt', 'Datasal'=>'Datasal', 'Förslag'=>'Förslag',
                'Övrigt'=>'Övrigt'], null, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                {{Form::label('Länk')}}
                {{ Form::text('url_to_page', url(Request::path()), ['class'=>'form-control']) }}
            </div>

</fieldset>