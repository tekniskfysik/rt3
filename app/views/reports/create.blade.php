@extends('layouts.default')
@section('title', 'Skicka rapport')
@section('content')
{{ HTML::ul($errors->all()) }}
{{ Form::open(array('url' => 'reports')) }}
@include('reports.form')
{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}
@stop