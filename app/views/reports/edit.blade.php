@extends('layouts.default')
@section('title', 'Hantera rapport')
@section('content')
{{ HTML::ul($errors->all()) }}
{{ Form::model($report, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('reports.update', $report->id)))}}

{{ Form::label('Message')}}
{{ Form::textarea('message', null, ['class' => 'form-control', 'disabled'=>''])}}
{{ Form::hidden('message', null)}}


@if($report->email)
{{ Form::label('Skickat av')}}
{{ Form::text('email', null, ['class' => 'form-control', 'disabled'=>''])}}
@endif


{{ Form::label('Status')}}
{{ Form::select('status', ['Ej hanterat'=>'Ej hanterat', 'Hanterat'=>'Hanterat'], null, ['class'=>'form-control']) }}
{{ Form::label('Kommentar')}}
{{ Form::textarea('comment', null, ['class' => 'form-control'])}}
{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop
