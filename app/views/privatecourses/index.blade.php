@extends('layouts.default')
@section('title', 'Privata Kurser')
@section('content')
{{ HTML::table(array('title', 'ects', 'year', 'start', 'weeks', 'user_id'), array('Titel', 'Poäng', 'År', 'LP', 'Längd', 'User'), $privatecourses, 'privatecourses') }}
<a class="btn btn-small btn-success" href="{{ URL::to('privatecourses/create') }}">Lägg till ny</a>
@stop