@extends('layouts.default')

@section('content')
@include('privatecourses.partials.privatecourse')
@include('blocks.partials.add-occasion-select-block', ['blocks' => Auth::user()->blocks, 'occasion_id' => $privatecourse->id, 'route' => 'addPrivateCourseToBlock'])
@stop