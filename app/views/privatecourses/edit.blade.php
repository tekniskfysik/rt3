@extends('layouts.default')
@section('title', 'Uppdatera -' . $privatecourse->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($privatecourse, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('privatecourses.update', $privatecourse->id)))}}
	@include('privatecourses.form')
	@include('categories.partials.categories-form', ['categories' => $privatecourse->categories])
	{{ Form::submit('Uppdatera kurs', array('class' => 'pull-right btn btn-primary')) }}
	{{ Form::close() }}
@stop
