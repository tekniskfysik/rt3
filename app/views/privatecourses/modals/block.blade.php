<div class="row">
		<div class="col-md-6">
		<h3>Egen kurs</h3>
		
		{{ $privatecourse->present()->categoriesLabels() }}
		<br> <br>
		@include('blocks.partials.remove-privatecourse-form', [
            'route' => 'removePrivateCourseFromBlock',
            'privatecourse_id' => $privatecourse->id,
            'block' => RT\Blocks\Block::findOrFail($block_id)])
		</div>
    <div class="col-md-6">
    		
        @include('privatecourses.partials.privatecourse')
    </div>
</div>

