
@extends('modals.master')
@section('modal-id', 'createPrivateCourse')
@section('modal-title', "Skapa egen kurs")
@section('modal-body')

{{ Form::open(array('url' => 'privatecourses', 'class' => 'form-horizontal')) }}
@include('privateCourses.form')
{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}
@stop




