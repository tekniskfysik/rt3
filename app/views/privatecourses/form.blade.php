<fieldset>
	{{Form::formGroup(Form::text('title', null, array('class' => 'form-control')), 'Kurs')}}
	{{Form::formGroup(Form::text('ects', null, array('class' => 'form-control')), 'Poäng')}}
	{{Form::formGroup(Form::selectRange('year', \Carbon\Carbon::now()->subYears(5)->year, \Carbon\Carbon::now()->addYears(5)->year,  \Carbon\Carbon::now()->year, array('class' => 'form-control')), 'År')}}
	{{Form::formGroup(Form::select('start', [''=> 'Välj tidsperiod'] + \RT\TimePeriods\TimePeriod::lists('title', 'week_offset'), null, array('class' => 'form-control')) , 'Start')}}
	{{Form::formGroup(Form::selectRange('weeks', 1, 20, null, array('class' => 'form-control')) , 'Veckor')}}

	{{Form::formGroup(Form::select('blocks[]', RT\Blocks\Block::my()->lists('title', 'id'), isset($privatecourse) ? $privatecourse->blocks->modelKeys() : null, array('multiple' => true, 'id'=>'e1', 'style'=>'width:100%'))
		, 'Blocks')}}
</fieldset>
