@extends('layouts.default')
@section('title', 'Lägg till användare')
@section('content')
<div class="col-md-6">
	@include('layouts.partials.errors')
	{{ Form::open(array('url' => 'register')) }}
	@include('users.form')
	{{ Form::submit('Registrera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
</div>
@stop