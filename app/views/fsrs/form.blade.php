<div class="form-group">
    {{ Form::label('course_id', 'Kurs') }}
    {{ Form::select('course_id', [''=> 'Välj kurs'] + $courseRepo->lists('title', 'id'), null, array('class' => 'form-control', isset($fsr) ? 'disabled' : '' => '')) }}

    {{ Form::hidden( isset($fsr) ? 'course_id':'', isset($fsr) ? $fsr->course_id :'') }}
</div>

<div class="form-group">
	{{ Form::label('title', 'FSR') }}
	{{ Form::textarea('title', null, array('class' => 'form-control', 'rows' => 3)) }}
</div>

<div class="form-group">
    {{ Form::label('hide', 'hide') }}
    {{ Form::hidden('hide', false)}}
    {{ Form::checkbox('hide') }}
</div>


@foreach (Goals\Goals\Goal::$goalTypes as $key => $type)
    <div class="form-group">
        <?php
        $list = Goals\Goals\Goal::whereType($key)->whereHidden(0)->get();
        $a = array();
        foreach( $list as $key1 => $val1){
            if(count($val1->childs) == 0){
                $a[$val1->id] = '-- ' . $val1->code . ": " . $val1->title ;
            }else{
                $a[$val1->id] = $val1->code . ": " . $val1->title;
            }

        }?>

        {{ Form::label('title', Goals\Goals\Goal::$goalTypes[$key]) }}
        {{ Form::select('goals[]', $a, isset($fsr) ? $fsr->goals->modelKeys() : null, array('multiple' => true, 'class' => 'form-control', 'size'=> count($a))) }}
    </div>

@endforeach


<div class="form-group">
    {{ Form::label('desc', 'Beskrivning / Kommentar') }}
    {{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>


