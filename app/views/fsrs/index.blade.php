@extends('layouts.default')
@section('title', isset($title)?$title:'Rapyd')

@section('styles')
{{ Rapyd::styles() }}
@stop

@section('content')

@if(isset($filter) && isset($showFilter))
<div class="well hidden-print">
    {{ $filter }}
</div>
@endif


<div role="tabpanel">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#national" aria-controls="national" role="tab" data-toggle="tab">Nationella mål</a></li>
        <li role="presentation"><a href="#cdio" aria-controls="cdio" role="tab" data-toggle="tab">CDIO</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="national">{{$matrix[0]}}</div>
        <div role="tabpanel" class="tab-pane" id="cdio">{{$matrix[1]}}</div>
    </div>
</div>
{{ $set->links() }}

<fieldset>
    <legend>Exportera kursmålsmatris</legend>

        {{ Form::open(array('url' => 'test')) }}
        <div class="form-group">
            <div class="row">
                <div class="col-md-2">
                    {{Form::label('Väl kurser') }}
                </div>
                <div class="col-md-10">
                    {{ Form::select('courses[]', $set->data->lists('title', 'id') + RT\Courses\Course::lists('title', 'id'),$set->data->lists('id'), array('multiple' => true,
                    'class' => 'form-control selectpicker show-tick',
                    'data-live-search'=>"true",
                    'data-selected-text-format'=>"count>5")) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-md-2">
                    {{Form::label('Alternativ') }}
                </div>
                <div class="col-md-10">
                    {{ Form::select('settings[]', ['expanderad', 'komprimerad', 'dolda'],null, array('multiple' => true,
                    'class' => 'form-control selectpicker show-tick',
                    'data-live-search'=>"true",
                    'data-selected-text-format'=>"count>5",
                    'title' => 'Inget valt')) }}
                </div>
            </div>
        </div>
    </fieldset>


{{ Form::submit('Exportera', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

<hr>
{{link_to_route('fsrs.overview', 'Alla FSR')}}


@stop


@section('scripts')
{{ Rapyd::scripts() }}

<script>
    $('td').mouseover(function () {
        $(this).siblings().css('background-color', '#EAD575');
        var ind = $(this).index();
        $('td:nth-child(' + (ind + 1) + ')').css('background-color', '#EAD575');
    });
    $('td').mouseleave(function () {
        $(this).siblings().css('background-color', '');
        var ind = $(this).index();
        $('td:nth-child(' + (ind + 1) + ')').css('background-color', '');
    });
</script>
@stop


