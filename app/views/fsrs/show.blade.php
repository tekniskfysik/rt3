@extends('layouts.default')
@section('title', $fsr->title)

@section('content')
<h1><small>{{$fsr->course->title}}</small></h1>
<p>{{$fsr->desc}} </p>

<h2>Uppfyllda mål</h2>
@foreach (Goals\Goals\Goal::$goalTypes as $key => $type)
    <? $list = $fsr->goals()->whereType($key)->whereHidden(0)->get();?>
    @if(count($list))
        <h4>{{$type}}</h4>
        <ul>
            @foreach( $list as $key1 => $val1)
                <li>{{$val1->title}}</li>
           @endforeach
        </ul>
    @endif
@endforeach

<p>
    @if($fsr->approved_by)
        <span class="label label-success">Godkänd</span> av {{$fsr->approvedUser->username}}
    @elseif($fsr->requested_by)
        <span class="label label-danger">Ej godkänd</span>  ({{$fsr->requestedUser->username}})
    @endif
</p>

<p>Uppdaterad: {{$fsr->updated_at}}</p>

@if($fsr->parent)
    <p>Tillhör: {{ $fsr->parent->link }} (rev {{ $fsr->revision }})</p>
@endif

@foreach($fsr->childs as $child)
    <li>{{$child->link}} - {{$child->revision}}</li>
@endforeach


<div id="chart-div"></div>
Not imlemented
@stop