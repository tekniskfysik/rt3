@extends('layouts.default')
@section('title', 'Redigera fsr')
@section('content')
{{ HTML::ul($errors->all()) }}
{{ Form::model($fsr, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('fsrs.update', $fsr->id)))}}
@include('fsrs.form')
{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop
