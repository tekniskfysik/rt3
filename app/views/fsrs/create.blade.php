@extends('layouts.default')
@section('title', 'Lägg till fsr')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'fsrs')) }}
	@include('fsrs.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop