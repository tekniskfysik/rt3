@extends('layouts.default')
@section('title', $category->title)
@section('content')
{{$category->desc}}	

    <h2>Kurser</h2>
    @foreach($category->courses as $course)
        <li>
            <a href="{{ URL::to('courses/' . $course->id) }}">{{{$course->title}}} - {{$course->pivot->ects}}</a>
        </li>
    @endforeach
@stop