@extends('layouts.default')
@section('title', 'Lägg till kategori')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'categories')) }}
	@include('categories.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop