@extends('layouts.default')
@section('title', 'Uppdatera -' . $category->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($category, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('categories.update', $category->id)))}}
	@include('categories.form')
	{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop