<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('desc', 'Desc') }}
	{{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('abbr', 'Abbr') }}
    {{ Form::text('abbr', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('image', 'Image') }}
    {{ Form::text('image', null, array('class' => 'form-control')) }}
</div>

@if(isset($category))
@include('courses.partials.courses-form', ['courses' => $category->courses])
@endif