<fieldset>
    <legend>Kategorier</legend>
    <div class="categories-form">
        <div class="form-group category-group hidden">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-7">
                {{ Form::select("category[id][]", $categoryRepo->lists('title', 'id'), null, array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2">
                <div class="input-group">
					<input name="category[ects][]" type="text" class="form-control">
				<span class="input-group-addon">hp</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="btn btn-link remove-category-button"><span class="glyphicon glyphicon-remove"></span></div>
            </div>
        </div>
    </div>
    <button class="btn btn-default pull-right" id="add-category-button">Lägg till</button>
</fieldset>
