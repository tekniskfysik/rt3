<div class="row">
    <br>
    <div class="col-md-3 col-sm-3 text-center">
        <a class="story-title" href="{{ route('categories.show', $category->id) }}"><img alt="{{$category->title}}" src="{{$image}}" style="width:150px; height: 150px" class="img-circle"></a>
    </div>
    <div class="col-md-9 col-sm-9">
        <h3><a href="{{ route('categories.show', $category->id) }}" class="text-muted">{{$category->title}}</a></h3>
        <div class="row">
            <div class="col-xs-9">
                <p>{{substr($category->desc, 0, 500)}}...</p>
                <small style="font-family:courier,'new courier';" class="text-muted"><a href="{{ route('categories.show', $category->id) }}" class="text-muted">Läs mer</a></small>
                </h4></div>
            <div class="col-xs-3"></div>
        </div>
        <br><br>
    </div>
</div>
<hr>