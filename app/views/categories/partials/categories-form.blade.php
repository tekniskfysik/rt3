<fieldset>
    <legend>Kategorier</legend>
    <div class="categories-form">
        <div class="form-group category-group hidden">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-7">
                {{ Form::select("category[id][]", $categoryRepo->lists('title', 'id'), null, array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    {{ Form::text("category[ects][]", null, array('class' => 'form-control')) }}
                    <span class="input-group-addon">hp</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="btn btn-link remove-category-button"><span class="glyphicon glyphicon-remove"></span></div>
            </div>
        </div>


        @foreach($categories as $category)
        <div class="form-group category-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-7">
                {{ Form::select("category[id][]", $categoryRepo->lists('title', 'id'), $category->id,
                array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    {{ Form::text("category[ects][]", $category->pivot->ects, array('class' => 'form-control')) }}
                    <span class="input-group-addon">hp</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="btn btn-link remove-category-button"><span class="glyphicon glyphicon-remove"></span></div>
            </div>
        </div>
        @endforeach

    </div>

    <button class="btn btn-default pull-right" id="add-category-button">Lägg till</button>
</fieldset>