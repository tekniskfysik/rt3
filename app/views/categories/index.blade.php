@extends('layouts.default')
@section('title', 'Kategorier')
@section('content')
<p>Under din tid på programmet ska du ha läst kurser inom ett antal olika områden som översiktligt beskrivs nedan.
    Du ska minst det angivna poängantalet (hp = högskolepoäng, 1,5 hp motsvarar ca 1 veckas studier på helfart).
</p>
<div id="chart-div"></div>
Not implemented
<hr>

    @foreach($categories as $category)
        @include('categories.partials.preview', ['image'=> $category->image?:"http://dummyimage.com/400x400/0000ff/fff.png&text={$category->abbr}"])
    @endforeach
	
@stop