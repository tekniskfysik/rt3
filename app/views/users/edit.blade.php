@extends('layouts.default')
@section('title', 'Uppdatera -' . $user->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($user, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('users.update', $user->id)))}}
	@include('users.form')
	{{ Form::submit('Uppdatera användare', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop