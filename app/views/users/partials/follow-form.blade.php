@if(Auth::check())

    @if ( $user->isFollowedBy(Auth::user()))
            {{ Form::open(['method' => 'DELETE', 'route' => ['follow_path', $user->id]]) }}
                {{ Form::hidden('userIdToUnfollow', $user->id) }}
                {{ Form::submit('Unfollow ' . $user->username, ['class' => 'btn ptn-danger']) }}
            {{ Form::close() }}
    @else
        {{ Form::open(['route' => 'follows_path']) }}
            {{ Form::hidden('userIdToFollow', $user->id) }}
            {{ Form::submit('Follow ' . $user->username, ['class' => 'btn ptn-primary']) }}
        {{ Form::close() }}
    @endif

@endif