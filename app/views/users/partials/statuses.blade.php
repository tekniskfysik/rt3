@foreach($user->followers as $follower)
<div class="hide">@include('users.partials.avatar', ['size' => 50, 'user' => $follower])</div>
@endforeach

@unless($user->is(Auth::user()))
@include('users.partials.follow-form')
@endunless

@if( Auth::check() && $user->is(Auth::user()) )
@include('statuses.partials.publish-status-form')
@endif

@include('statuses.partials.statuses', ['statuses' => $user->statuses])