<div class="form-group">
	{{ Form::label('username', 'Användarnamn') }}
	{{ Form::text('username', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('email', 'Email') }}
	{{ Form::text('email', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('public', 'Publik') }}
    {{ Form::checkbox('public', 1, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('password', 'Lösenord') }}
	{{ Form::password('password', array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('password_confirmation', 'Bekräfta Lösenord') }}
	{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
</div>

<div class="form-group">
	<div class="g-recaptcha" data-sitekey="{{ $_ENV['RE_CAP_SITE'] }}"></div>
</div>
