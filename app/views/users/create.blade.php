@extends('layouts.master')
@section('title', 'Lägg till användare')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'users')) }}
	@include('users.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop