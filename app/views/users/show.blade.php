@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="media">
            <div class="pull-left">
                @include('users.partials.avatar', ['size' => 50])
            </div>

            <div class="media-body">
                <h1 class="media-heading">{{$user->username}}</h1>
                <h3>
                    <small>({{$user->email}})</small>
                </h3>
                {{ Auth::check() && Auth::user()->id == $user->id ? link_to_route('users.edit', 'Inställningar', $user->id) : "Kan inte redigera" }} <br>
            </div>
        </div>
    </div>

    <div class="col-md-4">



        @if($user->blocks->count())
            <h2>Blockscheman</h2>
            @foreach ($user->blocks as $block)
            <li>
                <a href="{{ URL::to('blocks/' . $block->id) }}">{{{$block->title}}}</a>
            </li>
            @endforeach
        @endif

        @if($user->blocks->count())
            <h2>Mina kurser</h2>
            @foreach ($courses = $user->privatecourses as $block)
            <li>
                <a href="{{ URL::to('privatecourses/' . $block->id) }}">{{{$block->title}}}</a>
            </li>
            @endforeach
        @endif
    </div>
    <div class="col-md-4">
        @if($user->likes->count())
        <h2>Favoriter</h2>
        @foreach ($user->likedCourses as $like)
        <li>{{$like->likable->link}}</li>
        @endforeach
        @endif
    </div>

</div>



@stop

