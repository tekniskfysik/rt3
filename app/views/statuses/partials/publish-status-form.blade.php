
@include('layouts.partials.errors')

<div class="status-post">
	{{ Form::open(['data-remote', 'data-remote-success-message' => 'Posted successfully', 'route' => 'statuses.store']) }}
	
		<div class="form-group">
			{{ Form::textarea('body', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => "Whats on your mind?"]) }}
		</div>
		
		<div class="form-group status-post-submit">
			{{ Form::submit('Post Status', ['class' => 'btn btn-default btn-xs', 'data-confirm' => 'Are you sure?']) }}
		</div>
	
	{{ Form::close() }}
</div>
@include('layouts.partials.flash')
