@extends('layouts.default')
@section('title', 'Ändra lösenord')
@section('content')

@if (Session::has('error'))
 {{ Session::get('error') }}
@endif


<form action="{{ action('RemindersController@postReset') }}" method="POST">
     <p>{{ Form::label('email', 'Email') }}
  {{ Form::text('email', null, array('class' => 'form-control')) }}</p>
 
  <p>{{ Form::label('password', 'Password') }}
  {{ Form::password('password', array('class' => 'form-control')) }}</p>
 
  <p>{{ Form::label('password_confirmation', 'Password confirm') }}
  {{ Form::password('password_confirmation', array('class' => 'form-control')) }}</p>
 
  {{ Form::hidden('token', $token) }}
 
  <p>{{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}</p>
</form>

@stop
