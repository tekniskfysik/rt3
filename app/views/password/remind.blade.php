@extends('layouts.default')
@section('title', 'Påminn mig om lösenordet')
@section('content')

<form action="{{ action('RemindersController@postRemind') }}" method="POST">
    {{Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email') )}}
    {{Form::label('')}}
    {{Form::submit("Påminn", ['class' => 'btn btn-primary pull-right']) }} 

</form>

@stop