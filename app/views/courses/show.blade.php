@extends('layouts.default')
@section('title', $course->title)

    @section('angular')
        {{ HTML::script('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js') }}
        {{ HTML::script('//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js') }}
        {{ HTML::script('angular/js/likeApp.js') }}
        {{ HTML::script('angular/js/commentApp.js') }}
    @stop

    @section('sidebar')
        @include('courses.partials.info')
        <hr>

        <dl>
            @foreach($course->occasions as $courseoccasion)
	    <dd>{{$courseoccasion->link}}
		@if($courseoccasion->start>=30)
			, lp 4
		@elseif($courseoccasion->start>=20)
			, lp 3
		@elseif($courseoccasion->start>=10)
			, lp 2
		@else
			, lp 1
		@endif</dd>
            @endforeach
        </dl>


    @stop

    @section('sidebar-master')
        @include('courses.partials.course')

@stop



@section('content-bottom')
        @if(Auth::check())
    @include('angular._comments')
    @include('angular._like')
    <!--<a href="" class="btn btn-large btn-primary" data-toggle="tooltip" title="Lägg till i favoriter"><i class="glyphicon glyphicon-heart"></i></a>-->

    <script type="text/javascript">
        angular.bootstrap(document.getElementById('commentApp'), ['commentApp']);
        angular.bootstrap(document.getElementById('likeApp'), ['likeApp']);
    </script>
        @endif
@stop
