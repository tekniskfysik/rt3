<fieldset>
    <legend>Kurser</legend>
    <div class="courses-form">
        <div class="form-group course-group hidden">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-7">
                {{ Form::select("course[id][]", $courseRepo->lists('title', 'id'), null, array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    {{ Form::text("course[ects][]", null, array('class' => 'form-control')) }}
                    <span class="input-group-addon">hp</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="btn btn-link remove-course-button"><span class="glyphicon glyphicon-remove"></span></div>
            </div>
        </div>


        @foreach($courses as $course)
        <div class="form-group course-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-7">
                {{ Form::select("course[id][]", $courseRepo->lists('title', 'id'), $course->id,
                array('class' => 'form-control')) }}
            </div>
            <div class="col-md-2">
                <div class="input-group">
                    {{ Form::text("course[ects][]", $course->pivot->ects, array('class' => 'form-control')) }}
                    <span class="input-group-addon">hp</span>
                </div>
            </div>
            <div class="col-md-1">
                <div class="btn btn-link remove-course-button"><span class="glyphicon glyphicon-remove"></span></div>
            </div>
        </div>
        @endforeach

    </div>

    <button class="btn btn-default pull-right" id="add-course-button">Lägg till</button>
</fieldset>