@if($course->closed)
	<i>Denna kurs ges inte längre.</i>
@endif

<h3>Beskrivning</h3>
<p>{{$course->desc}}</p>


<h3>Kategorier</h3>
@foreach($course->categories as $cat)
<li><a href="{{ URL::to('categories/' . $cat->id) }}">{{{$cat->title . " - " . $cat->pivot->ects}}}</a></li>
@endforeach

<h3>Spår</h3>
@foreach($course->tracks as $track)
<li><a href="{{ URL::to('tracks/' . $track->id) }}">{{{$track->title}}}</a></li>
@endforeach

<h3>Rekommenderad i spår</h3>
@foreach($course->recommendedInTracks as $track)
<li><a href="{{ URL::to('tracks/' . $track->id) }}">
    {{{$track->title}}}</a></li>
@endforeach

<h3>Förkunskapskrav</h3>
<p>
    @if(count($course->prerequisites) > 0)
    @foreach($course->prerequisites as $prereq)
<li>
    <a href="{{ URL::to('courses/' . $prereq->id) }}">{{{$prereq->title}}}</a>
</li>
@endforeach
@else
Denna kurs har inga andra kurser som förkunskapskrav
@endif
</p>

<h3>Ger behörighet</h3>
<p>
    @if(count($course->qualifies) > 0)
    @foreach($course->qualifies as $qual)
<li><a href="{{ URL::to('courses/' . $qual->id) }}">{{{$qual->title}}}</a></li>
@endforeach
@else
Denna kurs är inte förkunskapskrav för någon annan kurs
@endif
</p>


@if(Auth::user() && Auth::user()->isEditor())
<h1> FSR <small>{{link_to_route('fsrs.course', 'Redigera', $course->id) }}</small></h1>
@else
<h3>FSR</h3>
@endif

@foreach($course->fsrs as $fsr)
<li>
    <a href="{{ URL::to('fsrs/' . $fsr->id) }}">{{{$fsr->title}}}</a>
</li>
@endforeach

@if(isset($goals) && $goals)
    {{implode(" ", $goals->lists("label"))}}
@endif




<p>Uppdaterad: {{{$course->updated_at}}}</p>
