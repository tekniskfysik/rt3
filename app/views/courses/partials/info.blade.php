<p>{{$course->link}}</p>
<p>{{$course->department->link}}</p>
<p>Poäng: {{$course->ects}}</p>
<p>Nivå: {{{$course->level->title}}}</p>
<p><a href={{$course->url_evaluation}}>Kursutvärderingar</a></p>
<p><a href="{{{$course->url_homepage}}}">Kurshemsida</a></p>
<hr>
<p>Förkunskapskrav:</p>
@if (!count($course->prerequisites))
	<p>Det finns inga förkunskapskrav för denna kurs!</p>
@else
	<ul>
		@foreach ($course->prerequisites as $prereq)
			<li>{{$prereq->link}}</li>
		@endforeach
	</ul>
@endif
<hr>
<p>Kod: {{$course->code}}<br>
    @include('courses.partials.courseplan-search-extern')
</p>

