<?php $sum = new \RT\Courses\CourseSummarizer($courses, $privatecourses);?>

<div class="block-sum text-center">
	<p> {{$sum->ects()}} hp (300hp)</p>
</div>

<h1>Kategorier</h1>

<input class="btn btn-default show-histogram" type="button" value="Histogram" />
<input class="btn btn-default show-table" type="button" value="Tabell" />

<div id="chart-container3"></div>

<div id="table-container" class="table-responsive category-table" style="display: none;">
	<table class="table">
		<thead>
			<tr>
				<th>Kategorier</th>
				@foreach($sum->category() as $cat)		
					<th>{{$cat->category->title}}</th>
				@endforeach
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">Mål</th>
				@foreach($sum->category() as $cat)		
					<th style="font-weight: normal;">{{$cat->req}} hp</th>
				@endforeach
			</tr>
			<tr>
				<th scope="row">Summa</th>
				@foreach($sum->category() as $cat)		
					<th style="font-weight: normal;">{{$cat->sum}} hp</th>
				@endforeach
			</tr>
		</tbody>
	</table>
</div>

@section('scripts')

<script src="http://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">

	$(function() {
		<?php $plot = $sum->plotCategory();?>
		$('#chart-container3').highcharts( {{json_encode($plot)}} )
	});

</script>

@stop
