@extends('layouts.default')
@section('title', 'Kurser')
@section('content')

<div class="page-header">
    <h1>Kurser <small>sök bland alla kurser</small></h1>
</div>

{{ HTML::dataTable(array('code', 'link', 'ects', 'labels'), array('Kod', 'Kurs', 'Poäng', ''), $courses, 'courses') }}
@stop