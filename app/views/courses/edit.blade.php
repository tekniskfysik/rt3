@extends('layouts.default')
@section('title', 'Uppdatera -' . $course->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($course, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('courses.update', $course->id)))}}
	@include('courses.form')

	@include('categories.partials.categories-form', ['categories' => $course->categories])


	{{ Form::submit('Uppdatera kurs', array('class' => 'pull-right btn btn-primary')) }}
	{{ Form::close() }}
@stop