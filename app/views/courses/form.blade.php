<fieldset>

    {{Form::formGroup(Form::text('title', null, array('class' => 'form-control')), 'Kurs')}}
    {{Form::formGroup(Form::text('url_evaluation', null, array('class' => 'form-control')), 'Kursutvärderingar')}}
    {{Form::formGroup(Form::textarea('desc', null, array('class' => 'form-control')), 'Beskrivning')}}
	{{Form::formGroup(Form::text('url_homepage', null, array('class' => 'form-control')), 'Kurshemsida')}}
    {{Form::formGroup(Form::text('code', null, array('class' => 'form-control')), 'Kod')}}
    {{Form::formGroup(Form::text('ects', null, array('class' => 'form-control')), 'Poäng')}}
    {{Form::formGroup(Form::select('department_id', RT\Departments\Department::lists('title', 'id'), null, array('class'
    => 'form-control')), 'Institution')}}
    {{Form::formGroup(Form::select('level_id', RT\Levels\Level::lists('title', 'id'), null, array('class' =>
    'form-control')), 'Nivå')}}
    {{Form::formGroup(Form::select('prerequisites[]', RT\Courses\Course::lists('title', 'id'), isset($course) ?
    $course->prerequisites->modelKeys() : null, array('multiple' => true, 'class' => 'form-control my-select',
    'style'=>'width:100%')), 'Förkunskaper')}}
	{{Form::hidden('closed', 0)}}
    {{Form::formGroup(Form::checkbox('closed', 1), 'Stängd')}}
</fieldset>


<fieldset>
    <legend>Spår</legend>
    <div class="form-group">
        <label class="col-md-2 control-label">Ingår</label>
        <div class="col-md-10">
            {{ Form::select('tracks[]', RT\Tracks\Track::lists('title', 'id'),
            isset($course)  ? $course->tracks->modelKeys() : null, array('multiple' => true, 'class' => 'form-control my-select')) }}
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-2 control-label">Rekommenderas</label>
        <div class="col-md-10">
            {{ Form::select('recommendedInTracks[]', RT\Tracks\Track::lists('title', 'id'),
            isset($course)  ? $course->recommendedInTracks->modelKeys() : null, array('multiple' => true, 'class' => 'form-control my-select')) }}
        </div>
    </div>
</fieldset>
