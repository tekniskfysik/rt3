@extends('layouts.default')
@section('title', 'Lägg till kurs')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'courses')) }}
	@include('courses.form')

	@include('categories.partials.categories-form-create')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop
