@extends('layouts.default')
@section('title', isset($title)?$title:'Rapyd')

@section('styles')
{{ Rapyd::styles() }}
@stop

@section('content')
{{ $form->header }}

{{ $form->message }} <br />

@if(!$form->message)

{{ $form->render('title') }}

@endif

{{ $form->footer }}

@stop

@section('scripts')
{{ Rapyd::scripts() }}

@stop
