@extends('layouts.default')
@section('title', isset($title)?$title:'Rapyd')

@section('styles')
{{ Rapyd::styles() }}
@stop

@section('content')

@if(isset($filter) && isset($showFilter))
        <div class="well hidden-print">
            {{ $filter }}
        </div>
@endif
{{ $grid }}
@stop

@section('scripts')
{{ Rapyd::scripts() }}
@stop


