@extends('layouts.default')
@section('title', 'Uppdatera läsår -' . $academicyear->title)
@section('content')
	{{ Form::model($academicyear, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('academicyears.update', $academicyear->id)))}}
	@include('academicyears.form')
	{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop