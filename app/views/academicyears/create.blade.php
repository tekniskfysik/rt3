@extends('layouts.default')
@section('title', 'Lägg till läsår')
@section('content')
	{{ Form::open(array('url' => 'academicyears')) }}
	@include('academicyears.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop