@extends('layouts.default')
@section('title', 'Läsår')
@section('content')
	{{ HTML::table(['title', 'year'], null, $academicyears, 'academicyears', true, false) }}
	<a class="btn btn-small btn-success" href="{{ URL::to('academicyears/create') }}">Lägg till ny</a>
@stop