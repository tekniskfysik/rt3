{{ Form::open(array('route' => 'api.likes.store')) }}
{{ Form::hidden('likable_id', $likable_id) }}
{{ Form::hidden('likable_type', $likable_type) }}

<button type="submit" class="btn btn-link pull-right" style="padding: 0px; color: red">
    <i class="glyphicon glyphicon-heart"></i>
</button>

{{ Form::close() }}
