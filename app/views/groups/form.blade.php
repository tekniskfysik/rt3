<div class="form-group">
	{{ Form::label('name', 'Name') }}
	{{ Form::text('name', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('name', 'Name') }}
    {{ Form::select('users[]', \Acme\Users\User::lists('username', 'id'), isset($group) ? $group->users->modelKeys() : null, array('multiple' => true, 'class' => 'form-control')) }}
</div>

