@extends('layouts.default')
@section('title', 'Lägg till grupp')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'groups')) }}
	@include('groups.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop