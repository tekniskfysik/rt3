@extends('layouts.default')
@section('title', 'Uppdatera -' . $group->name)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($group, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('groups.update', $group->id)))}}
	@include('groups.form')
	{{ Form::submit('Spara', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop