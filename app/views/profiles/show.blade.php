@extends('layouts.default')
@section('title', $profile->title)
@section('sidebar-master')
	<p>{{$profile->desc}}</p>
	@foreach($profile->tracks as $n => $track)
		@if(count($profile->tracks)>1)
			<h1>Spår {{$n+1}}: {{$track->title}}</h1>
			<hr>
			<p>{{$track->desc}}</p>
		@endif
		@include('tracks.partials.track')
	@endforeach
@stop

@section('sidebar')
<img class=" img-circle pull-right" alt="{{$profile->title}}" src='{{URL::to('/')}}/images/{{$profile->image}}?:"http://dummyimage.com/400x400/000000/fff.png&text=$profile->title"}}' style="width:200px; height: 200px">
@stop
