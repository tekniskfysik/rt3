@extends('layouts.default')
@section('title', 'Lägg till profil')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'profiles')) }}
	@include('profiles.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop