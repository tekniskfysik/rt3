@extends('layouts.default')
@section('title', 'Uppdatera -' . $profile->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($profile, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('profiles.update', $profile->id)))}}
	@include('profiles.form')
	{{ Form::submit('Spara', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop