<div class="row">
    <br>
    <div class="col-md-3 col-sm-3 text-center">
        <a class="story-title" href="{{ route('profiles.show', $profile->id) }}"><img alt="{{$profile->title}}" src='{{URL::to('/')}}/images/{{$profile->image}}' style="width:200px; height: 200px" class="img-circle"></a>
    </div>
    <div class="col-md-9 col-sm-9">
        <h3><a href="{{ route('profiles.show', $profile->id) }}" class="text-muted">{{$profile->title}}</a></h3>
        <div class="row">
            <div class="col-xs-9">
                <p>{{substr($profile->desc, 0, 500)}}
                    @if(strlen($profile->desc) > 500)
                    ... <br><small><a class="badge" href="{{ route('profiles.show', $profile->id) }}" class="text-muted">Läs mer</a></small>
                    @endif
                </p>

                </div>
            <div class="col-xs-3"></div>
        </div>
        <br><br>
    </div>
</div>
<hr>
