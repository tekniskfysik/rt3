@extends('layouts.default')
@section('title', 'Profiler')
@section('content')
<p>Som student på Teknisk Fysik i Umeå har du, utåver möjligheten att fritt välja kurser också tillgång till flera fördefinerade profiler anpassade för att ge dig en djupare insikt inom de olika områdena.
Självklart kan du följa någon av dessa profiler och byta ut individuella kurser för att hitta det spår som passar just dig bäst.</p>
<hr>
        @foreach($profiles as $profile)
            @include('profiles.partials.preview', ['image'=> "/images/$profile->image"?:"http://dummyimage.com/400x400/000000/fff.png&text=$profile->title"])
        @endforeach

@stop
