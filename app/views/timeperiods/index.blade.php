@extends('layouts.default')
@section('title', 'Läsperiod')
@section('content')
	{{ HTML::table(['title', 'week_offset'], null, $timeperiods, 'timeperiods', true, false) }}
	<a class="btn btn-small btn-success" href="{{ URL::to('timeperiods/create') }}">Lägg till ny</a>
@stop