@extends('layouts.default')
@section('title', 'Uppdatera läsår -' . $timeperiod->title)
@section('content')
	{{ Form::model($timeperiod, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('timeperiods.update', $timeperiod->id)))}}
	@include('timeperiods.form')
	{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop