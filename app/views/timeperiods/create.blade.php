@extends('layouts.default')
@section('title', 'Lägg till läsperiod')
@section('content')
	{{ Form::open(array('url' => 'timeperiods')) }}
	@include('timeperiods.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop