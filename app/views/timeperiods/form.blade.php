<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('week_offset', 'Vecko-offset') }}
	{{ Form::text('week_offset', null, array('class' => 'form-control')) }}
</div>

