@extends('layouts.default')
@section('title', \Goals\Goals\Goal::$goalTypes[$goal->type] .": ". $goal->code)

@section('content')

<h3>{{$goal->title}}</h3>
<p>{{$goal->desc}}</p>


@if($goal->parent_id != 0)
<h3>Överordnat mål</h3>
{{$goal->parent->link}}
@endif

@if(count($goal->childs))
    <h3>Underordnade mål</h3>
    <ul>
        @foreach($goal->childs as $child)
        <li>{{$child->link}}</li>
        @endforeach
    </ul>
@endif

<hr>
<h3>FSR</h3>
<ul>
    @foreach($goal->fsrs as $fsr)
    <li>{{$fsr->link}} - {{$fsr->course->link}}</li>
    @endforeach
</ul>

@stop