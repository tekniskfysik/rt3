<div class="form-group">
	{{ Form::label('title', 'Titel') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('desc', 'Beskrivning') }}
	{{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('type', 'Typ av mål') }}
    {{ Form::select('type', [''=> 'Välj typ'] + \Goals\Goals\Goal::$goalTypes, null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('parent_id', 'Förälder') }}
    {{ Form::select('parent_id', [''=> 'Ingen förälder'] + \Goals\Goals\Goal::all()->lists('title', 'id'), null, array('class' => 'form-control')) }}
</div>