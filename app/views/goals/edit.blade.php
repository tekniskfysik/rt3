@extends('layouts.default')
@section('title', 'Redigera mål')
@section('content')

{{ Form::model($goal, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('goals.update', $goal->id)))}}
@include('goals.form')
{{ Form::submit('Uppdatera', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop
