@extends('layouts.default')
@section('title', 'Lägg till mål')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'goals')) }}
	@include('goals.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop