<h3>Profilkurser</h3>
@foreach ($track->courses as $course)
<li>
    <a href="{{ URL::to('courses/' . $course->id) }}">{{{$course->title}}}</a>
</li>
@endforeach

<h3>Rekommenderade kurser</h3>
@foreach ($track->recommendedCourses as $course)
<li>
    <a href="{{ URL::to('courses/' . $course->id) }}">{{{$course->title}}}</a>
</li>
@endforeach

<h3>Blockschema</h3>
@foreach ($track->blocks as $block)
<li>
    <a href="{{ URL::to('blocks/' . $block->id) }}">{{{$block->title}}}</a>
</li>
@endforeach
