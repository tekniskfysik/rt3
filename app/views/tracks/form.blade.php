<div class="form-group">
	{{ Form::label('title', 'Spår (visas endast om det finns fler än ett spår till samma profil)') }}
	{{ Form::text('title', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('desc', 'Beskrivning (visas endast om det finns fler än ett spår till samma profil)') }}
	{{ Form::textarea('desc', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('profile_id', 'Profil') }}
	{{ Form::select('profile_id', RT\Profiles\Profile::lists('title', 'id'), null, ['class' => 'selectpicker form-control']) }}
</div>

<div class="form-group">
	{{ Form::label('courses', 'Profilkurser') }}
	{{ Form::select('courses[]', RT\Courses\Course::orderBy('title')->lists('title', 'id'), isset($track) ? $track->courses->modelKeys():null,
    array('multiple' => true, 'data-live-search'=>"true", 'class'=>"selectpicker form-control")) }}
</div>

<div class="form-group">
    {{ Form::label('courses', 'Rekommenderade kurser') }}
    {{ Form::select('recommended_courses[]', RT\Courses\Course::orderBy('title')->lists('title', 'id'), isset($track) ? $track->recommendedCourses->modelKeys():null,
    array('multiple' => true, 'data-live-search'=>"true", 'class'=>"selectpicker form-control")) }}
</div>