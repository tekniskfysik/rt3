@extends('layouts.default')
@section('title', 'Uppdatera -' . $track->title)
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::model($track, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('tracks.update', $track->id)))}}
	@include('tracks.form')
	{{ Form::submit('Uppdatera spår', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}

<hr>

{{ Form::open(['url' => 'blocks']) }}
{{ Form::hidden('blockable_type', 'RT\Tracks\Track') }}
{{ Form::hidden('blockable_id', $track->id) }}
{{ Form::hidden('title', $track->title) }}
{{ Form::submit('Lägg till blockschema', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
@stop

