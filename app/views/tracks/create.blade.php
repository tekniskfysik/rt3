@extends('layouts.default')
@section('title', 'Lägg till spår')
@section('content')
	{{ HTML::ul($errors->all()) }}
	{{ Form::open(array('url' => 'tracks')) }}
	@include('tracks.form')
	{{ Form::submit('Lägg till', array('class' => 'btn btn-primary')) }}
	{{ Form::close() }}
@stop