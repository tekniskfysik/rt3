@extends('layouts.default')
@section('title', 'Spår - '.$track->title)
@section('content')

Profil: {{$track->profile->title}}
<p>Profildesc: {{$track->profile->desc}}</p>

        @include('tracks.partials.track')





<div>
	<a class="btn btn-small btn-info" href="{{ URL::to('tracks/' . $track->id . '/edit') }}">Redigera</a>

	{{ Form::open(array('url' => 'tracks/' . $track->id, 'class' => 'pull-right')) }}
		{{ Form::hidden('_method', 'DELETE') }}
		{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
	{{ Form::close() }}

</div>
@stop