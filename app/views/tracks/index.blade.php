@extends('layouts.default')
@section('title', 'Spår')
@section('content')
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
	@endif

{{ HTML::table(array('id', 'title'), null, $tracks, 'tracks', true, false) }}


	<a class="btn btn-small btn-success" href="{{ URL::to('tracks/create') }}">Lägg till ny</a>
	
@stop