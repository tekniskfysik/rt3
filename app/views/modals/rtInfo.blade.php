<div class="modal fade" id="rtInfo">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['url' => 'reports', 'class'=> 'form-horizontal', 'role'=>'form']) }}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Om Röda tråden</h4>
            </div>

            <div class="modal-body" style="margin: 10px;">
                <p>Röda Tråden är ett projekt som startats av ett par tekniska fysiker vid Umeå Universitet. Projektet
                    startade i maj 2010 och beräknas vara klart efter kommande terminsslut.</p>
                <p>Medverkande i projektet är: Stefan Hedman, Richard Tano, Tomas Berglund och Robin Lundberg.</p>
            </div>

            <div class="modal-footer">
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Avbryt</button>
                        {{ Form::submit('Skicka', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>

        </div>
        {{ Form::close() }}
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div><!-- /.modal -->