<div class="modal fade" id="report">
    <div class="modal-dialog">
        <div class="modal-content">
            {{ Form::open(['url' => 'reports', 'class'=> 'form-horizontal', 'role'=>'form']) }}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                        aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Skicka meddelande till programledningen</h4>
                <p><small>Det kan gälla allt från tips och ideér, frågor om kurser eller innehållet på våra hemsidor.</small></p>

            </div>

            <div class="modal-body" style="margin: 10px;">
                @include('reports.form')
            </div>

            <div class="modal-footer">
                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Avbryt</button>
                        {{ Form::submit('Skicka', array('class' => 'btn btn-primary')) }}
                    </div>
                </div>
            </div>

        </div>
        {{ Form::close() }}
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div><!-- /.modal -->