<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>@yield('title', 'Home Page')</title>
 
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
@yield('meta')
@section('style')
	 <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">
	 <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">
	 <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}">
@show
	<!-- App -->
	<script>
window.App = window.App || {};
	 App.siteURL = '{{ URL::to("/") }}';
	 App.currentURL = '{{ URL::current() }}';
	 App.fullURL = '{{ URL::full() }}';
	 App.apiURL = '{{ URL::to("api") }}';
	 App.assetURL = '{{ URL::to("assets") }}';
	</script>
 
	<!-- jQuery and Modernizr -->
	<script src="{{ URL::asset('assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{ URL::asset("assets/js/vendor/jquery-1.10.1.min.js") }}"><\/script>')</script>
@yield('script.header')
 
</head>
<body>
 
@yield('body')
@section('script.footer')
	 <!-- Script Footer -->
	 <script src="{{ URL::asset('assets/js/vendor/underscore-min.js') }}"></script>
	 <script src="{{ URL::asset('assets/js/vendor/backbone-min.js') }}"></script>
	 <script src="{{ URL::asset('assets/js/vendor/bootstrap.min.js') }}"></script>
	 <script src="{{ URL::asset('assets/js/main.js') }}"></script>
 
	 <script>
	 var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
	 (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
g.src='//www.google-analytics.com/ga.js';
s.parentNode.insertBefore(g,s)}(document,'script'));
	 </script>
@show
 
</body>
</html>
- See more at: http://laravelsnippets.com/snippets/default-layout-masterbladephp#sthash.qVCoWm6l.dpuf