<!DOCTYPE html>
<html lang="en">
@include('layouts.head')

    <body>
        <div id="wrapper">
            @include('navigation')
            @yield('container')
            @include('layouts.footer')
        </div>
        {{ HTML::script(url("js/main.js")) }}
        @yield('scripts')
    </body>
</html>
