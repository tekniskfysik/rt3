@extends('layouts.master')
@section('container')
	<div class="container">
	    <div class="row">
	       	<div class="col-lg-12">
	        	@yield('content')
	        </div>   
	    </div>
	    @include('layouts.footer') 
 	</div>		
@stop