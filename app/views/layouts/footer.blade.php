<div class="footer hidden-print">


    <a class="btn btn-social-icon btn-twitter bottom-icon" id="back-to-top" href="#"
       class="btn-lg circle back-to-top"
       role="button" title="Skicka meddelande eller rapportera fel"
       data-toggle="tooltip"

       data-placement="top">
        <span class="fa fa-envelope-o" data-toggle="modal" data-target="#report"></span></a>
    </a>

    <style>
        .bottom-icon {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
        }
        .footer-content div a{
            margin: 5px;

        }

    </style>

    <div class="footer-content" id="social-footer">
        <div class="row text-center">
            <a class="btn btn-social-icon btn-twitter" href="https://twitter.com/tekniskfysik">
                <i class="fa fa-twitter"></i>
            </a>
            <a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/tekniskfysikumu?ref=ts&fref=ts">
                <i class="fa fa-facebook"></i>
            </a>
            <a class="btn btn-social-icon btn-instagram" href="https://www.instagram.com/tekniskfysik/">
                <i class="fa fa-instagram"></i>
            </a>
			{{--
            <a class="btn btn-social-icon btn-linkedin" href="http://se.linkedin.com/groups?gid=2481961">
                <i class="fa fa-linkedin"></i>
            </a>
            <a class="btn btn-social-icon btn-rss" href="{{url('rss')}}">
                <i class="fa fa-rss"></i>
            </a>
			--}}
        </div>
    </div>

    <div class="footer-content" id="top-footer">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
					{{--
                    <div class="col-md-3">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Studienämnden</a></li>
                            <li><a href="{{url('it')}}">IT-gruppen</a></li>
                            <li><a href="{{url('pr')}}">PR-gruppen</a></li>
                        </ul>
                    </div>
					--}}
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="http://www.student.umu.se/examen/bestammelser/examensbeskrivningar">Examensbeskrivning</a></li>
                            <li><a href="http://www.teknat.umu.se/student/styrdokument-for-utbildning/utbildningsplaner">Utbildningsplan</a></li>
                            <li><a href="http://www.tekniskfysik.se/ar-du-student/examensarbete/">Examensarbete</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="http://www.physics.umu.se/student/tekniskfysik/">Programhemsida</a></li>
                            <li><a href="http://www.tekniskfysik.se/">Rekryteringssida</a></li>
                            <li><a href="http://www.physics.umu.se/student/tekniskfysik/blanketter/">Tillgodoräknande</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="https://www.cambro.umu.se/portal">Cambro</a></li>
                            <li><a href="https://cas.umu.se/login?service=https://www.portal.umu.se/uPortal/Login">Portalen</a></li>
                            <li><a href="http://www.physics.umu.se">Fysikinstitutionen</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-content" id="bottom-footer">
        <div class="container text-center">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-pills nav-justified">
                        <li data-toggle="modal" data-target="#rtInfo"><a href="#">Om Röda Tråden</a></li>
                        <!--<li data-toggle="modal" data-target="#coming"><a href="#">Developer API</a></li>-->
                        <li data-toggle="modal" data-target="#report"><a href="#">Rapportera fel</a></li>
                    </ul>
                </div>
            </div>
            <hr>
            <div class="row">Environment: {{App::environment()}}</div>
        </div>
    </div>
</div>

@include('modals.report')
