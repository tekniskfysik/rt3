<dl class="list-striped">
    @foreach ($instances as $instance )
    @if($instance->updated_at)
        <dt><a href="{{ route($route.".show", $instance->id) }}">{{ $instance->title }}</a></dt><dd>{{$instance->updated_at->diffForHumans() }}</dd>
    @endif
    @endforeach
</dl>
