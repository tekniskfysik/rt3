<div class="list-group pull-right">
    <ul class="nav nav-pills">
        @foreach($links->all() as $item)
        <li class="{{$item->class}}" data-toggle="tooltip" title="{{$item->tooltip}}">
            <a href="{{ $item->link }}">{{ $item->title }}</a>
        </li>
        @endforeach
    </ul>
</div>