@extends('layouts.master')
@section('title', 'Teknisk fysiks kursdatabas')
@section('container')
<div class="container" id="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        @include('flash::message')
                        <div class="jumbotron">
                            <h1>
				Välkommen{{Auth::check() ? " ". Auth::user()->link : " till Röda Tråden 3!" }}
                            </h1>

                            <p class="lead">
                                Röda tråden är en databas med information om kurserna på Teknisk fysik i Umeå. Den är till för dig som undrar över något om kurserna inom teknisk fysik. Du kan vara student, kursansvarig eller bara allmänt nyfiken på Teknisk fysiks kurser. Röda tråden visar översiktligt hur förkunskapskrav hänger ihop mellan kurserna.
                            </p>

                            <p>
                                Röda tråden har byggts upp av studenter på programmet och drivs amanuenserna, med stöd från kursansvariga lärare och engagerade studenter.
                            </p>
				<p>Är du intresserad av att titta på våra baskurser? Det blockschemat kan du hitta <a href="{{url('blocks/5021')}}">här.</a>
				</p>
                            @unless(Auth::check())
                                <p>Vi noterar att du ej är inloggad i systemet. När man är inloggad kan man göra egna scheman och planera sin studiegång på ett smidigt sätt.</p>
                            @endunless



                        </div>
                    </div>
                </div>
		<div class="col-md-6">
			<div class="panel-primary panel">
			    <div class="panel-heading">
				<h3 class="panel-title"><a href="{{url('profiles')}}">Profiler</a></h3>
			    </div>
			    <div class="panel-body">
				<div class="row text-center">
				    @foreach(RT\Profiles\Profile::all() as $profile)
				    <div class="col-md-4">
					<div>
						<a href="{{ route('profiles.show', $profile->id) }}"> 
						@if($profile->image)
							<img src='{{URL::to('/')}}/images/{{$profile->image}}'
							style="height: 150px; width: 100%" class="img-circle"> </a>
						@else
							<img src='{{"http://dummyimage.com/400x400/000000/fff.png&text=$profile->title"}}'
							style="height: 150px; width: 100%" class="img-circle"> </a>

						@endif
					    <a href="{{ route('profiles.show', $profile->id) }}"> {{$profile->title}} </a>
					</div>
				    </div>
				    @endforeach
				</div>
			    </div>
			</div>
		</div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="panel-primary panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><a href="{{url('categories')}}">Kategorier</a></h3>
                            </div>
                            <div class="panel-body">
								<dl class="list-striped">
								@foreach (RT\Categories\Category::all() as $cat)
									<dt>{{ $cat->link }}</dt>
								@endforeach
								</dl>
                                <div id="chart-div" style="margin:0; padding:0">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Kursuppdateringar</h3>
                            </div>
                            <div class="panel-body">
                                @include('layouts.partials.recentList', ['instances' => RT\Courses\Course::recentUpdated()->get(),
                                'route' => 'courses'])
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Blockuppdateringar</h3>
                            </div>
                            <div class="panel-body">
                                @include('layouts.partials.recentList', ['instances' => RT\Blocks\Block::recentUpdated()->get(),
                                'route' => 'blocks'])
                            </div>
                        </div>
                    </div>

                </div>

               
            </div>
        </div>
    </div>

@stop

