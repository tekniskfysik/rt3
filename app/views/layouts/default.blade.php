@extends('layouts.master')
@section('container')
	<div class="container" id="content">
		@include('flash::message')
		{{ Breadcrumbs::render()}}
		{{ Session::get('message') }}
		
		<div class="row">
			<div class="col-lg-9">
				<h1 id="container">@yield('title')</h1>
			</div>
			<div class="col-lg-3 hidden-print">
				<div class="list-group pull-right">
					<ul class="nav nav-pills">
					@if(isset($links))
						@foreach($links->all() as $item)
							<li class="{{$item->class}}" data-toggle="tooltip", title="{{$item->tooltip}}">
							<a href="{{ $item->link }}"><i class="glyphicon {{ $item->icon }}"></i></a>
							</li>
						@endforeach
					@endif
					</ul>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				@yield('content')
			</div>
		</div>

		<div class="row">
			<div class="col-lg-8">
				@yield('sidebar-master')
			</div>
			<div class="col-lg-4">
				@yield('sidebar')
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				@yield('content-bottom')
			</div>
		</div>
	</div>
@stop
