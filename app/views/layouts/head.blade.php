<head>
    <meta charset="utf-8">
    <title>Röda tråden | @yield('title', 'Röda tråden')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{ HTML::style('css/app.css') }}
    {{ HTML::style('vendor/select2.min.css') }}
    {{ HTML::style('vendor/selectize.css') }}
		{{ HTML::style('vendor/bootstrap-select.min.css') }}

    @section('angular')
    @show
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
