@section('angular')
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js') }}
    {{ HTML::script('//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js') }}
    {{ HTML::script('angular/js/fsrApp.js') }}
@stop

@section('content')
<div ng-app="fsrApp" ng-controller="fsrCtrl">
	<table class="table table-striped .table-condensed"><tr><th></th><th>Desc</th><th ng-show="editor"><th ng-show="admin"></th><th>Status</th></tr>
		<tr ng-repeat="goal in goals | object2Array | orderBy:'title'">
			<td>@{{goal.title}}</td>
			<td>@{{goal.desc}}</td>
			<td ng-show="editor">
				<i class="btn @{{ goal.pivot.requested?'btn-primary':'btn-default'}}" ng-click="toggleRequest(goal)">&nbsp;</i>
			</td>
			<td ng-show="admin">
				<i class="btn @{{ goal.pivot.approved?'btn-primary':'btn-default'}}" ng-click="toggleApprove(goal)">&nbsp;</i>
			</td>
			<td>
				<span ng-show="goal.pivot.approved==goal.pivot.requested" class="label label-success">Godkänd</span>
				<span ng-show="goal.pivot.approved!=goal.pivot.requested" class="label label-danger">Väntar på att bli godkänd</span>
			</td>
		</tr>
	</table>
</div>
