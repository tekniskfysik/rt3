@extends('layouts.default')

@section('angular')
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js') }}
    {{ HTML::script('//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js') }}  
    {{ HTML::script('angular/js/courseApp.js') }}   
@stop

@section('content') 

    <div id="courseApp" ng-app="courseApp" ng-controller="CoursesCtrl"> 

        <div ng-show="selectedRow" class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">@{{course.title}} (@{{course.ects}} hp)
                    <div class="pull-right">
                        <span style="cursor:pointer;" ng-click="selectedRow=''" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                            <i class="glyphicon glyphicon-remove"></i>
                        </span>
                    </div>
                </h3>
            </div>
            <div class="panel-body">
                <p ng-bind-html="to_trusted(course.desc)"></p>
            </div>
        </div>  


         <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Kurser
                    <div class="pull-right">
                        <span style="cursor:pointer;" ng-model="toggleFilter" ng-click="toggleFilter =! toggleFilter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                            <i class="glyphicon glyphicon-filter"></i>
                        </span>
                    </div>
                </h3>
            </div>
            <div class="panel-body" ng-show="toggleFilter">
                <form class="form-inline" role="form" style="height:60px; padding:10px;">
                    <div class="form-group">
                        <label class="filter-col"></label>
                        <input ng-model="query" ng-change="search()" placeholder="Sökning" type="text" class="form-control input">
                    </div><!-- form group [search] -->

                    {{Form::select('category_id', ['' => 'Kategori'] + RT\Categories\Category::lists('title', 'id'), '', array('ng-model' => 'cat_id', 'ng-change' => "search()", 'class' => 'form-control'))}}

                    {{Form::select('level_id', ['' => 'Nivå'] + Level::lists('title', 'id'), '', array('ng-model' => 'level_id', 'ng-change' => "search()", 'class' => 'form-control'))}} 

                    <div class="form-group">
                        <label class="filter-col" style="margin-right:0;" for="pref-perpage">Antar rader:</label>
                        <select ng-model="limit" ng-change="getData()" class="form-control">
                            <option selected="selected" value="10">10</option>
                            <option value="15">15</option><option value="20">20</option> <option value="30">30</option>
                        </select>                                
                    </div> <!-- form group [rows] -->
                </form>
            </div>

            <table class="table table-hover table-striped" id="dev-table">
                <thead><tr><td>#</td><td>Kurs</td><td>Hp</td><td></td></tr></thead>
                <tbody>
                    <tr ng-click="setSelected(course.id);" 
                    ng-class="{success: course.id === selectedRow, warning: course.id != selectedRow && selectedRows.indexOf(course.id) > -1}" ng-repeat="course in courses | orderBy:'title' ">
                        <td class="col-md-1"><div ng-bind-html="to_trusted(course.level)"></div></td>
                        <td class="col-md-8"><a href="courses/@{{course.id}}">@{{course.title}}</a></td>
                        <td class="col-md-1"> @{{course.ects}} </td>
                        <td class="col-md-2"><div ng-bind-html="to_trusted(course.categories)"></div></td>

                    </tr>
                </tbody>
            </table>
            <div class="panel-footer text-center">
                <pagination style="margin:0px;" ng-show="paginator.total_pages" total-items="paginator.total_count" items-per-page="paginator.limit" ng-model="currentPage" ng-change="getData()"></pagination>    
            </div>
        </div>   
    </div>

@stop