@section('angular')	
{{ HTML::script('https://ajax.googleapis.com/ajax/libs/angularjs/1.2.18/angular.min.js') }}
{{ HTML::script('angular/js/app.js') }}
{{ HTML::script('angular/js/controllers/commentCtrl.js') }}
{{ HTML::script('angular/js/services/commentService.js') }}
@stop

<div class="col-md-8" ng-app="commentApp" ng-controller="commentController">

	<!-- PAGE TITLE =============================================== -->
	<div class="page-header">
		<h2>Kommentarssystem</h2>
	</div>

	<!-- NEW COMMENT FORM =============================================== -->
	<form ng-submit="submitComment()"> <!-- ng-submit will disable the default form action and use our function -->

		<!-- COMMENT TEXT -->
		<div class="form-group">
			<input type="text" class="form-control input-lg" name="comment" ng-model="commentData.text" placeholder="Skriv din kommentar">
		</div>

		<input type="hidden" name="private" value="ehj">
		<!-- <div class="form-group">
			<input type="checkbox" class="form-control input-sm" data="true" name="private" ng-model="commentData.private">
		</div>	 -->	
		
		<!-- SUBMIT BUTTON -->
		<div class="form-group text-right">	
			<button type="submit" class="btn btn-primary btn-lg">Kommentera</button>
		</div>
	</form>

	<!-- LOADING ICON =============================================== -->
	<!-- show loading icon if the loading variable is set to true -->
	<p class="text-center" ng-show="loading"><span class="fa fa-meh-o fa-5x fa-spin"></span></p>

	<!-- THE COMMENTS =============================================== -->
	<!-- hide these comments if the loading variable is true -->
	<div class="comment" ng-hide="loading" ng-repeat="comment in comments">
		<h3><small><% comment.author %> (<% comment.created_at %>)</h3>
		<p ng-click="editable = 'text'; edit(comment)" ng-hide="editable == 'text'"><% comment.text %></p>
		<span ng-show="editable == 'text'">
                <form class="form-inline">
                    <input type="text" size="30" ng-model="editedText" ng:required>
                    <button class="btn btn-success"  ng-click="editable = ''; save(comment, editedText);">Ok</button>
                    <button class="btn btn-warning" ng-click="editable = ''; editedText=comment.text">Cancel</button>
                </form>
            </span>
		
		<p><a href="#" ng-click="deleteComment(comment.id)" class="text-muted">Delete</a></p>
	</div>
	
</div>