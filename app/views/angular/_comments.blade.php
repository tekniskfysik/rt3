<div id="commentApp" ng-controller="CommentsCtrl" ng-cloak>
    <div class="btn-toolbar" ng-show="authenticated">
        <div class="btn-toolbar"><i class="btn glyphicon glyphicon-plus" ng-click="toggleAddMode()"></i></div>
    </div>
    <div ng-show="addMode">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Ny kommertar</h3></div>
            <div class="panel-body">
                <input ng-model="comment.text" class="form-control input-lg" placeholder="Skriv din kommentar"/>
            </div>
            <div class="panel-footer">
                <i class="btn glyphicon glyphicon-ok" ng-click="addcomment()" class=""></i>
                <i class="btn glyphicon glyphicon-remove" ng-click="toggleAddMode()"></i>
            </div>
        </div>
    </div>
    <div class="comment" ng-hide="loading" ng-repeat="comment in filtered = (comments | filter : {commentable_type:commentable_type, commentable_id:commentable_id}) | orderBy:'-updated_at' |startFrom:(currentPage-1)*pageSize | limitTo:pageSize">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">@{{comment.author}} <small>(@{{comment.updated_at}})</h3>
            </div>
            <div ng-show="comment.editMode == null || comment.editMode == false">
                <div class="panel-body">@{{comment.text}}</div>
                <div class="panel-footer" ng-show="authenticated == comment.user_id">      
                    <i class="btn glyphicon glyphicon-pencil" ng-click="toggleEditMode(comment)"></i>
                    <i class="btn glyphicon glyphicon-remove" ng-click="deletecomment(comment)"></i>
                </div>
            </div>
            <div ng-show="comment.editMode == true">
                <div class="panel-body">
                    <input ng-model="comment.text" class="form-control input-lg"/>
                </div>
                <div class="panel-footer">      
                    <i class="btn glyphicon glyphicon-ok" ng-click="updatecomment(comment)"></i>
                    <i class="btn glyphicon glyphicon-remove" ng-click="toggleEditMode(comment)"></i>
                </div>
            </div>         
        </div>
    </div>
    <pagination ng-show="totalItems>pageSize" total-items="filtered.length" items-per-page="pageSize" ng-model="currentPage" ng-change="pageChanged()"></pagination>   
     
</div>
