<!doctype html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>@yield('title', 'Röda Tråden')</title>

		@section('styles')
			{{ HTML::style('css/style.css') }}
		    {{ HTML::style('css/min/all.css') }}
		    {{ HTML::style(url('vendor/selectize/css/selectize.bootstrap3.css')) }}
		@show

		@section('scripts')
		{{HTML::script(url("//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"))}}
		{{HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js')}}
		{{HTML::script(url("js/bootswatch.js"))}}
		{{HTML::script(url("vendor/selectize/js/standalone/selectize.min.js"))}}
    	{{HTML::script(url("js/smartSearch.js"))}}
		<script type="text/javascript">
		    var root = '{{url("/")}}';
		</script>

		@show

		@section('angular')	
		@show
	</head>

	@section('body')
		<body class="container">
			@include('navigation')
			<div class="row">
				<div class="col-lg-12">
					@yield('content')
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9">
					@yield('sidebar-master')
				</div>
				<div class="col-lg-3">
					@yield('sidebar')
				</div>
			</div>
			@include('layouts.footer')
		</body>
	@show

</html>
	