@extends('layouts.master')
@section('container')
    <style type="text/css">html,body {
        height:100%;
        width:100%;
        position:relative;
    }
    #background-carousel{
        position:fixed;
        width:100%;
        height:100%;
        z-index:-1;
    }
    .carousel,
    .carousel-inner {
        width:100%;
        height:100%;
        z-index:0;
        overflow:hidden;
    }
    .item {
        width:100%;
        height:100%;
        background-position:center center;
        background-size:cover;
        z-index:0;
    }
     
    #content-wrapper {
        position:absolute;
        z-index:1 !important;
        min-width:100%;
        min-height:100%;
    }
    .well {
        opacity:0.85
    }</style>

    <div id="background-carousel">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
          <div class="item active" id="content-wrapper">
    <!-- PAGE CONTENT -->
        <div class="container">
            <div class="page-header"><h3>Fullscreen Background Carousel</h3></div>
            <div class="well"><p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.<br><a href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank" class="label label-danger">Bootstrap 3 - Carousel Collection</a></p>
            </div><!-- End Well -->
        </div><!-- End Container -->
    <!-- PAGE CONTENT -->
    </div>
            <div class="item" style="background-image:url(http://placehold.it/1600x800/)"></div>
          </div>
        </div>
    </div>
     
    <script type="text/javascript">
        $(document).ready(function(){
            $('#myCarousel').carousel({
            pause: 'none'
        })
        });
    </script>
@stop