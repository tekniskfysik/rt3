var $ = require('jquery');

module.exports = function () {

    $("#add-category-button").click( function(e){
        e.preventDefault();

        var categoryRow = $('.category-group:first');
        var categories = $('.categories-form');
        console.log(categoryRow.html());

        var formgroup = '<div class="form-group category-group">' + categoryRow.html() + '</div>';
        categories.append(formgroup);
    });

    $(".categories-form").on('click', '.remove-category-button', function(e){
        $(this).parent().parent().remove();
    });

    $("#add-course-button").click( function(e){
        e.preventDefault();

        var courseRow = $('.course-group:first');
        var courses = $('.courses-form');
        console.log(courseRow.html());

        var formgroup = '<div class="form-group course-group">' + courseRow.html() + '</div>';
        courses.append(formgroup);
    });

    $(".courses-form").on('click', '.remove-course-button', function(e){
        $(this).parent().parent().remove();
    });




};
