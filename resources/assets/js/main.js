var root = '{{url("/")}}';
global.root = root;

var $ = require('jquery');
global.jQuery = $;
global.$ = $;

require('bootstrap');

require("./vendor/select2.min.js");

require("./vendor/bootstrap-tagsinput.js");

require("./vendor/typeahead.bundle.js");

// selectpicker
require("./vendor/bootstrap-select.min.js");

require('./smartSearch.js')();

require('./ajaxHelper.js')();

require('./dynamicForm.js')();

require('./bootstrap-table.js')();

require('./block.js')();



// Used in course form etc.. 
$(".my-select").select2({});



// Used in track form and fsr index
$('.selectpicker').selectpicker();


/*
 * Confirmation on click
 */
$(".confirm").on("click", function() {
    return confirm($(this).data("confirm"));
});

/**
 * Tooltip event
 */
$("body").tooltip({
    selector: '[data-toggle=tooltip]'
});


/**
 * Model module for pop over modal
 */
$('#flash-overlay-modal').modal();


$('.my-dropdown').dropdown();
$('.my-dropdown').tooltip();

//Add Hover effect to menus
$('ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).fadeIn(0);
}, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(10).fadeOut();
});

	$(".show-table").click(function() {
		$(".category-table").show("slow");
		$("#chart-container3").hide("fast");
	});

	$(".show-histogram").click(function() {
		$("#chart-container3").show("slow");
		$(".category-table").hide("fast");
	});
