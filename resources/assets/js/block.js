var $ = require('jquery');
var d3 = require('d3');

module.exports = function(){

    if(window.rt === undefined){
        return;
    }

    var courses;
    var coursesData;
    var url = rt.block_url;
    var baseUrl = rt.base_url;

    var startYear;
    $.get(url , function( data ) {
        courses = data.course_occasions;
	startYear = data.start_year;
        for(var i in courses){
            var occasion = data.course_occasions[i];
            var course = occasion.course;
            occasion.title = course.title;
            occasion.speed = parseInt(course.ects * 10 * 5 / occasion.weeks);
            occasion.length = occasion.weeks;
        }
        var priv = data.private_courses;
        for(var i in priv){
            var occasion = data.private_courses[i];
            occasion.speed = parseInt(occasion.ects * 10 * 5 / occasion.weeks);
            occasion.length = occasion.weeks;
            occasion.type = 'private';

            courses.push(occasion);
        }
        coursesData = render(courses, parseInt(startYear));
        renderBlock(coursesData);
        //var sum = blockSummary(data.course_occasions);
        //renderCategorySummary(sum);
    });
    
    /*                       ONLY STUFF FOR ORDERING FIXNG COURSESDATA                     
     ***************************************************************************************
     */                                                                                     
    
    
     /**
     * creates a matrix of elements rows and assign each row with an empty array
     *
     * @param elements
     * @returns {Array}
     */
    function createMatrix(elements) {
        var matrix = [];
        for (var i = 0; i < elements; i++) {
            matrix.push([]);
        }
        return matrix;
    }

    function sort(courses){
        /**
         * Sort array on start attribute in ascending oreder
         *
         * @type {Array.<{start: number, length: number, speed: number, id: number}>}
         */
        courses = courses.sort(function (a, b) {
            return a.start - b.start
        });

        /**
         * Sort attay on length attribute in desceding order
         *
         * @type {Array.<{start: number, length: number, speed: number, id: number}>}
         */
        courses = courses.sort(function (a, b) {
            return b.length - a.length
        });
        return courses;
    }


    /**
     * Find first possible row where a submatrix defined by the course fits.
     *
     * @param course
     * @param matrix
     */
    function getFreeRow(course, matrix) {
        var emptyRows = 0;
        for (var row = 0; row < 1000 && emptyRows < course.speed; row++) {
            if (isEmptyRow(course, row, matrix)) {
                emptyRows++;
            }
            else {
                emptyRows = 0;
            }
        }
        course.row = row - course.speed;
    }

    /**
     * Determine if a row for a course is empty
     *
     * @param course
     * @param row
     * @returns {boolean}
     */
    function isEmptyRow(course, row, matrix) {
        var sum = 0;
        for (var col = course.start; col < course.start + course.length && col < matrix.length; col++) {
            if (matrix[col][row] == undefined) {
                matrix[col].push(0);
            }
            sum += matrix[col][row];
        }
        return sum == 0;
    }

    /**
     * Allocates space in matrix for a course
     *
     * @param course
     * @param matrix
     */
    function allocateRows(course, matrix) {
        for (var week = course.start; week < course.start + course.length && week < matrix.length; week++) {
            for (var i = course.row; i < course.row + course.speed; i++) {
                matrix[week][i] = course.id;
            }
        }
    }

    /**
     * Generate position for all courses in the matrix
     *
     * @param courses
     * @param matrix
     */
    function prepareBlock(courses, matrix) {
        for (var i = 0; i < courses.length; i++) {
            getFreeRow(courses[i], matrix);
            allocateRows(courses[i], matrix);
        }
    }

    /**
     * Render occasion from a set of occasions such that each course get a placement and year in the block.
     * @param allCourses
     * @returns {Array}
     */
    function render(allCourses, startYear){
        var data = [];
        var course;
        
        //Need to figure out the differance between
        //start year and the last year
        
        var endYear = startYear+5;
        for(var i=0; i<allCourses.length; i++) {
        	if(parseInt(allCourses[i].year)>endYear) {
        		endYear = parseInt(allCourses[i].year);
        	}
				}
        for(var i = 0; i<(endYear-startYear); i++){
            data[startYear+i] = [];
        }

        allCourses = sort(allCourses);

        for(var i in allCourses){
            course = allCourses[i];
            if (data[course.year] == undefined) {
                data[course.year] = [course];
            }else{
                data[course.year].push(course);
            }
        }

        var output = [];
        for( var year in data){
            var courses = data[year];
            var matrix = createMatrix(40);
            prepareBlock(courses, matrix);
            output.push({year: year, courses: courses});
        }

        return output;
    }
    
    /*                    ONLY STUFF FOR RENDERING STUDYBLOCK                              
     ***************************************************************************************
     */                                                                                    

		/*
		 * Calculates the courseBlock height from the
		 * coursesData[i].courses dataset
		 */
		function courseBlockHeight(dataset, scale, margin){
				var maxHeight = 1;
				for(var i = 0; i < dataset.length; i++){
						var block = dataset[i];
						var height = block.row + block.speed;
						if(height > maxHeight)
								maxHeight = height;
				}
				return maxHeight * scale + 2 * margin;
		}

		/*
		 * Calculates the height of a blockrow without the 
		 * Height of the part with courses (courseBlock), since this is a constant
		 * for all rows i call in it blockRowBaseHeight! Even tough that can
		 * be slightly misleading, WHAT THA HELL IS A BASE ANYWAY!?
		 *
		 * All heights is calculated from basic css properties so if that change
		 * this function will take that into consideration
		 */
		function blockRowBaseHeight() {
				//Need to add some hidden divs with correct class to get css properties
				var blockFooter = $("<div class='block-footer'><div class='block-footer-lp'><p style='margin: 0px;'>text</p></div></div>").hide().appendTo("body");
				var	blockHeader = $("<div class='block-header'>1234</div>").hide().appendTo("body");

				//Get height values from css properties
				var footerHeight = parseInt($(".block-footer").css("height")),
						titleHeight = parseInt($(".block-header").css("height")),
						//Here we need to specify Left,Right,Top or Bottom according to stupid firefox
						titlePadding = parseInt($(".block-header").css("paddingTop")), 
						baseHeight = footerHeight+titleHeight+titlePadding;
				
				//Remove hidden divs
				blockFooter.remove();
				blockHeader.remove();

				return baseHeight
		}

		function getTopOffsets(coursesData, scale, margin) {

				var baseHeight = blockRowBaseHeight();
				var topOffsetValues = [0];

				//Logic is slightly forced here, but just recall that
				//we start with pixel value of zero and when we push
				//we always push to topOffsetValues[i+1]
				for(var i=0; i<coursesData.length; i++) {
						topOffsetValues.push(courseBlockHeight(coursesData[i].courses, scale, margin)+baseHeight);
						topOffsetValues[i+1] += topOffsetValues[i];
				}

				var topOffset = d3.scale.ordinal()
						.domain(d3.range(coursesData.length))
						.range(topOffsetValues);
	
				return topOffset;
		}

		function yearButtonHeight() {
				var yearButton = $("<div class='block-row block-header add-year text-center' style='opacity: 1; left: 0px; right: 0px; position: absolute;'>Add Year</div>").hide().appendTo("body");
				var yearButtonHeightValue = parseInt($(".add-year").css("height"));

				yearButton.remove();

				return yearButtonHeightValue;
		}

		function blockRowMargin() {
				var	blockHeader = $("<div class='block-row'></div>").hide().appendTo("body");
				//Here we need to specify Left,Right,Top or Bottom according to stupid firefox
				var blockRowMarginValue = parseInt($(".block-row").css("marginTop"))
				blockHeader.remove();

				return blockRowMarginValue;
		}

		/*
		 * Append a course year to courseData
		 * Assumes sorted coursesData
		 */
		function addCourseYear(coursesData) {
				var curLastCourseYear = parseInt(coursesData[(coursesData.length)-1].year);
				var extraCourseYear = {courses: [], year: (curLastCourseYear+1)};
				coursesData.push(extraCourseYear);
		}

		/*
		 * Goes through the block and finds if you have all
		 * the prerequisites to take a course.
		 * Returns 1 if yes, 0 if no
		 */
		function doIHavePrerequisites(coursesData, prerequisites, year, start) {	
			var prerequisitesId = [];
			if (!prerequisites) {
				return 1;
			}	

			for (var i in prerequisites) {
				prerequisitesId.push(prerequisites[i].id);
			}

			var startYear = parseInt(coursesData[0].year);
			var ids = [];

			for (var i = year-startYear; i >= 0; i--) {
				if (i == year-startYear) {
					for (var j in coursesData[i].courses) {
						if (coursesData[i].courses[j].start < start) {
							ids.push(coursesData[i].courses[j].course_id);
						}
					}
				} else {
						for (var j in coursesData[i].courses) {
							ids.push(coursesData[i].courses[j].course_id);
						}
				}
			}

			for (var i in prerequisitesId) {
				if (ids.indexOf(prerequisitesId[i]) == -1) {
					return 0;
				}
			}

			// If any of the plucked course ids fits with the prerequisites
			// return 1
			return 1;
		}

		/*
		 * Removes a specific course from the coursesdata matrix
		 */
		//function removeCourseFromData(coursesData, courseId) {
		//	var i, j;

		//	for (i in coursesData) {
		//		var blockYear = coursesData[i].courses;
		//		for (j in blockYear) {
		//			if (blockYear[j].id == courseId) {
		//				blockYear.splice(j, 1);
		//			}
		//		}
		//	}
		//	return coursesData;
		//}

		/*
		 * Sums over the amount of hp in a study period.
		 */
		function hpSumInPeriod(coursesData, year, lp) {
			var startYear = parseInt(coursesData[0].year);
			var correctIndex = year - startYear;
			var correctCourses = coursesData[correctIndex].courses;
			var sum = 0;

			for (var i in correctCourses) {
				var start = correctCourses[i].start;
				var length = correctCourses[i].length;

				// Silly logic needed because apparently private and public courses do not store
				// their ects the same way
				if ( correctCourses[i].type != 'private' ) {
					var speed = parseFloat(correctCourses[i].course.ects)/length;
				} else {
					var speed = parseFloat(correctCourses[i].ects)/length;
				}
				var temp = 0;

				if (start < 10 * (lp - 1)) {
					if (length > (10 * (lp - 1) - start)) {
						sum = sum + (length - (10 * (lp - 1) - start))*speed;
					}
				} else if (start < 10 * lp) {
					if (length > (10 * lp - start)) {
						sum = sum + (10 * lp - start)*speed;
					} else {
						sum = sum + length*speed;
					}
				}
			}
			return sum;
		}

		/*
		 * Renders studyBlock from the array of objects courseData.
		 * If courseData changes you can just call this function again
		 * and the studyBlock will be updated accordingly with fancy
		 * animation representing the action taken.
		 *
		 * In order for the rendering to function properly the coursesData
		 * array must be sorted by years before beeing passed in
		 */
		function renderBlock(coursesData) {


				var xMax = 40;
				var margin = 2;
				var scale = 4;
				var animLength = 500;

				//Creating a scale that maps the order of coursesData
				//to topOffsets, I.e. the blockRow that corresponds to
				//coursesData[i] will have the top position of topOffset(i)
				var topOffset = getTopOffsets(coursesData, scale, margin);
		
				//Contains the blockrows absolute coordinates
				var studyBlock = d3.select("#studyBlock")
						.style("position", "relative")
						.style("height", (topOffset(coursesData.length) + yearButtonHeight() +blockRowMargin()*2) + "px");
		
				//Data join - update selection
				var blockRow = studyBlock.selectAll(".block-row")
						.data(coursesData, function(d) {return d.year;})

				//Update position
				blockRow.transition()
						.duration(animLength)
						.style("top", function(d, i) {return topOffset(i)+"px";});

				//ENTER
				//Only added object logic here

				//Adding block row "group div"
				var blockRowNew = blockRow.enter()
					.append("div")
						.attr("class", "block-row text-center")
						.style("left", 0 + "px")
						.style("right", 0 + "px")
						.style("position", "absolute") //Forcing the div to follow top style
						.attr("id", function(d) {return d.year;})
						.style("top", function(d, i) { return topOffset(i)+"px";})
						.style("opacity", 1e-6);

				//Animate transition for new blockRows
				blockRowNew.transition()
						.duration(animLength)
						.style("opacity", 1);
	
				//Adding title div
				var titles = blockRowNew
					.append("div")
						.attr("class", "block-header")
						.text(function(d){return (parseInt(d.year)) + " - " + (parseInt(d.year)+1);})

				//Block which holds the courses 
				var years = blockRowNew
					.append("div")
					.attr("class", "block")
			 		.style("height", function(d){
						var height = courseBlockHeight(d.courses, scale, margin);
						return height + "px";
					});

				//Footer that holds the blocks for each lp (in which you add courses)
				var footer = blockRowNew.append("div").attr("class", "block-footer");

				var footerLp = footer.selectAll(".block-footer-lp")
						.data(function(d){
								return [
										{lp:1, year:d.year},
										{lp:2, year:d.year},
										{lp:3, year:d.year},
										{lp:4, year:d.year}
								];
						})
					.enter()
					.append("div")
						.attr("class", "block-footer-lp");

				//Adds text with pace and amount of points per study period
				footerLp
					.append("p").attr("class", "pull-left").style("margin", "0")
						.text(function(d){
							var hp = Math.round(hpSumInPeriod(coursesData, d.year, d.lp)*10)/10;
							var pace = hp/10, percent = 0;

							if (pace >= 0.375) {
								for (pace; pace >= 0.375; pace -= 0.375) {
									percent += 25;
								}
							}

							return percent + " % / " + hp + " hp";
						});

				//Button used to add courses with
				var addButtons = footerLp.append("div").attr("class", "btn btn-link pull-right").append("a")
					.append("span").attr("class", "glyphicon glyphicon-plus").attr("data-toggle", "tooltip").attr("title", "Lägg till kurs").attr("data-placement", "left");

				//Button action when you want to add course
				addButtons.on('click', function(d){
					$.post(baseUrl + "/courseOccasionList", {year: d.year, start: (d.lp-1)*10, block_id: rt.block_id}, function( data) {
						$('.modal-body').html(data);
						$('.modal-title').html("Lägg till kurs på läsperiod " + d.lp +", " + d.year + " - " + (parseInt(d.year)+1));
						$('#myModal').modal();
					});
				});
				$(function(){
					$('.dropdown-toggle').dropdown();
				});

				//Append courses to block
				courses = years.selectAll(".block-item")
						.data(function(d){
								return d.courses;
						})
					.enter()
					.append("div")
					.attr('class', function(d){
						if (d.type) return "block-item course-" + d.type;

						var sometimesItIsHardToThinkOfAFittingVariableName = doIHavePrerequisites(coursesData, d.course.prerequisites, d.year, d.start);

						return "block-item prereq-" + sometimesItIsHardToThinkOfAFittingVariableName;
					});


				//Course position in block 
				courses
						.style("height", function(d) {
								var barHeight = (d.speed * scale) - margin;
								return barHeight + "px";
						})
						.style("width", function(d) {
								var barWidth = d.length / xMax * 100 - 0.5 ;
								return barWidth + "%";
						})
						.style("margin-left", function(d) {
								var left = d.start / xMax * 100 + 0.5;
								return left + "%";
						})
						.style("margin-top", function(d) {
								var top = (d.row * scale) + margin;
								return top + "px";
						});

				//Append link to each course
				var courseLinks = courses.append("p").attr("class", "text-center")
					.style("font-weight", "bold").style("cursor", "pointer").style("cursor", "hand").style("margin-top", "10px")
					.append("a").text(function(d){return d.title;});
				courseLinks.on('click', function(d){
						$.post(baseUrl + "/blockCourseOccasionModal", {occasion_id: d.id, block_id: rt.block_id, type: d.type }, function( data ) {
								$('.modal-title').html(d.title);
								$('.modal-body').html(data);
								$('#myModal').modal();
						});
				});

				blockRow.exit()
					.transition()
						.duration(animLength/4)
						.style("opacity", 1e-6)
						.remove();

				// Button to remove courses in the block view.
				// This button already exists, but it was added due to useability.
				// Dedicated to Jonas Bygdemo ;)
				// "Överför 25000kr till Jonas Bygdemo F14"

				var removeButton = courses.append("p").attr("class", "btn btn-link").append("a").attr("class", "glyphicon glyphicon-remove");

				removeButton.on('click', function(d){
						$.post(baseUrl + '/removeCourseOccasionFromBlockModal', {occasion_id: d.id, block_id: rt.block_id, title: d.title, type: d.type}, function(d) {
							$('.modal-title').html("Konfirmation");
							$('.modal-body').html(d);
							$('#myModal').modal();
						});
						//coursesData = removeCourseFromData(coursesData, d.id);
						//location.reload();
					});


				//Lets not forget about the add year button!
				var addYearButton = studyBlock.selectAll(".block-addyear")
						.data(['foo']);
	
				//Update button position if it exists
				addYearButton.transition()
					.duration(animLength)
					.style("top", topOffset(coursesData.length)+"px");

				//Create if doesnt exists
				addYearButton.enter()
					.append("div")
						.style("opacity", 1e-6)
						.style("left", 0)
						.style("right", 0)
						.attr("class", "block-row block-header block-addyear text-center")
						.style("top", (topOffset(coursesData.length)) + "px")
						.text("Add Year")
						.transition()
						.duration(animLength)
						.style("opacity", 1)
	
				addYearButton.on('click', function(d) {
						addCourseYear(coursesData);
						renderBlock(coursesData); //INCEPTION! :D
				});
		}
}
