var $ = require('jquery');

require("../../../node_modules/selectize/dist/js/standalone/selectize.js");

module.exports = function() {

    console.log("loaded smartSearch");

    $('#searchbox').selectize({
        valueField: 'url',
        labelField: 'title',
        searchField: ['title'],
        maxOptions: 10,
        options: [],
        create: false,
        render: {
            option: function(item, escape) {
                return '<div>' + escape(item.title) + '</div>';
            }
        },
        optgroups: [{
            value: 'profile',
            label: 'Profiler'
        }, {
            value: 'course',
            label: 'Kurser'
        }, {
            value: 'category',
            label: 'Kategorier'
        }],
        optgroupField: 'class',
        optgroupOrder: ['profile', 'course', 'category'],
        load: function(query, callback) {
            console.log(query);
            if (!query.length) return callback();
            var url = '/api/search';
            console.log(url);
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    console.log(res);
                    callback(res.data);
                }
            });
        },
        onChange: function() {
            window.location = this.items[0];
        }
    });

};