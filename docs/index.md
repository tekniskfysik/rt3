
The appliacation is bootstrapped by the laravel framework. 

The application starts by loading the service providers in app/config/app.php.

#Application packages/providers
There is three service providers included in this application. 
'Acme\AcmeServiceProvider',
'RT\RTServiceProvider',
'Goals\GoalServiceProvider'

These service providers belongs to separate packages (Acme, RT, Goals)

Acme contains the generic models for users, login, pages, groups. 
RT contains all logic for courses, exams, tracks, blocks etc and Goals contains the logic for the course goals. 

The routes as well as the intergace bindings for this module is defined in *ServiceProvider.php
The interface binding will tell the application which concrete class to instantiate when an interface is used.This make it possible to use the interface as a real class and decide here which implementation it uses, this make it possible to swich a whole class by just redefine this binding. 

Each package contains a Repository folder. Here is the database binding defined. 
All classes implements an interface where the allowed methods is defined. These classes and interfaces are connected with the interface bindings in the service provider.

There is also a Form folder containing the validation logic. These form classes is injected in the constructor for the controller and is triggered on controller actions for handling form request. (On login, or when other forms are filled). These classes using the package Laracasts\Validation. 


#Controllers
All controllers are defined in controllers in separate folders corresponding to the packages above. 
The controllers extends either the BaseController.php or the CrudController.php. 
These contains the base logic for the controllers. The CrudController is used for Crud (Create, read, update, delete) models such as courses, tracks etc. and contains some help for render the filterable tables etc. 
The routes (most of them) will call a method on a controller at each request. The controller is responsible to handle the request and prepare the data that is needed and then response with a rendered view or an json data object if it is a api call. The controller is also resposble for validatig the input and use the Form classes for form validation. 


#Views 
The pages are rendered with the blade engine.
All views are defined in app/views and the main template is defined in master.blade.php in the layouts folder.
The navigation in loaded in this file and is using a $form variable that is injected with  app/viewComposers/NavigationViewComposer from app/composers.php.
This injection make it possible to use the $form variable in all views. 


#Search
The search uses the APISearchConrtroller and will response with an object with courses, tracks, etc.. that mathces the input query string. The searchbar is implemented in resources/smartSearch.js and is using selectize javascript plugin to render the layout. 


# Behaviour - Javascript
block.js is responisible for render the bloch schedule in blocks/{id}.
It will fetch the block data and calculate the block layout with a greedy algorithm. 
The block uses the styling defined in block.less

dynamicForm.js is used to create the dynamic forms used in the edit section for courses, where it is possible to assign a course to multiple categories with pivot data (ects). 

The vendor directory contains the third party javascript files that is included in this project.
main.js include these files and browserify (gulp) create a javascript file for including in master layout.

# Styling - less/css
The styling for this application using bootstrap framework.
In resources/assets/less the application specific styling is defined. 
Run gulp to compile the styling to a css file. 