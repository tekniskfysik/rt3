<?php
return array(
  'DB_HOST' => 'localhost',
  'DB_NAME' => 'rtTest',
  'DB_PASSWORD' => 'secret',
  'DB_USERNAME' => 'homestead',
  # Only for testing purposes. Real codes are on site
  'RE_CAP_SITE' => '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI',
  'RE_CAP_SECRET' => '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'
);
