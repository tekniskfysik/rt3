# Foreword

This is the official repository for Teknisk Fysiks R�da Tr�den.

Here follows an, hopefully, up-to-date guide on how you can set up the local development environment "Homestead" so you can get to work on this website. I say hopefully since at time of writing (04/13-2018) this website is quite outdated and uses Laravel version 4.2. There was an attempt made by the original developer of this site (Christian Persson) to update it to the newer version 5.0. However, this was put to the side and there still exists a dead repository for this project. Me (Lucas Hedstr�m) took over at a later date and attempted to complete this migration. However, I was unsuccessful due to my lack of knowledge in the MVC-structure and the fundamentals of the site. Hopefully the site will live on as an outdated (and slightly annoying to use) but nevertheless useful site.

This guide uses a big part from the guide by Laravel themselves (http://laravel.com/docs/4.2/homestead), but in fear of that site getting removed, I have moved all the essential parts here, so there should not be a problem to get started just by following this guide.

Good luck!

# Starting a Homestead local development server
## Installing Virtualbox & Vagrant

Homestead is relient on the softwares [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html), so just make sure that you have the newest versions of these installed for you operating system.

## Adding the Vagrant box

We need to add the Homestead box to Vagrant. This is done by executing the following command:

`
vagrant box add laravel/homestead
`

Which can take a while dependent on how good your internet connection is.

## Installing Homestead

The easiest way to install Homestead globally is simply to use the "global" command by Composer:

`
composer global require "laravel/homestead=~2.0"
`

Which will install the 2.0 version of Homestead which is still compatible with this old of a website.

The Homestead executeable will be installed under `~/.config/composer/vendor/bin/`. Since we want to be able to use this executeable anywhere in our terminal, edit the bashrc-file

`
nano ~/.bashrc
`

and add "PATH=$PATH:~/.config/composer/vendor/bin" to the end of the file. After this, you need to source this file, which is simply done by executing `source ~/.bashrc`.

If you're using another shell you need to add this to your rc-file. However, if you're using another shell you probably know how to add a path to an executeable.

Before we initialize Homestead, we need to perform a small bugfix. In older versions of Homestead, they use a variable in a file which nowadays comes in to conflict with some variable in some other file. This is how you fix this;

1. Open up the problematic file to edit `nano ~/.config/composer/vendor/laravel/homestead/src/MakeCommand.php`.
2. Change the varible name "defaultName" to "defaultProjectName" everywhere in the file.
3. Save and exit.

Now Homestead should initialize correctly by executing `homestead init`.

After initialization, a folder in `~/.homestead` should appear that contains a file called "Homestead.yaml". This is a configuration file for your local development server. There is a lot of information online about this file, but I want to point out some important parts:

* Ip: Ip of your local server.
* Memory: The memory used by the virtual machine. You will probably need to increase this.
* folders: Maps the folder under "map:" to the folder under "to:" in your local development server.
* sites: Maps a local server name to the folder containing the websites files. Since we will call our website folder "rt3" you can change "Laravel" on that line to "rt3". Public is a folder found in all Laravel sites.

This is a note for later; When your website is up, everytime you make a change in this configuration file you need to run `homestead provision` in order for the changes to appear on your local development.

## Set your SSH key

In order to connect to your local server you need to set an SSH-key. I won't explain this much in detail, but simply run

`
ssh-keygen -t rsa -C "you@homestead@
`

and press yes on all options. You might have a key already existent. In that case, I don't think you need to run this command. I am not sure.

## Now we're getting somewhere

The server can be started by running

`
homestead up
`

This might take a moment to configure. After this is done, you can enter the server by 

`
homestead ssh
`

Now you want to download and configure R�da Tr�den. This is done by first cloning the git repository and installing the required packages by using Composer

`
git clone https://bitbucket.org/tekniskfysik/rt3.git
cd rt3
composer install
`

Composer might complain due to limited memory. The two steps to fix this is

1. Edit the limitation in php.ini. Open the file with `nano /etc/php5/cli/php.ini` and find the row with "memory_limit = 512m" and change 512m to -1.
2. Increase the memory that Homestead can use by editing the Homestead.yaml file as mentioned before.

We're soon there! First you need to create the MySQL-database for the site. This is done by

`
mysql -u homestead -psecret
create schema rtTest;
`

When executing the second command, you might get a "You must reset your password using ALTER USER statement before executing this statement". To resolve this, simply write

`
SET PASSWORD = PASSWORD('secret');
`

and you should be able to rerun that last command.

After you have created the database, you can exit MySQL by executing `exit`. Then, you need to migrate (fill) the databases with sample data. One way is to download the databases from the deployed website and import it to your local database. Another way is to run

`
php artisan migrate
php artisan db:seed
`

Unfortunately, this seed does not fill every table, and some functionalities might be gone on your test site. This should however get your local site up and running. Did I say that right? Yes, your site should be working now! Go to your browser and type the IP found in the Homestead.yaml file and you should see the website on your screen.

You might need to set the configurations on the site

`
.env.homestead.php
.env.local.php
.env.ph
`
	
dependent on which development environment you're using. 

If you need/want to change the javascript and css files you need to install gulp which is done simply through

`
sudo npm install -g gulp
npm install --no-bin-links
`

Make sure npm install is executed in the root folder of the project. Then whenever you have changed the javascript och css files you simply execute

`
	gulp
`

within the project and gulp compiles all .js and .less-files to single .js and .css files to the public folder. 

If you made it this far without using homestead you also need libnotify-bin so we install it through (If you do not have it you get some non-critical errors that does not affect the end result).

`
sudo apt-get install libnotify-bin
`

##Learning Laravel

Good video series teaching Laravel

https://laracasts.com/series/laravel-from-scratch

In order to get a clean laravel environment to follow the videos with, you can run following command in the Code/ folder in your homestead environment

```
#!

composer create-project laravel/laravel learning-laravel 4.0.9 --prefer-dist
```
which creates a folder learning-laravel with a clean version 4.0.9 of laravel.

Then in order to run this site on your browser you have to add a new site to learning-laravel's public folder in your homestead.yaml file. Don't use tab when editing the homestead.yaml file, only use spaces.

## Notes

* In order to migrate the databases in production environment you have to add the --force flag to the php artisan command. A good practice would also be to backup the databases before doing this, just to be sure.
	
### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

For further documentation about Laravel framework enter [Laravel 4.2 dox](http://laravel.com/docs/4.2)